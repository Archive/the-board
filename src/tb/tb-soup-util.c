
#include <libsoup/soup.h>

#include "tb/tb-soup-util.h"

/* This is only necessary because the SoupBuffer 
 * is not properly gir-introspected yet */

/**
 * tb_soup_buffer_get_data:
 *
 * Returns: (transfer full): the soup buffer.
 */
GByteArray *
tb_soup_buffer_get_data(SoupBuffer *buffer) {
    GByteArray *result = g_byte_array_new();

    g_byte_array_append(result, (guint8*) buffer->data, buffer->length);

    return result;
}
