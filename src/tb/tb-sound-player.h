#ifndef __TB_SOUND_PLAYER_H__
#define __TB_SOUND_PLAYER_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define TB_TYPE_SOUND_PLAYER            (tb_sound_player_get_type ())
#define TB_SOUND_PLAYER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TB_TYPE_SOUND_PLAYER, TbSoundPlayer))
#define TB_IS_SOUND_PLAYER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TB_TYPE_SOUND_PLAYER))
#define TB_SOUND_PLAYER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  TB_TYPE_SOUND_PLAYER, TbSoundPlayerClass))
#define TB_IS_SOUND_PLAYER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  TB_TYPE_SOUND_PLAYER))
#define TB_SOUND_PLAYER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TB_TYPE_SOUND_PLAYER, TbSoundPlayerClass))

typedef struct _TbSoundPlayer          TbSoundPlayer;
typedef struct _TbSoundPlayerPrivate   TbSoundPlayerPrivate;
typedef struct _TbSoundPlayerClass     TbSoundPlayerClass;

typedef enum
{
  TB_SOUND_PLAYER_STATE_UNKNOWN = 0,
  TB_SOUND_PLAYER_STATE_IDLE    = 1,
  TB_SOUND_PLAYER_STATE_PLAYING = 2,
  TB_SOUND_PLAYER_STATE_DONE    = 3,
  TB_SOUND_PLAYER_STATE_ERROR   = 4
} TbSoundPlayerState;

struct _TbSoundPlayer
{
  GObject parent_instance;

  TbSoundPlayerPrivate *priv;
};

struct _TbSoundPlayerClass
{
  GObjectClass parent_class;
};

GType    tb_sound_player_get_type     (void) G_GNUC_CONST;

G_END_DECLS

#endif /* __TB_SOUND_PLAYER_H__ */
