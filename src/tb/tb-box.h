/* -*- mode: C; c-basic-offset: 2; indent-tabs-mode: nil; -*- */
/* tb-box.h: Box container.

   Copyright (C) 2006-2008 Red Hat, Inc.
   Copyright (C) 2008-2010 litl, LLC.

   The libbigwidgets-lgpl is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The libbigwidgets-lgpl is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the libbigwidgets-lgpl; see the file COPYING.LIB.
   If not, write to the Free Software Foundation, Inc., 59 Temple Place -
   Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef __TB_BOX_H__
#define __TB_BOX_H__

#include <mx/mx.h>

G_BEGIN_DECLS

#define TB_TYPE_BOX            (tb_box_get_type ())
#define TB_BOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TB_TYPE_BOX, TbBox))
#define TB_IS_BOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TB_TYPE_BOX))
#define TB_BOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  TB_TYPE_BOX, TbBoxClass))
#define TB_IS_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  TB_TYPE_BOX))
#define TB_BOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TB_TYPE_BOX, TbBoxClass))

typedef struct _TbBox          TbBox;
typedef struct _TbBoxPrivate   TbBoxPrivate;
typedef struct _TbBoxClass     TbBoxClass;

typedef enum
{
    TB_BOX_PACK_NONE                 = 0,
    TB_BOX_PACK_EXPAND               = 1 << 0,
    TB_BOX_PACK_END                  = 1 << 1,
    TB_BOX_PACK_IF_FITS              = 1 << 2,
    TB_BOX_PACK_FIXED                = 1 << 3,
    TB_BOX_PACK_ALLOCATE_WHEN_HIDDEN = 1 << 4
} TbBoxPackFlags;

typedef enum
{
  TB_BOX_ALIGNMENT_FIXED  = 0,
  TB_BOX_ALIGNMENT_FILL   = 1,
  TB_BOX_ALIGNMENT_START  = 2,
  TB_BOX_ALIGNMENT_END    = 3,
  TB_BOX_ALIGNMENT_CENTER = 4
} TbBoxAlignment;

typedef enum
{
  TB_BOX_ORIENTATION_VERTICAL   = 1,
  TB_BOX_ORIENTATION_HORIZONTAL = 2
} TbBoxOrientation;

struct _TbBox
{
  MxWidget parent_instance;

  TbBoxPrivate *priv;
};

struct _TbBoxClass
{
  MxWidgetClass parent_class;
};

GType          tb_box_get_type              (void) G_GNUC_CONST;

ClutterActor  *tb_box_new                   (TbBoxOrientation  orientation);
void           tb_box_prepend               (TbBox            *box,
                                             ClutterActor     *child,
                                             TbBoxPackFlags   flags);
void           tb_box_append                (TbBox            *box,
                                             ClutterActor     *child,
                                             TbBoxPackFlags   flags);
gboolean       tb_box_is_empty              (TbBox            *box);
void           tb_box_remove_all            (TbBox            *box);
void           tb_box_insert_after          (TbBox            *box,
                                             ClutterActor     *child,
                                             ClutterActor     *ref_child,
                                             TbBoxPackFlags   flags);
void           tb_box_insert_before         (TbBox            *box,
                                             ClutterActor     *child,
                                             ClutterActor     *ref_child,
                                             TbBoxPackFlags   flags);
void           tb_box_set_child_packing     (TbBox            *box,
                                             ClutterActor     *child,
                                             TbBoxPackFlags    flags);
void           tb_box_set_child_align       (TbBox            *box,
                                             ClutterActor     *child,
                                             TbBoxAlignment    x_align,
                                             TbBoxAlignment    y_align);
void           tb_box_set_fixed_child_align (TbBox            *box,
                                             ClutterActor     *child,
                                             TbBoxAlignment    fixed_x_align,
                                             TbBoxAlignment    fixed_y_align);
void           tb_box_set_padding           (TbBox            *box,
                                             int               padding);
void           tb_box_set_border_width      (TbBox            *box,
                                             int               border_width);

G_END_DECLS

#endif /* __TB_BOX_H__ */
