
#include <glib.h>
#include <gio/gio.h>

#include "tb/tb-gio-util.h"

/* This is a workaround to force file contents arg in
 * g_file_replace_contents_async to be marshaled as a
 * bytearray */


/**
 * tb_g_file_replace_contents_async:
 * @cancellable: (allow-none):
 */
void
tb_g_file_replace_contents_async(GFile *file,
                                 GByteArray *contents,
                                 GCancellable *cancellable,
                                 GAsyncReadyCallback callback)
{
  g_file_replace_contents_async(file, contents->data, contents->len,
                                NULL, FALSE, G_FILE_CREATE_NONE, cancellable,
                                callback, NULL);
}

char *
tb_g_file_load_contents_finish(GFile        *file,
                               GAsyncResult *result,
                               GError      **error)
{
    char *contents;

    g_file_load_contents_finish (file, result, &contents, NULL, NULL, error);

    return contents;
}

typedef struct {
  GFile *file;
} DeleteData;

static void
delete_data_free(void *data)
{
  DeleteData *dd;

  dd = data;

  g_object_unref(dd->file);
  g_slice_free(DeleteData, dd);
}

static void
delete_async_thread(GSimpleAsyncResult *res,
                    GObject            *object,
                    GCancellable       *cancellable)
{
  DeleteData *dd;
  GError *error;

  error = NULL;

  dd = g_simple_async_result_get_op_res_gpointer(res);

  if (g_file_delete(dd->file,
                    cancellable,
                    &error))
    {
      g_assert(error == NULL);
    }
  else
    {
      g_assert(error != NULL);
      g_simple_async_result_set_from_error(res, error);
      g_error_free(error);
    }
}


void
tb_g_file_delete_async(GFile                 *file,
                       GCancellable          *cancellable,
                       GAsyncReadyCallback    callback,
                       gpointer               user_data)
{
  GSimpleAsyncResult *res;
  DeleteData *dd;

  dd = g_slice_new0(DeleteData);
  dd->file = g_object_ref(file);

  res = g_simple_async_result_new(G_OBJECT(file),
                                  callback, user_data,
                                  tb_g_file_delete_async);

  g_simple_async_result_set_op_res_gpointer(res, dd,
                                            delete_data_free);

  g_simple_async_result_run_in_thread(res,
                                      delete_async_thread,
                                      G_PRIORITY_DEFAULT,
                                      cancellable);
  g_object_unref(res);
}

gboolean
tb_g_file_delete_finish(GFile                 *file,
                        GAsyncResult          *res,
                        GError               **error)
{
  GSimpleAsyncResult *simple;

  simple = G_SIMPLE_ASYNC_RESULT(res);

  g_warn_if_fail(g_simple_async_result_get_source_tag(simple) == tb_g_file_delete_async);

  if (g_simple_async_result_propagate_error(simple, error))
    return FALSE;

  return TRUE;
}
