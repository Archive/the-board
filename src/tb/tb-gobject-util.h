#ifndef __TB_GOBJECT_UTIL_H__
#define __TB_GOBJECT_UTIL_H__

#include <glib-object.h>

void tb_signal_stop_emission_by_name (GObject     *object,
                                      const gchar *detailed_signal);

#endif /* __TB_GOBJECT_UTIL_H__ */
