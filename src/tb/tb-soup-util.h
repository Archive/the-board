#ifndef __TB_SOUP_UTIL_H__
#define __TB_SOUP_UTIL_H__

#include <glib.h>
#include <libsoup/soup.h>

GByteArray *tb_soup_buffer_get_data (SoupBuffer *buffer);

#endif /* __TB_SOUP_UTIL_H__ */
