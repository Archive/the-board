#include <glib.h>
#include <gio/gio.h>
#include <cheese/cheese-camera.h>

#include "tb/tb-cheese-util.h"

/* This is a saner way to use cheese_camera_setup()
 * and cheese_camera_play asynchronously so that we
 * never block the ui thread while webcam is loading */

typedef struct {
  CheeseCamera *camera;
} CameraStartData;

static void
camera_start_data_free(void *data)
{
  CameraStartData *dsd;

  dsd = data;

  g_object_unref(dsd->camera);
  g_slice_free(CameraStartData, dsd);
}

static void
camera_start_async_thread(GSimpleAsyncResult *res,
                          GObject            *object,
                          GCancellable       *cancellable)
{
  CameraStartData *dsd;
  GError *error;

  error = NULL;

  dsd = g_simple_async_result_get_op_res_gpointer(res);

  cheese_camera_setup(dsd->camera, NULL, &error);

  if (error != NULL)
    {
      g_simple_async_result_set_from_error(res, error);
      g_error_free(error);
      return;
    }

  cheese_camera_play(dsd->camera);
}


void
tb_cheese_camera_start_async(CheeseCamera        *camera,
                             GAsyncReadyCallback  callback)
{
  GSimpleAsyncResult *res;
  CameraStartData *dsd;

  dsd = g_slice_new0(CameraStartData);
  dsd->camera = g_object_ref(camera);

  res = g_simple_async_result_new(G_OBJECT(camera),
                                  callback, NULL,
                                  tb_cheese_camera_start_async);

  g_simple_async_result_set_op_res_gpointer(res, dsd,
                                            camera_start_data_free);

  g_simple_async_result_run_in_thread(res,
                                      camera_start_async_thread,
                                      G_PRIORITY_DEFAULT,
                                      NULL);

  g_object_unref(res);
}

gboolean
tb_cheese_camera_start_finish(CheeseCamera  *camera,
                              GAsyncResult   *res,
                              GError        **error)
{
  GSimpleAsyncResult *simple;

  simple = G_SIMPLE_ASYNC_RESULT(res);

  g_warn_if_fail(g_simple_async_result_get_source_tag(simple) == tb_cheese_camera_start_async);

  if (g_simple_async_result_propagate_error(simple, error))
    return FALSE;

  return TRUE;
}
