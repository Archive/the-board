/* -*- mode: C; c-basic-offset: 2; indent-tabs-mode: nil; -*- */
/* tb-box.c: Box container.

   Copyright (C) 2006-2008 Red Hat, Inc.
   Copyright (C) 2008-2010 litl, LLC.

   The libtbwidgets-lgpl is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The libtbwidgets-lgpl is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the libtbwidgets-lgpl; see the file COPYING.LIB.
   If not, write to the Free Software Foundation, Inc., 59 Temple Place -
   Suite 330, Boston, MA 02111-1307, USA.
*/

#include <glib.h>

#include <clutter/clutter.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include <mx/mx.h>

#include "tb/tb-box.h"
#include "tb-enum-types.h"

static void mx_stylable_iface_init (MxStylableIface *iface);
static void clutter_container_iface_init (ClutterContainerIface *iface);

G_DEFINE_TYPE_WITH_CODE (TbBox, tb_box, MX_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (CLUTTER_TYPE_CONTAINER,
                                                clutter_container_iface_init)
                         G_IMPLEMENT_INTERFACE (MX_TYPE_STYLABLE,
                                                mx_stylable_iface_init));

#define TB_BOX_GET_PRIVATE(obj)    \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), TB_TYPE_BOX, TbBoxPrivate))

#define BOX_CHILD_IS_VISIBLE(c)    \
(CLUTTER_ACTOR_IS_VISIBLE (c->actor))

#define BOX_CHILD_IN_LAYOUT(c)    \
(!c->fixed && (BOX_CHILD_IS_VISIBLE (c) || c->if_hidden))

static inline gboolean
float_equals(gfloat a, gfloat b) {

  return a - b < (1e-30) &&
         a - b > -(1e-30);
}


enum
{
  PROP_0,

  PROP_ORIENTATION,
  PROP_X_ALIGN,
  PROP_Y_ALIGN,
  PROP_DEBUG
};

struct _TbBoxPrivate
{
  GList                  *children;
  GList                  *children_in_paint_order;
  TbBoxOrientation        orientation;
  TbBoxAlignment          x_align;
  TbBoxAlignment          y_align;
  int                     spacing;
  guint                   debug               : 1;
};

typedef struct
{
  ClutterActor *actor;

  guint         expand    : 1;
  guint         end       : 1;
  guint         if_fits   : 1;
  guint         fixed     : 1;
  guint         if_hidden : 1;

  /* TbBoxAlignment applies if fixed=true */
  guint         fixed_x_align : 3;
  guint         fixed_y_align : 3;

  /* TbBoxAlignment applies if fixed=false */
  guint         x_align : 3;
  guint         y_align : 3;
} TbBoxChild;

typedef struct
{
  int          minimum;
  int          natural;
  int          adjustment;
  guint        does_not_fit : 1;
} TbBoxAdjustInfo;

static gboolean
box_child_set_flags (TbBoxChild     *c,
                     TbBoxPackFlags  flags)
{
  TbBoxPackFlags old;

  old = 0;

  if (c->end)
    old |= TB_BOX_PACK_END;
  if (c->expand)
    old |= TB_BOX_PACK_EXPAND;
  if (c->if_fits)
    old |= TB_BOX_PACK_IF_FITS;
  if (c->fixed)
    old |= TB_BOX_PACK_FIXED;
  if (c->if_hidden)
    old |= TB_BOX_PACK_ALLOCATE_WHEN_HIDDEN;

  if (old == flags)
    return FALSE; /* no change */

  c->expand    = (flags & TB_BOX_PACK_EXPAND) != 0;
  c->end       = (flags & TB_BOX_PACK_END) != 0;
  c->if_fits   = (flags & TB_BOX_PACK_IF_FITS) != 0;
  c->fixed     = (flags & TB_BOX_PACK_FIXED) != 0;
  c->if_hidden = (flags & TB_BOX_PACK_ALLOCATE_WHEN_HIDDEN) != 0;

  return TRUE;
}

static gboolean
box_child_set_fixed_align (TbBoxChild     *c,
                           TbBoxAlignment  fixed_x_align,
                           TbBoxAlignment  fixed_y_align)
{
  if (fixed_x_align == c->fixed_x_align &&
      fixed_y_align == c->fixed_y_align)
    return FALSE;

  c->fixed_x_align = fixed_x_align;
  c->fixed_y_align = fixed_y_align;

  return TRUE;
}

static gboolean
box_child_set_align (TbBoxChild     *c,
                     TbBoxAlignment  x_align,
                     TbBoxAlignment  y_align)
{
  if (x_align == c->x_align &&
      y_align == c->y_align)
    return FALSE;

  c->x_align = x_align;
  c->y_align = y_align;

  return TRUE;
}

static TbBoxChild *
box_child_find (TbBox       *box,
                ClutterActor *actor)
{
  GList *c;

  for (c = box->priv->children; c != NULL; c = c->next)
    {
      TbBoxChild *child = c->data;

      if (child->actor == actor)
        return (TbBoxChild *) child;
    }

  return NULL;
}

static TbBoxChild *
box_child_new_from_actor (ClutterActor    *child,
                          TbBoxPackFlags  flags)
{
  TbBoxChild *c;

  c = g_new0 (TbBoxChild, 1);

  g_object_ref (child);
  c->actor = child;

  box_child_set_flags (c, flags);
  c->x_align = TB_BOX_ALIGNMENT_FILL;
  c->y_align = TB_BOX_ALIGNMENT_FILL;

  return c;
}

static void
box_child_free (TbBoxChild *c)
{
  g_object_unref (c->actor);

  g_free (c);
}

static void
box_child_remove (TbBox *box, TbBoxChild *child)
{
  TbBoxPrivate *priv = box->priv;

  priv->children = g_list_remove (priv->children, child);
  priv->children_in_paint_order = g_list_remove (priv->children_in_paint_order, child);

  clutter_actor_unparent (child->actor);

  /* at this point, the actor passed to the "actor-removed" signal
   * handlers is not parented anymore to the container but since we
   * are holding a reference on it, it's still valid
   */
  g_signal_emit_by_name (box, "actor-removed", child->actor);

  box_child_free (child);
}

static void
tb_box_real_add (ClutterContainer *container,
                 ClutterActor     *child)
{
  tb_box_append (TB_BOX (container), child, TB_BOX_PACK_FIXED);
}

static void
tb_box_real_remove (ClutterContainer *container,
                    ClutterActor     *child)
{
  TbBox *box = TB_BOX (container);
  TbBoxChild *c;

  g_object_ref (child);

  c = box_child_find (box, child);

  if (c != NULL)
    {
      box_child_remove (box, c);

      clutter_actor_queue_relayout (CLUTTER_ACTOR (box));
    }

  g_object_unref (child);
}

static void
tb_box_real_foreach (ClutterContainer *container,
                     ClutterCallback   callback,
                     gpointer          user_data)
{
  TbBox *group = TB_BOX (container);
  TbBoxPrivate *priv = group->priv;
  GList *l;

  for (l = priv->children; l; l = l->next)
   {
     TbBoxChild *c = (TbBoxChild *) l->data;

     (* callback) (c->actor, user_data);
   }
}

static void
tb_box_real_foreach_with_internals (ClutterContainer *container,
                                    ClutterCallback   callback,
                                    gpointer          user_data)
{
  TbBox *group = TB_BOX (container);
  TbBoxPrivate *priv = group->priv;

  tb_box_real_foreach (container, callback, user_data);
}

static void
tb_box_real_raise (ClutterContainer *container,
                   ClutterActor     *child,
                   ClutterActor     *sibling)
{
  TbBox *box = TB_BOX (container);
  TbBoxPrivate *priv = box->priv;
  TbBoxChild *c;

  c = box_child_find (box, child);

  /* Child not found */
  if (c == NULL)
    return;

  priv->children_in_paint_order = g_list_remove (priv->children_in_paint_order, c);

  /* Raise at the top */
  if (!sibling)
    {
      GList *last_item;

      last_item = g_list_last (priv->children_in_paint_order);

      if (last_item)
        {
          TbBoxChild *sibling_child = last_item->data;

          sibling = sibling_child->actor;
        }

      priv->children_in_paint_order = g_list_append (priv->children_in_paint_order, c);
    }
  else
    {
      TbBoxChild *sibling_child;
      gint pos;

      sibling_child = box_child_find (box, sibling);

      pos = g_list_index (priv->children_in_paint_order, sibling_child) + 1;

      priv->children_in_paint_order = g_list_insert (priv->children_in_paint_order, c, pos);
    }

  if (sibling &&
      !float_equals(clutter_actor_get_depth (sibling), clutter_actor_get_depth (child)))
    {
      clutter_actor_set_depth (child, clutter_actor_get_depth (sibling));
    }
}

static void
tb_box_real_lower (ClutterContainer *container,
                   ClutterActor     *child,
                   ClutterActor     *sibling)
{
  TbBox *box = TB_BOX (container);
  TbBoxPrivate *priv = box->priv;
  TbBoxChild *c;

  c = box_child_find (box, child);

  /* Child not found */
  if (c == NULL)
    return;

  priv->children_in_paint_order = g_list_remove (priv->children_in_paint_order, c);

  /* Push to bottom */
  if (!sibling)
    {
      GList *first_item;

      first_item = g_list_first (priv->children_in_paint_order);

      if (first_item)
        {
          TbBoxChild *sibling_child = first_item->data;

          sibling = sibling_child->actor;
        }

      priv->children_in_paint_order = g_list_prepend (priv->children_in_paint_order, c);
    }
  else
    {
      TbBoxChild *sibling_child;
      gint pos;

      sibling_child = box_child_find (box, sibling);

      pos = g_list_index (priv->children_in_paint_order, sibling_child);

      priv->children_in_paint_order = g_list_insert (priv->children_in_paint_order, c, pos);
    }

  if (sibling &&
      !float_equals(clutter_actor_get_depth (sibling), clutter_actor_get_depth (child)))
    {
      clutter_actor_set_depth (child, clutter_actor_get_depth (sibling));
    }
}

static int
sort_z_order (gconstpointer a,
              gconstpointer b)
{
  const TbBoxChild *child_a = a;
  const TbBoxChild *child_b = b;
  float depth_a, depth_b;

  depth_a = clutter_actor_get_depth (child_a->actor);
  depth_b = clutter_actor_get_depth (child_b->actor);

  if (depth_a < depth_b)
    return -1;
  if (depth_a > depth_b)
    return 1;
  return 0;
}

static void
tb_box_real_sort_depth_order (ClutterContainer *container)
{
  TbBox *box = TB_BOX (container);
  TbBoxPrivate *priv = box->priv;

  priv->children_in_paint_order = g_list_sort (priv->children_in_paint_order, sort_z_order);

  if (CLUTTER_ACTOR_IS_VISIBLE (CLUTTER_ACTOR (box)))
    clutter_actor_queue_redraw (CLUTTER_ACTOR (box));
}

static void
clutter_container_iface_init (ClutterContainerIface *iface)
{
  iface->add = tb_box_real_add;
  iface->remove = tb_box_real_remove;
  iface->foreach = tb_box_real_foreach;
  iface->foreach_with_internals = tb_box_real_foreach_with_internals;
  iface->raise = tb_box_real_raise;
  iface->lower = tb_box_real_lower;
  iface->sort_depth_order = tb_box_real_sort_depth_order;
}

static void
mx_stylable_iface_init (MxStylableIface *iface)
{
  static gboolean is_initialized = FALSE;

  if (!is_initialized)
    {
      GParamSpec *pspec;

      is_initialized = TRUE;

      pspec = g_param_spec_int ("spacing",
                                "Spacing",
                                "Spacing between items of the box",
                                G_MININT, G_MAXINT, 0,
                                G_PARAM_READWRITE);
      mx_stylable_iface_install_property (iface,
                                          TB_TYPE_BOX, pspec);
    }
}

static void
tb_box_set_property (GObject      *gobject,
                     guint         prop_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
  TbBoxPrivate *priv = TB_BOX (gobject)->priv;
  gboolean need_repaint = TRUE;
  gboolean need_resize = TRUE;

  switch (prop_id)
    {
    case PROP_ORIENTATION:
      priv->orientation = g_value_get_enum (value);
      break;

    case PROP_X_ALIGN:
      priv->x_align = g_value_get_enum (value);
      break;

    case PROP_Y_ALIGN:
      priv->y_align = g_value_get_enum (value);
      break;

    case PROP_DEBUG:
      priv->debug = g_value_get_boolean (value);

      need_repaint = FALSE;
      need_resize = FALSE;
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }

  if (need_resize)
    clutter_actor_queue_relayout (CLUTTER_ACTOR (gobject));
  else if (need_repaint)
    clutter_actor_queue_redraw (CLUTTER_ACTOR (gobject));
}

static void
tb_box_get_property (GObject    *gobject,
                     guint       prop_id,
                     GValue     *value,
                     GParamSpec *pspec)
{
  TbBoxPrivate *priv = TB_BOX (gobject)->priv;

  switch (prop_id)
    {
    case PROP_ORIENTATION:
      g_value_set_enum (value, priv->orientation);
      break;

    case PROP_X_ALIGN:
      g_value_set_enum (value, priv->x_align);
      break;

    case PROP_Y_ALIGN:
      g_value_set_enum (value, priv->y_align);
      break;

    case PROP_DEBUG:
      g_value_set_boolean (value, priv->debug);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
tb_box_finalize (GObject *gobject)
{
  G_OBJECT_CLASS (tb_box_parent_class)->finalize (gobject);
}

static void
tb_box_style_changed_cb (TbBox *self)
{
  gboolean relayout;
  gint spacing;

  TbBoxPrivate *priv = TB_BOX (self)->priv;

  mx_stylable_get (MX_STYLABLE (self),
                   "spacing", &spacing,
                   NULL);

  relayout = FALSE;

  if (priv->spacing != spacing)
    {
      priv->spacing = spacing;
      relayout = TRUE;
    }

  if (relayout)
    clutter_actor_queue_relayout (CLUTTER_ACTOR (self));
}

static void
tb_box_dispose (GObject *gobject)
{
  TbBox *self = TB_BOX (gobject);
  TbBoxPrivate *priv = self->priv;

  while (priv->children)
    {
      clutter_actor_destroy(((TbBoxChild*)priv->children->data)->actor);
    }

  G_OBJECT_CLASS (tb_box_parent_class)->dispose (gobject);
}

static void
tb_box_get_content_width_request (ClutterActor  *self,
                                  int           *min_width_p,
                                  int           *natural_width_p)
{
  TbBoxPrivate *priv;
  int total_min;
  int total_natural;
  int n_children_in_min;
  int n_children_in_natural;
  GList *c;

  priv = TB_BOX (self)->priv;

  total_min = 0;
  total_natural = 0;
  n_children_in_min = 0;
  n_children_in_natural = 0;

  for (c = priv->children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;
      float min_width;
      float natural_width;

      if (!BOX_CHILD_IN_LAYOUT (child))
        continue;

      /* PACK_IF_FITS children are do not contribute to the min size
       * of the whole box, but do contribute to natural size, and
       * will be hidden entirely if their width request does not
       * fit.
       */
      clutter_actor_get_preferred_width (child->actor,
                                         -1,
                                         &min_width,
                                         &natural_width);

      if (priv->debug)
        g_debug ("Child %p min width %g natural %g",
                 child->actor,
                 min_width,
                 natural_width);

      n_children_in_natural += 1;

      /* children with if fits flag won't appear at our min width if
       * we are horizontal. If we're vertical, always request enough
       * width for all if_fits children. Children with 0 min size won't
       * themselves appear but they will get spacing around them, so
       * they count in n_children_in_min.
       */
      if (priv->orientation == TB_BOX_ORIENTATION_VERTICAL)
        {
          total_min = MAX (total_min, min_width);
          n_children_in_min += 1;

          total_natural = MAX (total_natural, natural_width);
        }
      else
        {
          if (!child->if_fits)
            {
              total_min += min_width;
              n_children_in_min += 1;
            }

          total_natural += natural_width;
        }
    }

  if (priv->orientation == TB_BOX_ORIENTATION_HORIZONTAL &&
      n_children_in_min > 1)
    {
      total_min += priv->spacing * (n_children_in_min - 1);
    }

  if (priv->orientation == TB_BOX_ORIENTATION_HORIZONTAL &&
      n_children_in_natural > 1)
    {
      total_natural += priv->spacing * (n_children_in_natural - 1);
    }

  if (min_width_p)
    *min_width_p = total_min;
  if (natural_width_p)
    *natural_width_p = total_natural;
}

static void
tb_box_get_preferred_width (ClutterActor  *self,
                            float          for_height,
                            float         *min_width_p,
                            float         *natural_width_p)
{
  TbBoxPrivate *priv;
  MxPadding padding = { 0, };
  int content_min_width;
  int content_natural_width;
  int outside;

  priv = TB_BOX (self)->priv;

  mx_widget_get_padding (MX_WIDGET (self), &padding);

  tb_box_get_content_width_request (self,
                                    &content_min_width,
                                    &content_natural_width);

  outside = padding.left + padding.right;

  if (min_width_p)
    *min_width_p = content_min_width + outside;
  if (natural_width_p)
    *natural_width_p = content_natural_width + outside;

  if (priv->debug)
    {
      if (min_width_p)
        g_debug ("Computed minimum width as %g", *min_width_p);
      if (natural_width_p)
        g_debug ("Computed natural width as %g", *natural_width_p);
    }
}

static void
tb_box_get_content_area_horizontal (ClutterActor  *self,
                                    int            requested_content_width,
                                    int            natural_content_width,
                                    int            allocated_box_width,
                                    int           *x_p,
                                    int           *width_p)
{
  TbBoxPrivate *priv;
  MxPadding padding = { 0, };
  int left;
  int right;
  int unpadded_box_width;
  int content_width;

  priv = TB_BOX (self)->priv;

  mx_widget_get_padding (MX_WIDGET (self), &padding);

  left = padding.left;
  right = padding.right;

  g_return_if_fail (requested_content_width >= 0);

  if (natural_content_width < allocated_box_width)
    content_width = natural_content_width;
  else
    content_width = MAX (requested_content_width, allocated_box_width);

  unpadded_box_width = allocated_box_width - left - right;

  switch (priv->x_align)
    {
    case TB_BOX_ALIGNMENT_FIXED:
      g_warning("Must specify a real alignment for content, not FIXED");
      break;
    case TB_BOX_ALIGNMENT_FILL:
      if (x_p)
        *x_p = left;
      if (width_p)
        *width_p = unpadded_box_width;
      break;
    case TB_BOX_ALIGNMENT_START:
      if (x_p)
        *x_p = left;
      if (width_p)
        *width_p = content_width;
      break;
    case TB_BOX_ALIGNMENT_END:
      if (x_p)
        *x_p = allocated_box_width - right - content_width;
      if (width_p)
        *width_p = content_width;
      break;
    case TB_BOX_ALIGNMENT_CENTER:
      if (x_p)
        *x_p = left + (unpadded_box_width - content_width) / 2;
      if (width_p)
        *width_p = content_width;
      break;
    }
}

static void
tb_box_get_content_area_vertical (ClutterActor   *self,
                                  int             requested_content_height,
                                  int             natural_content_height,
                                  int             allocated_box_height,
                                  int            *y_p,
                                  int            *height_p)
{
  TbBoxPrivate *priv;
  MxPadding padding = { 0, };
  int top;
  int bottom;
  int unpadded_box_height;
  int content_height;

  priv = TB_BOX (self)->priv;

  mx_widget_get_padding (MX_WIDGET (self), &padding);

  top = padding.top;
  bottom = padding.bottom;

  g_return_if_fail (requested_content_height >= 0);

  if (natural_content_height < allocated_box_height)
    content_height = natural_content_height;
  else
    content_height = MAX (requested_content_height, allocated_box_height);

  unpadded_box_height = allocated_box_height - top - bottom;

  switch (priv->y_align)
    {
    case TB_BOX_ALIGNMENT_FIXED:
      g_warning("Must specify a real alignment for content, not FIXED");
      break;
    case TB_BOX_ALIGNMENT_FILL:
      if (y_p)
        *y_p = top;
      if (height_p)
        *height_p = unpadded_box_height;
      break;
    case TB_BOX_ALIGNMENT_START:
      if (y_p)
        *y_p = top;
      if (height_p)
        *height_p = content_height;
      break;
    case TB_BOX_ALIGNMENT_END:
      if (y_p)
        *y_p = allocated_box_height - bottom - content_height;
      if (height_p)
        *height_p = content_height;
      break;
    case TB_BOX_ALIGNMENT_CENTER:
      if (y_p)
        *y_p = top + (unpadded_box_height - content_height) / 2;
      if (height_p)
        *height_p = content_height;
      break;
    }
}
static TbBoxAdjustInfo *
tb_box_adjust_infos_new (TbBox      *box,
                         float        for_content_width)
{
  TbBoxPrivate *priv = box->priv;
  TbBoxAdjustInfo *adjusts = g_new0 (TbBoxAdjustInfo, g_list_length (priv->children));
  GList *c;
  gint i = 0;

  for (c = priv->children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;

      if (!BOX_CHILD_IN_LAYOUT (child))
        {
          adjusts[i].minimum = adjusts[i].natural = 0;
        }
      else if (priv->orientation == TB_BOX_ORIENTATION_VERTICAL)
        {
          float minimum, natural;

          clutter_actor_get_preferred_height (child->actor, for_content_width,
                                              &minimum, &natural);
          adjusts[i].minimum = minimum;
          adjusts[i].natural = natural;
        }
      else
        {
          float minimum, natural;

          clutter_actor_get_preferred_width (child->actor, -1,
                                             &minimum, &natural);

          adjusts[i].minimum = minimum;
          adjusts[i].natural = natural;
        }

      i++;
    }

  return adjusts;
}

static void
tb_box_adjust_if_fits_as_not_fitting (GList            *children,
                                      TbBoxAdjustInfo *adjusts)
{
    GList *c;
    gint i;

    i = 0;

    for (c = children; c != NULL; c = c->next)
      {
        TbBoxChild *child = (TbBoxChild *) c->data;

        if (child->if_fits)
          {
            adjusts[i].adjustment -= adjusts[i].minimum;
            adjusts[i].does_not_fit = TRUE;
          }

        ++i;
      }
}

static gboolean
tb_box_adjust_up_to_natural_size (GList            *children,
                                  int              *remaining_extra_space_p,
                                  TbBoxAdjustInfo *adjusts,
                                  gboolean          if_fits)
{
  int smallest_increase;
  int space_to_distribute;
  GList *c;
  gint n_needing_increase;
  gint i;

  g_assert (*remaining_extra_space_p >= 0);

  if (*remaining_extra_space_p == 0)
    return FALSE;

  smallest_increase = G_MAXINT;
  n_needing_increase = 0;

  i = 0;

  for (c = children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;

      if (BOX_CHILD_IN_LAYOUT (child) &&
          ((!child->if_fits && !if_fits) ||
           (child->if_fits && if_fits && !adjusts[i].does_not_fit)))
        {
          float needed_increase;

          g_assert (adjusts[i].adjustment >= 0);

          /* Guaranteed to be >= 0 */
          needed_increase = adjusts[i].natural - adjusts[i].minimum;

          g_assert (needed_increase >= 0);

          needed_increase -= adjusts[i].adjustment; /* see how much we've already increased */

          if (needed_increase > 0)
            {
              n_needing_increase += 1;
              smallest_increase = MIN (smallest_increase, needed_increase);
            }
        }

      ++i;
    }

  if (n_needing_increase == 0)
    return FALSE;

  g_assert (smallest_increase < G_MAXINT);

  space_to_distribute = MIN (*remaining_extra_space_p,
                             smallest_increase * n_needing_increase);

  g_assert(space_to_distribute >= 0);
  g_assert(space_to_distribute <= *remaining_extra_space_p);

  *remaining_extra_space_p -= space_to_distribute;

  i = 0;

  for (c = children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;

      if (BOX_CHILD_IN_LAYOUT (child) &&
          ((!child->if_fits && !if_fits) ||
           (child->if_fits && if_fits && !adjusts[i].does_not_fit)))
        {
          float needed_increase;

          g_assert (adjusts[i].adjustment >= 0);

          /* Guaranteed to be >= 0 */
          needed_increase = adjusts[i].natural - adjusts[i].minimum;

          g_assert(needed_increase >= 0);

          needed_increase -= adjusts[i].adjustment; /* see how much we've already increased */

          if (needed_increase > 0)
            {
              float extra;

              extra = (space_to_distribute / n_needing_increase);

              n_needing_increase -= 1;
              space_to_distribute -= extra;
              adjusts[i].adjustment += extra;
            }
        }

      ++i;
    }

  g_assert (n_needing_increase == 0);
  g_assert (space_to_distribute == 0);

  return TRUE;
}

static gboolean
tb_box_adjust_one_if_fits (GList             *children,
                           int                spacing,
                           int               *remaining_extra_space_p,
                           TbBoxAdjustInfo  *adjusts)
{
  GList *c;
  int spacing_delta;
  gint i;
  gboolean visible_children = FALSE;

  if (*remaining_extra_space_p == 0)
    return FALSE;

  /* if there are no currently visible children, then adding a child won't
   * add another spacing
   */
  i = 0;

  for (c = children; c != NULL; c = c->next) {
      TbBoxChild *child = (TbBoxChild *) c->data;

      if (BOX_CHILD_IN_LAYOUT (child) &&
          (!child->if_fits || !adjusts[i].does_not_fit))
        {
          visible_children = TRUE;
          break;
        }

      i++;
  }

  spacing_delta = visible_children ? spacing : 0;

  i = 0;

  for (c = children; c != NULL; c = c->next)
    {
      if (adjusts[i].does_not_fit)
        {
          /* This child was adjusted downward, see if we can pop it visible
           * (picking the smallest instead of first if-fits child on each pass
           * might be nice, but for now it's the first that fits)
           */
          if ((adjusts[i].minimum + spacing_delta) <= *remaining_extra_space_p)
            {
              adjusts[i].adjustment += adjusts[i].minimum;

              g_assert (adjusts[i].adjustment >= 0);

              adjusts[i].does_not_fit = FALSE;
              *remaining_extra_space_p -= (adjusts[i].minimum + spacing_delta);

              g_assert (*remaining_extra_space_p >= 0);

              return TRUE;
            }
        }

      ++i;
    }

  return FALSE;
}

static gboolean
box_child_is_expandable (TbBoxChild *child, TbBoxAdjustInfo *adjust)
{
  return (BOX_CHILD_IS_VISIBLE (child) || child->if_hidden) && child->expand &&
         (!child->if_fits || (adjust && !(adjust->does_not_fit)));
}

static int
tb_box_count_expandable_children (GList           *children,
                                  TbBoxAdjustInfo *adjusts)
{
  GList *c;
  gint count;
  gint i;

  count = 0;
  i = 0;

  for (c = children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;

      /* We assume here that we've prevented via g_warning
       * any floats/fixed from having expand=TRUE
       */
      if (box_child_is_expandable (child, adjusts ? &(adjusts[i]) : NULL))
        ++count;

      ++i;
    }

  return count;
}

static void
tb_box_adjust_for_expandable (GList            *children,
                              int              *remaining_extra_space_p,
                              TbBoxAdjustInfo *adjusts)
{
  GList *c;
  int expand_space;
  gint expand_count;
  gint i;

  if (*remaining_extra_space_p == 0)
    return;

  expand_space = *remaining_extra_space_p;
  expand_count = tb_box_count_expandable_children (children, adjusts);

  if (expand_count == 0)
    return;

  i = 0;

  for (c = children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;

      if (box_child_is_expandable (child, &(adjusts[i])) &&
          !adjusts[i].does_not_fit)
        {
          float extra;

          extra = (expand_space / expand_count);

          expand_count -= 1;
          expand_space -= extra;
          adjusts[i].adjustment += extra;
        }

      ++i;
    }

  /* if we had anything to expand, then we will have used up all space */
  g_assert (expand_space == 0);
  g_assert (expand_count == 0);

  *remaining_extra_space_p = 0;
}
static void
tb_box_compute_adjusts (GList             *children,
                        TbBoxAdjustInfo  *adjusts,
                        int                spacing,
                        int                alloc_request_delta)
{
  int remaining_extra_space;

  if (children == NULL)
    return;

  /* Go ahead and cram all PACK_IF_FITS children to zero width,
   * we'll expand them again if we can.
   */
  tb_box_adjust_if_fits_as_not_fitting (children, adjusts);

  /* Make no adjustments if we got too little or just right space.
   * (FIXME handle too little space better)
   */
  if (alloc_request_delta <= 0) {
      return;
  }

  remaining_extra_space = alloc_request_delta;

  /* Adjust non-PACK_IF_FITS up to natural size */
  while (tb_box_adjust_up_to_natural_size (children,
                                           &remaining_extra_space, adjusts,
                                           FALSE))
      ;

  /* See if any PACK_IF_FITS can get their minimum size */
  while (tb_box_adjust_one_if_fits (children,
                                    spacing, &remaining_extra_space, adjusts))
      ;

  /* If so, then see if they can also get a natural size */
  while (tb_box_adjust_up_to_natural_size(children,
                                          &remaining_extra_space, adjusts,
                                          TRUE))
      ;

  /* And finally we can expand to fill empty space */
  tb_box_adjust_for_expandable (children, &remaining_extra_space, adjusts);

  /* remaining_extra_space need not be 0, if we had no expandable children */
}

static int
tb_box_get_adjusted_size (TbBoxAdjustInfo  *adjust)
{
  return adjust->minimum + adjust->adjustment;
}

static void
tb_box_get_hbox_height_request (ClutterActor  *self,
                                int            for_width,
                                int           *min_height_p,
                                int           *natural_height_p)
{
  TbBoxPrivate *priv;
  int total_min;
  int total_natural;
  int requested_content_width;
  int natural_content_width;
  int allocated_content_width;
  TbBoxAdjustInfo *width_adjusts;
  GList *c;
  gint i;

  priv = TB_BOX (self)->priv;

  total_min = 0;
  total_natural = 0;

  tb_box_get_content_width_request (self, &requested_content_width,
                                    &natural_content_width);

  tb_box_get_content_area_horizontal (self,
                                      requested_content_width,
                                      natural_content_width,
                                      for_width, NULL,
                                      &allocated_content_width);

  width_adjusts = tb_box_adjust_infos_new (TB_BOX (self), for_width);

  tb_box_compute_adjusts (priv->children,
                          width_adjusts,
                          priv->spacing,
                          allocated_content_width - requested_content_width);

  i = 0;

  for (c = priv->children; c != NULL; c = c->next)
    {
      TbBoxChild *child = c->data;
      float min_height, natural_height;
      int req = 0;

      if (!BOX_CHILD_IN_LAYOUT (child))
        {
          ++i;
          continue;
        }

      req = tb_box_get_adjusted_size (&width_adjusts[i]);

      clutter_actor_get_preferred_height (child->actor, req,
                                          &min_height, &natural_height);

      if (priv->debug)
        g_debug ("H - Child %p min height %g natural %g",
                 child->actor, min_height, natural_height);

      total_min = MAX (total_min, min_height);
      total_natural = MAX (total_natural, natural_height);

      ++i;
    }

  g_free (width_adjusts);

  if (min_height_p)
    *min_height_p = total_min;
  if (natural_height_p)
    *natural_height_p = total_natural;
}

static void
tb_box_get_vbox_height_request (ClutterActor  *self,
                                int            for_width,
                                int           *min_height_p,
                                int           *natural_height_p)
{
  TbBoxPrivate *priv;
  int total_min;
  int total_natural;
  int n_children_in_min;
  int n_children_in_natural;
  GList *c;

  priv = TB_BOX (self)->priv;

  total_min = 0;
  total_natural = 0;
  n_children_in_min = 0;
  n_children_in_natural = 0;

  for (c = priv->children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;
      float min_height;
      float natural_height;

      if (!BOX_CHILD_IN_LAYOUT (child))
        continue;

      clutter_actor_get_preferred_height (child->actor, for_width,
                                          &min_height, &natural_height);

      if (priv->debug)
        g_debug ("V - Child %p min height %g natural %g",
                 child->actor, min_height, natural_height);

      n_children_in_natural += 1;
      total_natural += natural_height;

      if (!child->if_fits)
        {
          n_children_in_min += 1;
          total_min += min_height;
        }
    }

  if (n_children_in_min > 1)
    total_min += priv->spacing * (n_children_in_min - 1);
  if (n_children_in_natural > 1)
    total_natural += priv->spacing * (n_children_in_natural - 1);

  if (min_height_p)
    *min_height_p = total_min;
  if (natural_height_p)
    *natural_height_p = total_natural;
}

static void
tb_box_get_content_height_request (ClutterActor  *self,
                                   int            for_width,
                                   int           *min_height_p,
                                   int           *natural_height_p)
{
  TbBoxPrivate *priv;

  priv = TB_BOX (self)->priv;

  if (priv->orientation == TB_BOX_ORIENTATION_VERTICAL)
    tb_box_get_vbox_height_request (self, for_width,
                                     min_height_p, natural_height_p);
  else
    tb_box_get_hbox_height_request (self, for_width,
                                     min_height_p, natural_height_p);
}

static void
tb_box_get_preferred_height (ClutterActor  *self,
                             float          for_width,
                             float         *min_height_p,
                             float         *natural_height_p)
{
  TbBoxPrivate *priv;
  MxPadding padding = { 0, };
  int content_min_height, content_natural_height;
  int content_for_width;
  int outside;

  priv = TB_BOX (self)->priv;

  mx_widget_get_padding (MX_WIDGET (self), &padding);

  content_for_width = for_width
      - padding.left - padding.right;

  /* We need to call this even if just returning the box-height prop,
   * so that children can rely on getting the full request, allocate
   * cycle in order every time, and so we compute the cached requests.
   */
  tb_box_get_content_height_request (self,
                                     content_for_width,
                                     &content_min_height,
                                     &content_natural_height);

  outside = padding.top + padding.bottom;

  if (min_height_p)
      *min_height_p = content_min_height + outside;
  if (natural_height_p)
      *natural_height_p = content_natural_height + outside;


  if (priv->debug)
    {
      if (min_height_p)
        g_debug ("Computed minimum height for width=%g as %g",
                 for_width, *min_height_p);
      if (natural_height_p)
        g_debug ("Computed natural height for width=%g as %g",
                 for_width, *natural_height_p);
    }
}

static void
box_child_align_horizontal (TbBoxChild      *child,
                            float            height,
                            float            content_x,
                            float            allocated_content_width,
                            ClutterActorBox *child_box)
{
  float x, y, width;

  clutter_actor_get_position (child->actor, &x, &y);
  clutter_actor_get_preferred_width(child->actor, height, NULL, &width);

  switch (child->x_align)
    {
    case TB_BOX_ALIGNMENT_FIXED:
      /* honor child forced x,width but take borders and padding
       * into account (unlike with fixed children) */
      child_box->x1 = content_x + x;
      child_box->x2 = child_box->x1 + width;
      break;
    case TB_BOX_ALIGNMENT_START:
      child_box->x1 = content_x;
      child_box->x2 = child_box->x1 + width;
      break;
    case TB_BOX_ALIGNMENT_END:
      child_box->x2 = content_x + allocated_content_width;
      child_box->x1 = child_box->x2 - width;
      break;
    case TB_BOX_ALIGNMENT_CENTER:
      child_box->x1 = content_x + (allocated_content_width - width) / 2;
      child_box->x2 = child_box->x1 + width;
      break;
    case TB_BOX_ALIGNMENT_FILL:
      child_box->x1 = content_x;
      child_box->x2 = content_x + allocated_content_width;
      break;
    }
}

static void
box_child_align_vertical (TbBoxChild      *child,
                          float            width,
                          float            content_y,
                          float            allocated_content_height,
                          ClutterActorBox *child_box)
{
  float x, y, height;

  clutter_actor_get_position (child->actor, &x, &y);
  clutter_actor_get_preferred_height (child->actor, width, NULL, &height);

  switch (child->y_align)
    {
    case TB_BOX_ALIGNMENT_FIXED:
      /* honor child forced y,height but take borders and padding
       * into account (unlike with fixed children) */
      child_box->y1 = content_y + y;
      child_box->y2 = child_box->y1 + height;
      break;
    case TB_BOX_ALIGNMENT_START:
      child_box->y1 = content_y;
      child_box->y2 = child_box->y1 + height;
      break;
    case TB_BOX_ALIGNMENT_END:
      child_box->y2 = content_y + allocated_content_height;
      child_box->y1 = child_box->y2 - height;
      break;
    case TB_BOX_ALIGNMENT_CENTER:
      child_box->y1 = content_y + (allocated_content_height - height) / 2;
      child_box->y2 = child_box->y1 + height;
      break;
    case TB_BOX_ALIGNMENT_FILL:
      child_box->y1 = content_y;
      child_box->y2 = content_y + allocated_content_height;
      break;
    }
}

static void
tb_box_layout (ClutterActor          *self,
               float                  content_x,
               float                  content_y,
               float                  allocated_content_width,
               float                  allocated_content_height,
               float                  requested_content_width,
               float                  requested_content_height,
               ClutterAllocationFlags flags)
{
  TbBoxPrivate *priv;
  TbBoxAdjustInfo *adjusts;
  ClutterActorBox child_box;
  float allocated_size, requested_size;
  float start;
  float end;
  GList *c;
  gint i;

  priv = TB_BOX (self)->priv;

  if (priv->orientation == TB_BOX_ORIENTATION_VERTICAL)
    {
      allocated_size = allocated_content_height;
      requested_size = requested_content_height;
      start = content_y;
    }
  else
    {
      allocated_size = allocated_content_width;
      requested_size = requested_content_width;
      start = content_x;
    }

  end = start + allocated_size;

  adjusts = tb_box_adjust_infos_new (TB_BOX (self), allocated_content_width);

  tb_box_compute_adjusts (priv->children,
                          adjusts,
                          priv->spacing,
                          allocated_size - requested_size);

  i = 0;

  for (c = priv->children; c != NULL; c = c->next)
   {
      TbBoxChild *child = (TbBoxChild *) c->data;
      float req;

      if (!BOX_CHILD_IN_LAYOUT (child))
        {
          ++i;
          continue;
        }

      req = tb_box_get_adjusted_size (&adjusts[i]);

      if (priv->orientation == TB_BOX_ORIENTATION_VERTICAL)
        {
          child_box.y1 = child->end ? end - req : start;
          child_box.y2 = child_box.y1 + req;

          box_child_align_horizontal (child, req,
                                      content_x, allocated_content_width,
                                      &child_box);

          if (priv->debug)
            g_debug ("V - Child %p %s being allocated: %g, %g, %g, %g w: %g h: %g",
                     child->actor,
                     g_type_name_from_instance((GTypeInstance*) child->actor),
                     child_box.x1,
                     child_box.y1,
                     child_box.x2,
                     child_box.y2,
                     child_box.x2 - child_box.x1,
                     child_box.y2 - child_box.y1);

          clutter_actor_allocate (child->actor, &child_box,
                                  flags);
        }
      else
        {
          child_box.x1 = child->end ? end - req : start;
          child_box.x2 = child_box.x1 + req;

          box_child_align_vertical (child, req,
                                    content_y, allocated_content_height,
                                    &child_box);

          if (priv->debug)
            g_debug ("H - Child %p %s being allocated: %g, %g, %g, %g  w: %g h: %g",
                     child->actor,
                     g_type_name_from_instance((GTypeInstance*) child->actor),
                     child_box.x1,
                     child_box.y1,
                     child_box.x2,
                     child_box.y2,
                     child_box.x2 - child_box.x1,
                     child_box.y2 - child_box.y1);

          clutter_actor_allocate (child->actor, &child_box,
                                  flags);
        }

      if (req <= 0)
        {
          /* Child was adjusted out of existence, act like it's
           * !visible
           */
          child_box.x1 = 0;
          child_box.y1 = 0;
          child_box.x2 = 0;
          child_box.y2 = 0;

          clutter_actor_allocate (child->actor, &child_box,
                                  flags);
        }

      /* Children with req == 0 still get spacing unless they are IF_FITS.
       * The handling of spacing could use improvement (spaces should probably
       * act like items with min width 0 and natural width of spacing) but
       * it's pretty hard to get right without rearranging the code a lot.
       */
      if (!adjusts[i].does_not_fit)
        {
          if (child->end)
            end -= (req + priv->spacing);
          else
            start += (req + priv->spacing);
        }

      ++i;
    }

  g_free (adjusts);
}

static void
tb_box_allocate (ClutterActor          *self,
                 const ClutterActorBox *box,
                 ClutterAllocationFlags flags)
{
  TbBoxPrivate *priv;
  int requested_content_width;
  int requested_content_height;
  int natural_content_width;
  int natural_content_height;
  int allocated_content_width;
  int allocated_content_height = 0;
  int content_x, content_y = 0;
  GList *c;

  CLUTTER_ACTOR_CLASS (tb_box_parent_class)->allocate (self,
                                                        box,
                                                        flags);

  priv = TB_BOX (self)->priv;

  if (priv->debug)
    g_debug ("Entire box %p being allocated: %g, %g, %g, %g",
             self,
             box->x1,
             box->y1,
             box->x2,
             box->y2);

  CLUTTER_ACTOR_CLASS (tb_box_parent_class)->allocate (self, box, flags);

  tb_box_get_content_width_request (self,
                                    &requested_content_width,
                                    &natural_content_width);

  tb_box_get_content_area_horizontal (self, requested_content_width,
                                      natural_content_width, box->x2 - box->x1,
                                      &content_x, &allocated_content_width);

  tb_box_get_content_height_request (self,
                                     allocated_content_width,
                                     &requested_content_height,
                                     &natural_content_height);

  tb_box_get_content_area_vertical (self, requested_content_height,
                                    natural_content_height, box->y2 - box->y1,
                                    &content_y, &allocated_content_height);

  if (priv->debug)
    {
      if (allocated_content_height < requested_content_height)
        g_debug ("Box %p allocated height %d but requested %d",
                 self,
                 allocated_content_height,
                 requested_content_height);
      if (allocated_content_width < requested_content_width)
        g_debug ("Box %p allocated width %d but requested %d",
                 self,
                 allocated_content_width,
                 requested_content_width);
    }

  for (c = priv->children; c != NULL; c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;
      ClutterActorBox child_box;

      if (!(BOX_CHILD_IS_VISIBLE (child) || child->if_hidden))
        {
          child_box.x1 = 0;
          child_box.y1 = 0;
          child_box.x2 = 0;
          child_box.y2 = 0;

          clutter_actor_allocate (child->actor, &child_box, FALSE);
        }
      else if (child->fixed)
        {
          float x, y, width, height;

          clutter_actor_get_position (child->actor, &x, &y);
          clutter_actor_get_preferred_width(child->actor, -1, NULL, &width);
          clutter_actor_get_preferred_height(child->actor, width, NULL, &height);

          switch (child->fixed_x_align)
            {
            case TB_BOX_ALIGNMENT_FIXED:
              /* honor child forced x,y instead of aligning automatically */
              child_box.x1 = x;
              child_box.x2 = x + width;
              break;
            case TB_BOX_ALIGNMENT_START:
              child_box.x1 = content_x;
              child_box.x2 = child_box.x1 + width;
              break;
            case TB_BOX_ALIGNMENT_END:
              child_box.x2 = content_x + allocated_content_width;
              child_box.x1 = child_box.x2 - width;
              break;
            case TB_BOX_ALIGNMENT_CENTER:
              child_box.x1 = content_x + (allocated_content_width - width) / 2;
              child_box.x2 = child_box.x1 + width;
              break;
            case TB_BOX_ALIGNMENT_FILL:
              child_box.x1 = content_x;
              child_box.x2 = content_x + allocated_content_width;
              break;
            }

          switch (child->fixed_y_align)
            {
            case TB_BOX_ALIGNMENT_FIXED:
              /* honor child forced x,y instead of aligning automatically */
              child_box.y1 = y;
              child_box.y2 = y + height;
              break;
            case TB_BOX_ALIGNMENT_START:
              child_box.y1 = content_y;
              child_box.y2 = child_box.y1 + height;
              break;
            case TB_BOX_ALIGNMENT_END:
              child_box.y2 = content_y + allocated_content_height;
              child_box.y1 = child_box.y2 - height;
              break;
            case TB_BOX_ALIGNMENT_CENTER:
              child_box.y1 = content_y + (allocated_content_height - height) / 2;
              child_box.y2 = child_box.y1 + height;
              break;
            case TB_BOX_ALIGNMENT_FILL:
              child_box.y1 = content_y;
              child_box.y2 = content_y + allocated_content_height;
              break;
            }

          if (priv->debug)
            g_debug ("Fixed Child being allocated: %g, %g, %g, %g",
                     child_box.x1,
                     child_box.y1,
                     child_box.x2,
                     child_box.y2);

          clutter_actor_allocate(child->actor, &child_box,
                                 flags);
        }
    }

  tb_box_layout (self, content_x, content_y,
                 allocated_content_width, allocated_content_height,
                 requested_content_width, requested_content_height,
                 flags);
}

static void
tb_box_paint (ClutterActor *actor)
{
  TbBox *self = TB_BOX (actor);
  GList *c;
  ClutterColor color;
  guint8 actor_opacity;
  ClutterGeometry allocation;

  CLUTTER_ACTOR_CLASS (tb_box_parent_class)->paint (actor);

  actor_opacity = clutter_actor_get_paint_opacity (actor);

  /* Remember allocation.x and .y don't matter since we are
   * theoretically already translated to 0,0
   */
  clutter_actor_get_allocation_geometry (actor, &allocation);

  /* Children */

  for (c = self->priv->children_in_paint_order;
       c != NULL;
       c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;

      g_assert (child != NULL);

      if (BOX_CHILD_IS_VISIBLE (child)) {
        clutter_actor_paint (child->actor);
      }
    }
}

static void
tb_box_pick (ClutterActor       *actor,
              const ClutterColor *color)
{
  TbBox *self = TB_BOX (actor);
  GList *c;

  CLUTTER_ACTOR_CLASS (tb_box_parent_class)->pick (actor, color);

  for (c = self->priv->children_in_paint_order;
       c != NULL;
       c = c->next)
    {
      TbBoxChild *child = (TbBoxChild *) c->data;

      g_assert (child != NULL);

      if (BOX_CHILD_IS_VISIBLE (child))
        clutter_actor_paint (child->actor);
    }
}

static gboolean
tb_box_get_paint_volume (ClutterActor       *actor,
                         ClutterPaintVolume *volume)
{
  return clutter_paint_volume_set_from_allocation (volume, actor);
}

static void
tb_box_class_init (TbBoxClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TbBoxPrivate));

  gobject_class->set_property = tb_box_set_property;
  gobject_class->get_property = tb_box_get_property;
  gobject_class->dispose = tb_box_dispose;
  gobject_class->finalize = tb_box_finalize;

  actor_class->get_preferred_width = tb_box_get_preferred_width;
  actor_class->get_preferred_height = tb_box_get_preferred_height;
  actor_class->get_paint_volume = tb_box_get_paint_volume;
  actor_class->allocate = tb_box_allocate;
  actor_class->paint = tb_box_paint;
  actor_class->pick  = tb_box_pick;

  g_object_class_install_property
                 (gobject_class,
                  PROP_ORIENTATION,
                  g_param_spec_enum ("orientation",
                                     "Orientation",
                                     "Orientation of the box",
                                     TB_TYPE_BOX_ORIENTATION,
                                     TB_BOX_ORIENTATION_VERTICAL,
                                     G_PARAM_READWRITE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_X_ALIGN,
                  g_param_spec_enum ("x-align",
                                     "X alignment",
                                     "X alignment",
                                     TB_TYPE_BOX_ALIGNMENT,
                                     TB_BOX_ALIGNMENT_START,
                                     G_PARAM_READWRITE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_Y_ALIGN,
                  g_param_spec_enum ("y-align",
                                     "Y alignment",
                                     "Y alignment",
                                     TB_TYPE_BOX_ALIGNMENT,
                                     TB_BOX_ALIGNMENT_START,
                                    G_PARAM_READWRITE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_DEBUG,
                  g_param_spec_boolean ("debug",
                                        "Debug",
                                        "Whether debug is activated or not",
                                        FALSE,
                                        G_PARAM_READWRITE));
}

static void
tb_box_init (TbBox *box)
{
  box->priv = TB_BOX_GET_PRIVATE (box);

  box->priv->orientation = TB_BOX_ORIENTATION_VERTICAL;
  box->priv->x_align = TB_BOX_ALIGNMENT_FILL;
  box->priv->y_align = TB_BOX_ALIGNMENT_FILL;
  box->priv->spacing = 0;

  g_signal_connect (box, "style-changed",
                    G_CALLBACK (tb_box_style_changed_cb), NULL);
}

ClutterActor *
tb_box_new (TbBoxOrientation orientation)
{
  return g_object_new (TB_TYPE_BOX,
                       "orientation", orientation,
                       NULL);
}

void
tb_box_prepend (TbBox          *box,
                ClutterActor    *child,
                TbBoxPackFlags  flags)
{
  TbBoxPrivate *priv;
  TbBoxChild *c;

  g_return_if_fail (TB_IS_BOX (box));
  g_return_if_fail (CLUTTER_IS_ACTOR (child));

  priv = box->priv;

  g_object_ref (child);

  c = box_child_new_from_actor (child, flags);

  priv->children = g_list_prepend (priv->children, c);
  priv->children_in_paint_order = g_list_append (priv->children_in_paint_order, c);

  clutter_actor_set_parent (child, CLUTTER_ACTOR (box));

  g_signal_emit_by_name (box, "actor-added", child);

  tb_box_real_sort_depth_order (CLUTTER_CONTAINER (box));

  clutter_actor_queue_relayout (CLUTTER_ACTOR (box));

  g_object_unref (child);
}

void
tb_box_append (TbBox          *box,
               ClutterActor    *child,
               TbBoxPackFlags  flags)
{
  TbBoxPrivate *priv;
  TbBoxChild *c;

  g_return_if_fail (TB_IS_BOX (box));
  g_return_if_fail (CLUTTER_IS_ACTOR (child));

  if (clutter_actor_get_parent (child))
    {
      g_warning ("Attempting to add an actor with type %s to a box with type %s, "
                 "but the actor is already inside a parent of type %s.",
                 g_type_name (G_OBJECT_TYPE (child)),
                 g_type_name (G_OBJECT_TYPE (box)),
                 g_type_name (G_OBJECT_TYPE (clutter_actor_get_parent (child))));
      return;
    }

  priv = box->priv;

  g_object_ref (child);

  c = box_child_new_from_actor (child, flags);

  priv->children = g_list_append (priv->children, c);
  priv->children_in_paint_order = g_list_append (priv->children_in_paint_order, c);

  clutter_actor_set_parent (child, CLUTTER_ACTOR (box));

  g_signal_emit_by_name (box, "actor-added", child);

  tb_box_real_sort_depth_order (CLUTTER_CONTAINER (box));

  clutter_actor_queue_relayout (CLUTTER_ACTOR (box));

  g_object_unref (child);
}

gboolean
tb_box_is_empty (TbBox *box)
{
  g_return_val_if_fail (TB_IS_BOX (box), TRUE);

  return (box->priv->children == NULL);
}

void
tb_box_remove_all (TbBox *box)
{
  TbBoxPrivate *priv;

  g_return_if_fail (TB_IS_BOX (box));

  priv = box->priv;

  while (priv->children != NULL) {
    TbBoxChild *child = priv->children->data;

    box_child_remove (box, child);
  }

  clutter_actor_queue_relayout (CLUTTER_ACTOR (box));
}

void
tb_box_insert_after (TbBox          *box,
                     ClutterActor    *child,
                     ClutterActor    *ref_child,
                     TbBoxPackFlags  flags)
{
  TbBoxPrivate *priv;
  TbBoxChild *c, *ref_c;
  gint position;

  g_return_if_fail (TB_IS_BOX (box));
  g_return_if_fail (CLUTTER_IS_ACTOR (child));
  g_return_if_fail (CLUTTER_IS_ACTOR (ref_child));

  priv = box->priv;

  g_object_ref (child);

  ref_c = box_child_find (box, ref_child);

  if (ref_c != NULL)
    {
      c = box_child_new_from_actor (child, flags);

      position = g_list_index (priv->children, ref_c);
      priv->children = g_list_insert (priv->children, c, ++position);
      priv->children_in_paint_order =
          g_list_append (priv->children_in_paint_order, c);

      clutter_actor_set_parent (child, CLUTTER_ACTOR (box));

      g_signal_emit_by_name (box, "actor-added", child);

      tb_box_real_sort_depth_order (CLUTTER_CONTAINER (box));

      clutter_actor_queue_relayout (CLUTTER_ACTOR (box));
    }
  else
    {
      g_warning("Attempting to insert an actor of type `%s' to a TbBox"
                " of type `%s' after a child actor of type `%s'"
                " but the TbBox is not its parent.",
                g_type_name (G_OBJECT_TYPE (child)),
                g_type_name (G_OBJECT_TYPE (box)),
                g_type_name (G_OBJECT_TYPE (ref_child)));
    }

  g_object_unref (child);
}

void
tb_box_insert_before (TbBox          *box,
                      ClutterActor    *child,
                      ClutterActor    *ref_child,
                      TbBoxPackFlags  flags)
{
  TbBoxPrivate *priv;
  TbBoxChild *c, *ref_c;
  gint position;

  g_return_if_fail (TB_IS_BOX (box));
  g_return_if_fail (CLUTTER_IS_ACTOR (child));
  g_return_if_fail (CLUTTER_IS_ACTOR (ref_child));

  priv = box->priv;

  g_object_ref (child);

  ref_c = box_child_find (box, ref_child);

  if (ref_c != NULL)
    {
      c = box_child_new_from_actor (child, flags);

      position = g_list_index (priv->children, ref_c);
      priv->children = g_list_insert (priv->children, c, position);
      priv->children_in_paint_order =
          g_list_append (priv->children_in_paint_order, c);

      clutter_actor_set_parent (child, CLUTTER_ACTOR (box));

      g_signal_emit_by_name (box, "actor-added", child);

      tb_box_real_sort_depth_order (CLUTTER_CONTAINER (box));

      clutter_actor_queue_relayout (CLUTTER_ACTOR (box));
    }
  else
    {
      g_warning("Attempting to insert an actor of type `%s' to a TbBox"
                " of type `%s' before a child actor of type `%s'"
                " but the TbBox is not its parent.",
                g_type_name (G_OBJECT_TYPE (child)),
                g_type_name (G_OBJECT_TYPE (box)),
                g_type_name (G_OBJECT_TYPE (ref_child)));
    }

  g_object_unref (child);
}

void
tb_box_set_child_packing (TbBox          *box,
                          ClutterActor    *child,
                          TbBoxPackFlags  flags)
{
  TbBoxChild *c;

  g_return_if_fail (TB_IS_BOX (box));
  g_return_if_fail (CLUTTER_IS_ACTOR (child));

  g_object_ref (child);

  c = box_child_find (box, child);

  if (c != NULL && box_child_set_flags (c, flags))
    clutter_actor_queue_relayout (CLUTTER_ACTOR (box));

  g_object_unref (child);
}

void
tb_box_set_child_align(TbBox          *box,
                       ClutterActor   *child,
                       TbBoxAlignment  x_align,
                       TbBoxAlignment  y_align)
{
  TbBoxChild *c;

  g_return_if_fail (TB_IS_BOX (box));
  g_return_if_fail (CLUTTER_IS_ACTOR (child));

  g_object_ref (child);

  c = box_child_find (box, child);

  if (c != NULL && box_child_set_align (c, x_align, y_align))
    clutter_actor_queue_relayout (CLUTTER_ACTOR (box));

  g_object_unref (child);
}

void
tb_box_set_fixed_child_align(TbBox              *box,
                             ClutterActor       *child,
                             TbBoxAlignment     fixed_x_align,
                             TbBoxAlignment     fixed_y_align)
{
  TbBoxChild *c;

  g_return_if_fail (TB_IS_BOX (box));
  g_return_if_fail (CLUTTER_IS_ACTOR (child));

  g_object_ref (child);

  c = box_child_find (box, child);

  if (c != NULL && box_child_set_fixed_align (c, fixed_x_align, fixed_y_align))
    clutter_actor_queue_relayout (CLUTTER_ACTOR (box));

  g_object_unref (child);
}
