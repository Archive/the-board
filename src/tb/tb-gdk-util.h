#ifndef __TB_GDK_UTIL_H__
#define __TB_GDK_UTIL_H__

#include <gtk/gtk.h>

GtkClipboard *tb_get_default_clipboard ();

#endif /* __TB_GDK_UTIL_H__ */
