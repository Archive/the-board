#ifndef __TB_SOUND_RECORDER_H__
#define __TB_SOUND_RECORDER_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define TB_TYPE_SOUND_RECORDER            (tb_sound_recorder_get_type ())
#define TB_SOUND_RECORDER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TB_TYPE_SOUND_RECORDER, TbSoundRecorder))
#define TB_IS_SOUND_RECORDER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TB_TYPE_SOUND_RECORDER))
#define TB_SOUND_RECORDER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  TB_TYPE_SOUND_RECORDER, TbSoundRecorderClass))
#define TB_IS_SOUND_RECORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  TB_TYPE_SOUND_RECORDER))
#define TB_SOUND_RECORDER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TB_TYPE_SOUND_RECORDER, TbSoundRecorderClass))

typedef struct _TbSoundRecorder          TbSoundRecorder;
typedef struct _TbSoundRecorderPrivate   TbSoundRecorderPrivate;
typedef struct _TbSoundRecorderClass     TbSoundRecorderClass;

typedef enum
{
  TB_SOUND_RECORDER_STATE_UNKNOWN   = 0,
  TB_SOUND_RECORDER_STATE_IDLE      = 1,
  TB_SOUND_RECORDER_STATE_RECORDING = 2,
  TB_SOUND_RECORDER_STATE_DONE      = 3,
  TB_SOUND_RECORDER_STATE_ERROR     = 4
} TbSoundRecorderState;

struct _TbSoundRecorder
{
  GObject parent_instance;

  TbSoundRecorderPrivate *priv;
};

struct _TbSoundRecorderClass
{
  GObjectClass parent_class;
};

GType    tb_sound_recorder_get_type     (void) G_GNUC_CONST;

void     tb_sound_recorder_start        (TbSoundRecorder *recorder);
void     tb_sound_recorder_stop         (TbSoundRecorder *recorder);

G_END_DECLS

#endif /* __TB_SOUND_RECORDER_H__ */
