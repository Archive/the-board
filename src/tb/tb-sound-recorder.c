#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <gst/gst.h>

#include "tb-enum-types.h"
#include "tb/tb-sound-recorder.h"

G_DEFINE_TYPE (TbSoundRecorder, tb_sound_recorder, G_TYPE_OBJECT);

#define TB_SOUND_RECORDER_GET_PRIVATE(obj)    \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), TB_TYPE_SOUND_RECORDER, TbSoundRecorderPrivate))

enum
{
  PROP_0,

  PROP_STATE,
  PROP_URI,
  PROP_DURATION
};

struct _TbSoundRecorderPrivate
{
  GstElement            *pipeline;
  GstBus                *bus;
  TbSoundRecorderState   state;
  char                  *uri;
  gint                   duration;
  guint                  tick_timeout_id;
};

static void tb_sound_recorder_destroy_pipeline (TbSoundRecorder *recorder);

static void
tb_sound_recorder_set_state (TbSoundRecorder      *recorder,
                             TbSoundRecorderState  state)
{
  TbSoundRecorderPrivate *priv;

  g_return_if_fail (TB_IS_SOUND_RECORDER (recorder));

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (priv->state == state)
    return;

  priv->state = state;

  g_object_notify (G_OBJECT (recorder), "state");
}

static void
tb_sound_recorder_set_uri (TbSoundRecorder *recorder,
                           const char      *uri)
{
  TbSoundRecorderPrivate *priv;

  g_return_if_fail (TB_IS_SOUND_RECORDER (recorder));
  g_return_if_fail (uri != NULL);

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (priv->uri &&
      !strcmp (priv->uri, uri))
    return;

  g_free (priv->uri);
  priv->uri = g_strdup (uri);

  if (priv->pipeline)
    tb_sound_recorder_destroy_pipeline (recorder);

  g_object_notify (G_OBJECT (recorder), "uri");
}

static void
tb_sound_recorder_reset_pipeline (TbSoundRecorder *recorder)
{
  TbSoundRecorderPrivate *priv;
  GstState state, pending;
  GstMessage *msg;

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (!priv->pipeline)
    return;

  gst_element_get_state (priv->pipeline, &state, &pending, 0);

  if (state == GST_STATE_NULL && pending == GST_STATE_VOID_PENDING)
    {
      return;
    }
  else if (state == GST_STATE_NULL && pending != GST_STATE_VOID_PENDING)
    {
      gst_element_set_state (priv->pipeline, GST_STATE_NULL);
      return;
    }

  gst_element_set_state (priv->pipeline, GST_STATE_READY);
  gst_element_get_state (priv->pipeline, NULL, NULL, -1);

  while ((msg = gst_bus_pop (priv->bus)))
    gst_bus_async_signal_func (priv->bus, msg, NULL);

  gst_element_set_state (priv->pipeline, GST_STATE_NULL);
}

static void
tb_sound_recorder_destroy_pipeline (TbSoundRecorder *recorder)
{
  TbSoundRecorderPrivate *priv;

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (priv->bus)
    {
      gst_bus_set_flushing (priv->bus, TRUE);
      gst_bus_remove_signal_watch (priv->bus);

      gst_object_unref (priv->bus);
      priv->bus = NULL;
    }

  if (priv->pipeline)
    {
      gst_element_set_state (priv->pipeline, GST_STATE_NULL);

      gst_object_unref (priv->pipeline);
      priv->pipeline = NULL;
    }

  if (priv->tick_timeout_id != 0)
    {
      g_source_remove (priv->tick_timeout_id);
      priv->tick_timeout_id = 0;
    }

  g_object_notify (G_OBJECT (recorder), "duration");
}

static gboolean
tb_sound_recorder_tick_timeout (gpointer user_data)
{
  TbSoundRecorderPrivate *priv;
  TbSoundRecorder *recorder;
  GstFormat format = GST_FORMAT_TIME;
  gint64 val = -1;
  gint secs;

  recorder = TB_SOUND_RECORDER (user_data);
  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  /* This check stops us from doing an unnecessary query */
  if (priv->state != TB_SOUND_RECORDER_STATE_RECORDING)
    return FALSE;

  if (gst_element_query_position (priv->pipeline, &format, &val) && val != -1)
    {
      secs = val / GST_SECOND;

      if (priv->duration != secs)
        {
          priv->duration = secs;
          g_object_notify (G_OBJECT (recorder), "duration");
        }
    }

  return TRUE;
}

static void
tb_sound_recorder_on_state_changed (GstBus          *bus,
                                    GstMessage      *msg,
                                    TbSoundRecorder *recorder)
{
  TbSoundRecorderPrivate *priv;
  GstState state;

  g_return_if_fail (TB_IS_SOUND_RECORDER (recorder));

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (msg->src != GST_OBJECT (priv->pipeline))
    return;

  gst_message_parse_state_changed (msg, NULL, &state, NULL);

  switch (state)
    {
    case GST_STATE_PLAYING:
      tb_sound_recorder_set_state (recorder, TB_SOUND_RECORDER_STATE_RECORDING);

      if (priv->tick_timeout_id == 0)
        {
          priv->tick_timeout_id =
            g_timeout_add (200,
                           tb_sound_recorder_tick_timeout,
                           recorder);
        }

      break;

    case GST_STATE_READY:
    case GST_STATE_PAUSED:
      tb_sound_recorder_set_state (recorder, TB_SOUND_RECORDER_STATE_IDLE);

      if (priv->tick_timeout_id != 0)
        {
          g_source_remove (priv->tick_timeout_id);
          priv->tick_timeout_id = 0;
        }
      break;

    default:
      /* Do nothing */
      break;
    }
}

static void
tb_sound_recorder_on_error (GstBus          *bus,
                            GstMessage      *msg,
                            TbSoundRecorder *recorder)
{
  tb_sound_recorder_reset_pipeline (recorder);
  tb_sound_recorder_set_state (recorder, TB_SOUND_RECORDER_STATE_ERROR);
}

static void
tb_sound_recorder_on_eos (GstBus          *bus,
                          GstMessage      *msg,
                          TbSoundRecorder *recorder)
{
  tb_sound_recorder_set_state (recorder, TB_SOUND_RECORDER_STATE_DONE);
  tb_sound_recorder_reset_pipeline (recorder);
}

static gboolean
tb_sound_recorder_ensure_pipeline (TbSoundRecorder *recorder)
{
  TbSoundRecorderPrivate *priv;
  GFile *file;
  GError *error;
  gchar *pipeline_desc;
  gchar *file_path;

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (priv->pipeline)
    return TRUE;

  if (priv->uri == NULL)
    {
      tb_sound_recorder_set_state (recorder, TB_SOUND_RECORDER_STATE_ERROR);
      return FALSE;
    }

  error = NULL;

  file = g_file_new_for_uri (priv->uri);
  file_path = g_file_get_path (file);

  pipeline_desc = g_strdup_printf("autoaudiosrc name=source ! "
                                  "audioconvert ! "
                                  "audioresample ! "
                                  "vorbisenc ! "
                                  "oggmux ! "
                                  "filesink location=\"%s\"",
                                  file_path);

  g_object_unref (file);
  g_free (file_path);

  priv->pipeline = gst_parse_launch (pipeline_desc, &error);

  g_free (pipeline_desc);

  if (error)
    {
      g_error_free (error);
      priv->pipeline = NULL;

      tb_sound_recorder_set_state (recorder, TB_SOUND_RECORDER_STATE_ERROR);
      return FALSE;
    }

  if (!gst_element_set_state (priv->pipeline, GST_STATE_READY))
    {
      g_object_unref (priv->pipeline);
      priv->pipeline = NULL;

      tb_sound_recorder_set_state (recorder, TB_SOUND_RECORDER_STATE_ERROR);
      return FALSE;
    }

  priv->bus = gst_element_get_bus (priv->pipeline);

  gst_bus_add_signal_watch (priv->bus);

  g_signal_connect (priv->bus,
                    "message::state-changed",
                    G_CALLBACK (tb_sound_recorder_on_state_changed),
                    recorder);

  g_signal_connect (priv->bus,
                    "message::error",
                    G_CALLBACK (tb_sound_recorder_on_error),
                    recorder);

  g_signal_connect (priv->bus,
                    "message::eos",
                    G_CALLBACK (tb_sound_recorder_on_eos),
                    recorder);

  return TRUE;
}

static void
tb_sound_recorder_finalize (GObject *gobject)
{
  G_OBJECT_CLASS (tb_sound_recorder_parent_class)->finalize (gobject);
}

static void
tb_sound_recorder_dispose (GObject *gobject)
{
  tb_sound_recorder_destroy_pipeline (TB_SOUND_RECORDER (gobject));

  G_OBJECT_CLASS (tb_sound_recorder_parent_class)->dispose (gobject);
}

static void
tb_sound_recorder_get_property (GObject    *gobject,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  TbSoundRecorderPrivate *priv;

  priv = TB_SOUND_RECORDER_GET_PRIVATE (gobject);

  switch (prop_id)
    {
    case PROP_STATE:
      g_value_set_enum (value, priv->state);
      break;

    case PROP_URI:
      g_value_set_string (value, priv->uri);
      break;

    case PROP_DURATION:
      g_value_set_double (value, priv->duration);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
tb_sound_recorder_set_property (GObject      *gobject,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  TbSoundRecorderPrivate *priv;

  priv = TB_SOUND_RECORDER_GET_PRIVATE (gobject);

  switch (prop_id)
    {
    case PROP_URI:
      tb_sound_recorder_set_uri (TB_SOUND_RECORDER (gobject),
                                 g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
tb_sound_recorder_class_init (TbSoundRecorderClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TbSoundRecorderPrivate));

  gobject_class->get_property = tb_sound_recorder_get_property;
  gobject_class->set_property = tb_sound_recorder_set_property;
  gobject_class->dispose = tb_sound_recorder_dispose;
  gobject_class->finalize = tb_sound_recorder_finalize;

  g_object_class_install_property
                 (gobject_class,
                  PROP_STATE,
                  g_param_spec_enum ("state",
                                     "State",
                                     "State of the sound recorder",
                                     TB_TYPE_SOUND_RECORDER_STATE,
                                     TB_SOUND_RECORDER_STATE_UNKNOWN,
                                     G_PARAM_READABLE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_URI,
                  g_param_spec_string ("uri",
                                       "URI",
                                       "URI to save sound to",
                                       NULL,
                                       G_PARAM_READWRITE |
                                       G_PARAM_CONSTRUCT));

  g_object_class_install_property
                 (gobject_class,
                  PROP_DURATION,
                  g_param_spec_double ("duration",
                                       "Duration",
                                       "Sound recording duration",
                                       0.0,
                                       G_MAXDOUBLE,
                                       0.0,
                                       G_PARAM_READABLE));
}

static void
tb_sound_recorder_init (TbSoundRecorder *recorder)
{
  TbSoundRecorderPrivate *priv;

  recorder->priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  recorder->priv->state = TB_SOUND_RECORDER_STATE_UNKNOWN;
  recorder->priv->uri = NULL;
  recorder->priv->pipeline = NULL;
  recorder->priv->bus = NULL;
  recorder->priv->duration = 0;
  recorder->priv->tick_timeout_id = 0;
}

void
tb_sound_recorder_start (TbSoundRecorder *recorder)
{
  TbSoundRecorderPrivate *priv;

  g_return_if_fail (TB_IS_SOUND_RECORDER (recorder));

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (tb_sound_recorder_ensure_pipeline (recorder))
    gst_element_set_state (priv->pipeline, GST_STATE_PLAYING);
}

void
tb_sound_recorder_stop (TbSoundRecorder *recorder)
{
  TbSoundRecorderPrivate *priv;

  g_return_if_fail (TB_IS_SOUND_RECORDER (recorder));

  priv = TB_SOUND_RECORDER_GET_PRIVATE (recorder);

  if (priv->pipeline == NULL)
    return;

  gst_element_send_event (priv->pipeline, gst_event_new_eos());
}
