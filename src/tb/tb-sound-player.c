#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

#include "tb-enum-types.h"
#include "tb/tb-sound-player.h"

G_DEFINE_TYPE (TbSoundPlayer, tb_sound_player, G_TYPE_OBJECT);

#define TB_SOUND_PLAYER_GET_PRIVATE(obj)    \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), TB_TYPE_SOUND_PLAYER, TbSoundPlayerPrivate))

#define TICK_TIMEOUT 0.5

enum
{
  PROP_0,

  PROP_PLAYING,
  PROP_STATE,
  PROP_PROGRESS,
  PROP_DURATION,
  PROP_URI
};

struct _TbSoundPlayerPrivate
{
  GstElement            *pipeline;
  GstBus                *bus;
  TbSoundPlayerState     state;
  char                  *uri;
  gboolean               playing;
  GstState               stacked_state;
  gdouble                stacked_progress;
  gdouble                target_progress;
  gdouble                duration;
  guint                  tick_timeout_id;

  guint                  in_seek : 1;
};

static void tb_sound_player_destroy_pipeline (TbSoundPlayer *player);
static gboolean tb_sound_player_ensure_pipeline (TbSoundPlayer *player);

static void
tb_sound_player_set_state (TbSoundPlayer      *player,
                           TbSoundPlayerState  state)
{
  TbSoundPlayerPrivate *priv;

  g_return_if_fail (TB_IS_SOUND_PLAYER (player));

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (priv->state == state)
    return;

  priv->state = state;

  g_object_notify (G_OBJECT (player), "state");
}

static void
tb_sound_player_set_uri (TbSoundPlayer *player,
                         const char    *uri)
{
  TbSoundPlayerPrivate *priv;

  g_return_if_fail (TB_IS_SOUND_PLAYER (player));

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (priv->uri && !strcmp (priv->uri, uri))
    return;

  g_free (priv->uri);
  priv->uri = g_strdup (uri);

  if (priv->pipeline)
    tb_sound_player_destroy_pipeline (player);

  tb_sound_player_ensure_pipeline (player);

  g_object_notify (G_OBJECT (player), "uri");
}

static void
tb_sound_player_set_progress (TbSoundPlayer *player,
                              gdouble        progress)
{
  TbSoundPlayerPrivate *priv;
  GstState pending;
  GstQuery *duration_q;
  gint64 position;

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (!priv->pipeline)
    return;

  priv->target_progress = progress;

  if (priv->in_seek)
    {
      priv->stacked_progress = progress;
      return;
    }

  gst_element_get_state (priv->pipeline, &priv->stacked_state, &pending, 0);

  if (pending)
    priv->stacked_state = pending;

  gst_element_set_state (priv->pipeline, GST_STATE_PAUSED);

  duration_q = gst_query_new_duration (GST_FORMAT_TIME);

  if (gst_element_query (priv->pipeline, duration_q))
    {
      gint64 duration = 0;

      gst_query_parse_duration (duration_q, NULL, &duration);

      position = progress * duration;
    }
  else
    position = 0;

  gst_query_unref (duration_q);

  gst_element_seek (priv->pipeline,
		    1.0,
		    GST_FORMAT_TIME,
		    GST_SEEK_FLAG_FLUSH,
		    GST_SEEK_TYPE_SET,
		    position,
		    GST_SEEK_TYPE_NONE, GST_CLOCK_TIME_NONE);

  priv->in_seek = TRUE;
  priv->stacked_progress = 0.0;
}

static gdouble
tb_sound_player_get_progress (TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;
  GstQuery *position_q, *duration_q;
  gdouble progress;

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (!priv->pipeline)
    return 0.0;

  if (priv->in_seek)
    {
      return priv->target_progress;
    }

  position_q = gst_query_new_position (GST_FORMAT_TIME);
  duration_q = gst_query_new_duration (GST_FORMAT_TIME);

  if (gst_element_query (priv->pipeline, position_q) &&
      gst_element_query (priv->pipeline, duration_q))
    {
      gint64 position, duration;

      position = duration = 0;

      gst_query_parse_position (position_q, NULL, &position);
      gst_query_parse_duration (duration_q, NULL, &duration);

      progress = CLAMP ((gdouble) position / (gdouble) duration, 0.0, 1.0);
    }
  else
    progress = 0.0;

  gst_query_unref (position_q);
  gst_query_unref (duration_q);

  return progress;
}

static void
tb_sound_player_query_duration (TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;
  GstFormat format = GST_FORMAT_TIME;
  gdouble new_duration, difference;
  gboolean success;
  gint64 duration;

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  success = gst_element_query_duration (priv->pipeline, &format, &duration);

  if (G_UNLIKELY (success != TRUE))
    return;

  new_duration = (gdouble) duration / GST_SECOND;

  difference = ABS (priv->duration - new_duration);

  if (difference > 1e-3)
    {
      priv->duration = new_duration;

      if (difference > 1.0)
        g_object_notify (G_OBJECT (player), "duration");
    }
}

static void
tb_sound_player_reset_pipeline (TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;
  GstState state, pending;
  GstMessage *msg;

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (!priv->pipeline)
    return;

  gst_element_get_state (priv->pipeline, &state, &pending, 0);

  if (state == GST_STATE_NULL && pending == GST_STATE_VOID_PENDING)
    {
      return;
    }
  else if (state == GST_STATE_NULL && pending != GST_STATE_VOID_PENDING)
    {
      gst_element_set_state (priv->pipeline, GST_STATE_NULL);
      return;
    }

  gst_element_set_state (priv->pipeline, GST_STATE_READY);
  gst_element_get_state (priv->pipeline, NULL, NULL, -1);

  while ((msg = gst_bus_pop (priv->bus)))
    gst_bus_async_signal_func (priv->bus, msg, NULL);

  gst_element_set_state (priv->pipeline, GST_STATE_NULL);

  g_object_notify (G_OBJECT (player), "duration");
  g_object_notify (G_OBJECT (player), "progress");
}

static void
tb_sound_player_destroy_pipeline (TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (priv->bus)
    {
      gst_bus_set_flushing (priv->bus, TRUE);
      gst_bus_remove_signal_watch (priv->bus);

      gst_object_unref (priv->bus);
      priv->bus = NULL;
    }

  if (priv->pipeline)
    {
      gst_element_set_state (priv->pipeline, GST_STATE_NULL);

      gst_object_unref (priv->pipeline);
      priv->pipeline = NULL;
    }

  if (priv->tick_timeout_id != 0)
    {
      g_source_remove (priv->tick_timeout_id);
      priv->tick_timeout_id = 0;
    }

  g_object_notify (G_OBJECT (player), "duration");
  g_object_notify (G_OBJECT (player), "progress");
}

static gboolean
tb_sound_player_tick_timeout (gpointer user_data)
{
  GObject *player = user_data;

  g_object_notify (player, "progress");

  return TRUE;
}

static void
tb_sound_player_on_state_changed (GstBus        *bus,
                                  GstMessage    *msg,
                                  TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;
  GstState state, old_state;

  g_return_if_fail (TB_IS_SOUND_PLAYER (player));

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (msg->src != GST_OBJECT (priv->pipeline))
    return;

  gst_message_parse_state_changed (msg, &old_state, &state, NULL);

  if (state == GST_STATE_PAUSED && old_state == GST_STATE_READY)
    tb_sound_player_query_duration (player);

  switch (state)
    {
    case GST_STATE_PLAYING:
      tb_sound_player_set_state (player, TB_SOUND_PLAYER_STATE_PLAYING);

      if (priv->tick_timeout_id == 0)
        {
          priv->tick_timeout_id =
            g_timeout_add (TICK_TIMEOUT * 1000,
                           tb_sound_player_tick_timeout,
                           player);
        }
      break;

    case GST_STATE_READY:
    case GST_STATE_PAUSED:
      tb_sound_player_set_state (player, TB_SOUND_PLAYER_STATE_IDLE);

      if (priv->tick_timeout_id != 0)
        {
          g_source_remove (priv->tick_timeout_id);
          priv->tick_timeout_id = 0;
        }
      break;

    default:
      /* Do nothing */
      break;
    }
}

static void
tb_sound_player_on_error (GstBus        *bus,
                          GstMessage    *msg,
                          TbSoundPlayer *player)
{
  tb_sound_player_reset_pipeline (player);
  tb_sound_player_set_state (player, TB_SOUND_PLAYER_STATE_ERROR);
}

static void
tb_sound_player_on_eos (GstBus        *bus,
                        GstMessage    *msg,
                        TbSoundPlayer *player)
{
  g_object_notify (G_OBJECT (player), "progress");

  tb_sound_player_set_state (player, TB_SOUND_PLAYER_STATE_DONE);
  tb_sound_player_reset_pipeline (player);
}

static void
tb_sound_player_on_async_done (GstBus        *bus,
                               GstMessage    *msg,
                               TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (priv->in_seek)
    {
      g_object_notify (G_OBJECT (player), "progress");

      priv->in_seek = FALSE;
      gst_element_set_state (priv->pipeline, priv->stacked_state);

      if (priv->stacked_progress)
        {
          tb_sound_player_set_progress (player, priv->stacked_progress);
        }
    }
}

static void
tb_sound_player_on_duration (GstBus        *bus,
                             GstMessage    *msg,
                             TbSoundPlayer *player)
{
  gint64 duration;

  gst_message_parse_duration (msg, NULL, &duration);

  if (G_UNLIKELY (duration != GST_CLOCK_TIME_NONE))
    return;

  tb_sound_player_query_duration (player);
}

static gboolean
tb_sound_player_ensure_pipeline (TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;
  GstElement *playbin;
  GError *error;

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (priv->pipeline)
    return TRUE;

  if (priv->uri == NULL)
    {
      tb_sound_player_set_state (player, TB_SOUND_PLAYER_STATE_ERROR);
      return FALSE;
    }

  error = NULL;

  priv->pipeline = gst_pipeline_new (NULL);

  playbin = gst_element_factory_make ("playbin2", NULL);

  if (playbin == NULL)
    {
      g_object_unref (priv->pipeline);
      priv->pipeline = NULL;

      tb_sound_player_set_state (player, TB_SOUND_PLAYER_STATE_ERROR);
      return FALSE;
    }

  g_object_set (playbin,
                "uri", priv->uri,
                NULL);

  gst_bin_add (GST_BIN (priv->pipeline), playbin);

  if (!gst_element_set_state (priv->pipeline, GST_STATE_READY))
    {
      g_object_unref (priv->pipeline);
      priv->pipeline = NULL;

      tb_sound_player_set_state (player, TB_SOUND_PLAYER_STATE_ERROR);
      return FALSE;
    }

  priv->bus = gst_element_get_bus (priv->pipeline);

  gst_bus_add_signal_watch (priv->bus);

  g_signal_connect (priv->bus,
                    "message::state-changed",
                    G_CALLBACK (tb_sound_player_on_state_changed),
                    player);

  g_signal_connect (priv->bus,
                    "message::error",
                    G_CALLBACK (tb_sound_player_on_error),
                    player);

  g_signal_connect (priv->bus,
                    "message::eos",
                    G_CALLBACK (tb_sound_player_on_eos),
                    player);

  g_signal_connect (priv->bus,
                    "message::async-done",
                    G_CALLBACK (tb_sound_player_on_async_done),
                    player);

  g_signal_connect (priv->bus,
                    "message::duration",
                    G_CALLBACK (tb_sound_player_on_duration),
                    player);

  /* Pause pipeline so that the file duration becomes
   * available as soon as possible */
  gst_element_set_state (priv->pipeline, GST_STATE_PAUSED);

  return TRUE;
}

void
tb_sound_player_set_playing (TbSoundPlayer *player,
                             gboolean       playing)
{
  TbSoundPlayerPrivate *priv;
  GstState state;

  g_return_if_fail (TB_IS_SOUND_PLAYER (player));

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (playing)
    state = GST_STATE_PLAYING;
  else
    state = GST_STATE_PAUSED;

  if (tb_sound_player_ensure_pipeline (player))
    gst_element_set_state (priv->pipeline, state);

  g_object_notify (G_OBJECT (player), "playing");
  g_object_notify (G_OBJECT (player), "progress");
}

static gboolean
tb_sound_player_get_playing (TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;
  GstState state, pending;
  gboolean playing;

  g_return_if_fail (TB_IS_SOUND_PLAYER (player));

  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  if (!priv->pipeline)
    return FALSE;

  gst_element_get_state (priv->pipeline, &state, &pending, 0);

  if (pending)
    playing = (pending == GST_STATE_PLAYING);
  else
    playing = (state == GST_STATE_PLAYING);

  return playing;
}

static void
tb_sound_player_finalize (GObject *gobject)
{
  G_OBJECT_CLASS (tb_sound_player_parent_class)->finalize (gobject);
}

static void
tb_sound_player_dispose (GObject *gobject)
{
  tb_sound_player_destroy_pipeline (TB_SOUND_PLAYER (gobject));

  G_OBJECT_CLASS (tb_sound_player_parent_class)->dispose (gobject);
}

static void
tb_sound_player_get_property (GObject    *gobject,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  TbSoundPlayer *player;
  TbSoundPlayerPrivate *priv;

  player = TB_SOUND_PLAYER (gobject);
  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  switch (prop_id)
    {
    case PROP_PLAYING:
      g_value_set_boolean (value,
                           tb_sound_player_get_playing (player));
      break;

    case PROP_STATE:
      g_value_set_enum (value, priv->state);
      break;

    case PROP_PROGRESS:
      g_value_set_double (value,
                          tb_sound_player_get_progress (player));
      break;

    case PROP_DURATION:
      g_value_set_double (value, priv->duration);
      break;

    case PROP_URI:
      g_value_set_string (value, priv->uri);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
tb_sound_player_set_property (GObject      *gobject,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  TbSoundPlayer *player;
  TbSoundPlayerPrivate *priv;

  player = TB_SOUND_PLAYER (gobject);
  priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  switch (prop_id)
    {
    case PROP_PLAYING:
      tb_sound_player_set_playing (player,
                                   g_value_get_boolean (value));
      break;

    case PROP_PROGRESS:
      tb_sound_player_set_progress (player,
                                    g_value_get_double (value));
      break;

    case PROP_URI:
      tb_sound_player_set_uri (player,
                               g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
      break;
    }
}

static void
tb_sound_player_class_init (TbSoundPlayerClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TbSoundPlayerPrivate));

  gobject_class->get_property = tb_sound_player_get_property;
  gobject_class->set_property = tb_sound_player_set_property;
  gobject_class->dispose = tb_sound_player_dispose;
  gobject_class->finalize = tb_sound_player_finalize;

  g_object_class_install_property
                 (gobject_class,
                  PROP_PLAYING,
                  g_param_spec_boolean ("playing",
                                        "Playing",
                                        "Whether player is playing or not",
                                        FALSE,
                                        G_PARAM_READWRITE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_PROGRESS,
                  g_param_spec_double ("progress",
                                       "Progress",
                                       "Player's playback progress",
                                       0.0,
                                       1.0,
                                       0.0,
                                       G_PARAM_READWRITE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_DURATION,
                  g_param_spec_double ("duration",
                                       "Duration",
                                       "Sound duration",
                                       0.0,
                                       G_MAXDOUBLE,
                                       0.0,
                                       G_PARAM_READABLE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_STATE,
                  g_param_spec_enum ("state",
                                     "State",
                                     "State of the sound player",
                                     TB_TYPE_SOUND_PLAYER_STATE,
                                     TB_SOUND_PLAYER_STATE_UNKNOWN,
                                     G_PARAM_READABLE));

  g_object_class_install_property
                 (gobject_class,
                  PROP_URI,
                  g_param_spec_string ("uri",
                                       "URI",
                                       "URI to play sound from",
                                       NULL,
                                       G_PARAM_READWRITE |
                                       G_PARAM_CONSTRUCT));
}

static void
tb_sound_player_init (TbSoundPlayer *player)
{
  TbSoundPlayerPrivate *priv;

  player->priv = TB_SOUND_PLAYER_GET_PRIVATE (player);

  player->priv->state = TB_SOUND_PLAYER_STATE_UNKNOWN;
  player->priv->playing = FALSE;
  player->priv->uri = NULL;
  player->priv->pipeline = NULL;
  player->priv->bus = NULL;
  player->priv->stacked_progress = 0.0;
  player->priv->duration = 0.0;
  player->priv->tick_timeout_id = 0;
}
