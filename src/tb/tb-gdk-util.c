
#include <gtk/gtk.h>

#include "tb/tb-gdk-util.h"

/* This is only necessary because the GDK_SELECTION_CLIPBOARD
 * GdkAtom is not properly gir-introspected yet */

/**
 * tb_get_default_clipboard:
 * 
 * Returns: (transfer none): the default clipboard.
 */
GtkClipboard *
tb_get_default_clipboard()
{
  return gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
}
