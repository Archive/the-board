#ifndef __TB_GIO_UTIL_H__
#define __TB_GIO_UTIL_H__

#include <glib.h>
#include <gio/gio.h>

void     tb_g_file_replace_contents_async(GFile               *file,
                                          GByteArray          *contents,
                                          GCancellable        *cancellable,
                                          GAsyncReadyCallback  callback);

char    *tb_g_file_load_contents_finish  (GFile               *file,
                                          GAsyncResult        *result,
                                          GError             **error);

void     tb_g_file_delete_async          (GFile               *file,
                                          GCancellable        *cancellable,
                                          GAsyncReadyCallback  callback,
                                          gpointer             user_data);

gboolean tb_g_file_delete_finish         (GFile               *file,
                                          GAsyncResult        *res,
                                          GError             **error);

#endif /* __TB_GIO_UTIL_H__ */
