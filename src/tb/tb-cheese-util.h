#ifndef __TB_CHEESE_UTIL_H__
#define __TB_CHEESE_UTIL_H__

#include <glib.h>
#include <gio/gio.h>
#include <cheese/cheese-camera.h>

void     tb_cheese_camera_start_async (CheeseCamera        *camera,
                                       GAsyncReadyCallback  callback);

gboolean tb_cheese_camera_start_finish  (CheeseCamera      *camera,
                                         GAsyncResult      *res,
                                         GError           **error);

#endif /* __TB_CHEESE_UTIL_H__ */
