#include <glib-object.h>

#include "tb/tb-gobject-util.h"

/* This is only necessary because the g_signal_stop_emission_by_name
 * is not working well in gjs yet */

void
tb_signal_stop_emission_by_name(GObject     *object,
                                const gchar *detailed_signal)
{
    g_signal_stop_emission_by_name(object, detailed_signal);
}
