#ifndef __TB_MX_UTIL_H__
#define __TB_MX_UTIL_H__

#include <mx/mx.h>

MxPadding *tb_mx_stylable_get_padding (MxStylable *stylable);

#endif /* __TB_MX_UTIL_H__ */
