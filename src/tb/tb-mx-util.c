
#include <mx/mx.h>

#include "tb/tb-mx-util.h"

/* This is only necessary because the mx_stylable_get_property 
 * is not properly gir-introspected yet */

/**
 * tb_mx_stylable_get_padding:
 * 
 * Returns: (transfer full): the stylable padding.
 */
MxPadding *
tb_mx_stylable_get_padding (MxStylable *stylable)
{
  MxPadding *padding;

  mx_stylable_get (stylable,
                   "padding", &padding,
                   NULL);

  return padding;
}
