var _THE_BOARD_URL = "http://localhost:2010/";

function callMethod(methodName, args, onLoad, onError) {
    var req = new XMLHttpRequest();

    var url = _THE_BOARD_URL + methodName;

    if (args) {
        url += "?";

        var first = true;
        for (argName in args) {
            url += (first ? "" : "&") +
                   argName + "=" + args[argName];

            first = false;
        }
    }

    req.open("POST", encodeURI(url), true);

    var onStateChange = function() {
        if (req.readyState == 4 && req.status == 200) {
            var json;

            if (req.responseText) {
                json = JSON.parse(req.responseText);
            } else {
                json = {};
            }

            onLoad(json);
        }
    }

    var onErrorInternal = function() {
        onError();
    }

    if (onLoad) {
        req.onreadystatechange = onStateChange;
    }

    if (onError) {
        req.onerror = onErrorInternal;
    }

    req.send(null);
}

function showNotification(title, description) {
    try {
      Cc['@mozilla.org/alerts-service;1'].
      getService(Ci.nsIAlertsService).
      showAlertNotification(null,
                            title,
                            description,
                            false, '', null);
    } catch(e) {
      // Prevents runtime error on platforms
      // that don't implement nsIAlertsService
    }
}

function showErrorTheBoardNotRunning() {
    var title = 'The Board is not running';
    var description = 'The Board must be running before ' +
                      'you start adding content to it.';

    showNotification(title, description);
}

function showErrorDownloadFailed() {
    var title = 'Oops!';
    var description = 'The Board could not download ' +
                      'this file. Check your internet connection ' +
                      'and try again?';

    showNotification(title, description);
}

var userDirs = null;

function downloadFile(fileUrl, dirType, onComplete, onError) {
    if (!userDirs) {
        callMethod("getUserDirs", null,
                   function(json) {
                       userDirs = json;
                       downloadFile(fileUrl, dirType, onComplete, onError);
                   },
                   showErrorTheBoardNotRunning);

        return;
    }

    var dm = Cc["@mozilla.org/download-manager;1"].
             getService(Ci.nsIDownloadManager);

    var ioService = Cc["@mozilla.org/network/io-service;1"].
                    getService(Ci.nsIIOService);

    var uri = ioService.newURI(fileUrl, null, null);
    var url = uri.QueryInterface(Ci.nsIURL);

    var msrv = Components.classes["@mozilla.org/mime;1"].
               getService(Ci.nsIMIMEService);

    var type = msrv.getTypeFromURI(uri);
    var mime = msrv.getFromTypeAndExtension(type, "");

    if (!(dirType in userDirs)) {
        if (onError) {
            onError();
        }
        return;
    }

    var targetDir = Cc["@mozilla.org/file/local;1"].
                    createInstance(Ci.nsILocalFile);

    targetDir.initWithPath(userDirs[dirType]);

    if (!targetDir.exists()) {
        targetDir.create(Ci.nsIFile.DIRECTORY_TYPE, 0755);
    }

    targetFile = targetDir.clone();
    targetFile.append(url.fileName);

    var index = 0;

    while (targetFile.exists()) {
        index++;

        targetFile = targetDir.clone();

        targetFile.append(url.fileBaseName +
                          " (" + index + ")." +
                          url.fileExtension);
    }

    var fileUri = ioService.newFileURI(targetFile);

    var persist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"].
                  createInstance(Ci.nsIWebBrowserPersist);

    persist.persistFlags =
        Ci.nsIWebBrowserPersist.PERSIST_FLAGS_BYPASS_CACHE |
        Ci.nsIWebBrowserPersist.PERSIST_FLAGS_CLEANUP_ON_FAILURE |
        Ci.nsIWebBrowserPersist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;

    var dl = dm.addDownload(dm.DOWNLOAD_TYPE_DOWNLOAD,
                            uri, fileUri,
                            "The Board (" + url.fileName + ")",
                            mime,
                            Math.round(Date.now() * 1000),
                            null,
                            persist);

    let listener = {
        onStateChange : function() {},
        onProgressChange : function() {},
        onSecurityChange : function() {},

        onDownloadStateChange : function(state, aDownload) {
            if (aDownload != dl) {
                return;
            }

            switch (aDownload.state) {
            case Ci.nsIDownloadManager.DOWNLOAD_FINISHED:
                if (onComplete) {
                    onComplete(targetFile.path, targetFile.leafName);
                }
                break;
            case Ci.nsIDownloadManager.DOWNLOAD_FAILED:
            case Ci.nsIDownloadManager.DOWNLOAD_CANCELED:
                if (onError) {
                    onError();
                }
                break;
            default:
                // do nothing
                break;
            }
        }
    };

    dm.addListener(listener);

    persist.progressListener =
        dl.QueryInterface(Ci.nsIWebProgressListener);

    persist.saveURI(dl.source, null, null, null, null, dl.targetFile);

    return targetFile.path;
}

function addPhotoToTheBoard(imageUrl) {
    var args = { id: "photo",
                 link: window.content.location.href };

    var onFileDownloaded = function(filePath, filename) {
        args.imageFilename = filePath;
        args.text = filename;

        callMethod("addThing", args,
                   null, showErrorTheBoardNotRunning);
    };

    downloadFile(imageUrl, 'pictures',
                 onFileDownloaded,
                 showErrorDownloadFailed);
}

function addVideoToTheBoard(videoUrl) {
    var args = { id: "video",
                 link: window.content.location.href };

    var onFileDownloaded = function(filePath, filename) {
        args.videoFilename = filePath;
        args.text = filename;

        callMethod("addThing", args,
                   null, showErrorTheBoardNotRunning);
    };

    downloadFile(videoUrl, 'videos',
                 onFileDownloaded,
                 showErrorDownloadFailed);
}

function addNoteToTheBoard(text) {
    var args = { id: "note",
                 text: text,
                 link: window.content.location.href };

    callMethod("addThing", args,
               null, showErrorTheBoardNotRunning);
}

function addLabelToTheBoard(text) {
    var args = { id: "label",
                 text: text,
                 link: window.content.location.href };

    callMethod("addThing", args,
               null, showErrorTheBoardNotRunning);
}

function jsdump(str) {
    Cc['@mozilla.org/consoleservice;1']
                    .getService(Ci.nsIConsoleService)
                                .logStringMessage(str);
}

function showHideMenuItem() {
    var menuItem = document.getElementById("add-to-the-board");

    menuItem.hidden = !gContextMenu.isTextSelected &&
                      !gContextMenu.onImage &&
                      !gContextMenu.onVideo &&
                      !gContextMenu.onLink;
}

function onMenuClick(info, tab) {
    if (gContextMenu.onImage) {
        addPhotoToTheBoard(document.popupNode.src);
    } else if (gContextMenu.onVideo) {
        addVideoToTheBoard(document.popupNode.currentSrc);
    } else if (gContextMenu.onLink) {
        addLabelToTheBoard(gContextMenu.linkURL);
    } else if (gContextMenu.isTextSelected) {
        var focusedWindow = document.commandDispatcher.focusedWindow;
        var selection = focusedWindow.getSelection().toString();

        addNoteToTheBoard(selection);
    }
}

function init() {
    var menu = document.getElementById("contentAreaContextMenu");
    menu.addEventListener("popupshowing", showHideMenuItem, false);
}

window.addEventListener("load", init, false);
