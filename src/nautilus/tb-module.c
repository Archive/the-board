#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>

#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-column-provider.h>

#include "tb-nautilus.h"

void
nautilus_module_initialize (GTypeModule*module)
{
  tb_nautilus_register_type (module);

  bindtextdomain (GETTEXT_PACKAGE, THE_BOARD_LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
}

void
nautilus_module_shutdown (void)
{
}

void 
nautilus_module_list_types (const GType **types,
                            int          *num_types)
{
  static GType type_list[1];

  type_list[0] = TB_TYPE_NAUTILUS;
  *types = type_list;
  *num_types = 1;
}
