#ifndef TB_NAUTILUS_H
#define TB_NAUTILUS_H 

#include <glib-object.h>

G_BEGIN_DECLS

#define TB_TYPE_NAUTILUS  (tb_nautilus_get_type ())
#define TB_NAUTILUS(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), TB_TYPE_NAUTILUS, TbNautilus))
#define TB_IS_NAUTILUS(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), TB_TYPE_NAUTILUS))

typedef struct _TbNautilus        TbNautilus;
typedef struct _TbNautilusClass   TbNautilusClass;
typedef struct _TbNautilusPrivate TbNautilusPrivate;

struct _TbNautilus
{
  GObject parent;

  TbNautilusPrivate *priv;
};

struct _TbNautilusClass
{
  GObjectClass parent_class;
};

GType tb_nautilus_get_type      (void);

void  tb_nautilus_register_type (GTypeModule *module);

G_END_DECLS

#endif /* TB_NAUTILUS_H */
