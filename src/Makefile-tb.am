tb_cflags = \
    -I$(top_srcdir)/src \
    -DPREFIX=\"$(prefix)\" \
    -DLIBDIR=\"$(libdir)\" \
    -DG_LOG_DOMAIN=\"TheBoard\" \
    -DG_DISABLE_DEPRECATED \
    $(TB_CFLAGS)

if HAVE_CHEESE
tb_cflags += $(CHEESE_CFLAGS)
endif

if HAVE_LIBSOUP
tb_cflags += $(LIBSOUP_CFLAGS)
endif

tb_built_sources = \
    tb-enum-types.h \
    tb-enum-types.c

BUILT_SOURCES += $(tb_built_sources)

THE_BOARD_STAMP_FILES = stamp-tb-marshal.h stamp-tb-enum-types.h

tb_source_h = \
    tb/tb-box.h \
    tb/tb-gio-util.h \
    tb/tb-gdk-util.h \
    tb/tb-gobject-util.h \
    tb/tb-mx-util.h \
    tb/tb-sound-player.h \
    tb/tb-sound-recorder.h

if HAVE_CHEESE
tb_source_h += tb/tb-cheese-util.h
endif

if HAVE_LIBSOUP
tb_source_h += tb/tb-soup-util.h
endif

tb_source_c = \
    tb/tb-box.c \
    tb/tb-gio-util.c \
    tb/tb-gdk-util.c \
    tb/tb-gobject-util.c \
    tb/tb-mx-util.c \
    tb/tb-sound-player.c \
    tb/tb-sound-recorder.c

if HAVE_CHEESE
tb_source_c += tb/tb-cheese-util.c
endif

if HAVE_LIBSOUP
tb_source_c += tb/tb-soup-util.c
endif

tb-enum-types.h: stamp-tb-enum-types.h Makefile
	@true

stamp-tb-enum-types.h: $(tb_source_h) tb/tb-enum-types.h.in
	$(AM_V_GEN) ( cd $(srcdir) && \
	$(GLIB_MKENUMS) \
	  --template $(srcdir)/tb/tb-enum-types.h.in \
 	  $(tb_source_h) ) >> xgen-beth && \
 	(cmp -s xgen-beth tb-enum-types.h || cp xgen-beth tb-enum-types.h) && \
 	rm -f xgen-beth && \
 	echo timestamp > $(@F)

tb-enum-types.c: stamp-tb-enum-types.h tb/tb-enum-types.c.in
	$(AM_V_GEN) ( cd $(srcdir) && \
	$(GLIB_MKENUMS) \
	  --template $(srcdir)/tb/tb-enum-types.c.in \
	  $(tb_source_h) ) >> xgen-betc && \
	cp xgen-betc tb-enum-types.c && \
	rm -f xgen-betc

lib_LTLIBRARIES += libthe-board-1.0.la

libthe_board_1_0_la_LIBADD = \
	$(TB_LIBS)

if HAVE_CHEESE
libthe_board_1_0_la_LIBADD += $(CHEESE_LIBS)
endif

if HAVE_LIBSOUP
libthe_board_1_0_la_LIBADD += $(LIBSOUP_LIBS)
endif

libthe_board_1_0_la_SOURCES = \
    $(tb_source_c) \
    $(tb_source_h) \
    $(tb_built_sources)

libthe_board_1_0_la_CPPFLAGS = $(tb_cflags)

CLEANFILES += $(THE_BOARD_STAMP_FILES) $(BUILT_SOURCES)

EXTRA_DIST += \
    tb/tb-enum-types.h.in \
    tb/tb-enum-types.c.in

INTROSPECTION_GIRS += TheBoard-1.0.gir

TheBoard-1.0.gir: libthe-board-1.0.la Makefile
TheBoard_1_0_gir_NAMESPACE = TheBoard
TheBoard_1_0_gir_VERSION = 1.0
TheBoard_1_0_gir_LIBS = libthe-board-1.0.la
TheBoard_1_0_gir_CFLAGS = $(AM_CPPFLAGS) $(tb_cflags)
TheBoard_1_0_gir_SCANNERFLAGS = --warn-all --symbol-prefix=tb --identifier-prefix=Tb
TheBoard_1_0_gir_INCLUDES = \
    Clutter-1.0 \
    GdkPixbuf-2.0 \
    Gtk-3.0 \
    Mx-1.0

if HAVE_CHEESE
TheBoard_1_0_gir_INCLUDES += Cheese-3.0
endif

if HAVE_LIBSOUP
TheBoard_1_0_gir_INCLUDES += Soup-2.4
endif

TheBoard_1_0_gir_FILES = \
    $(addprefix $(srcdir)/,$(tb_source_h)) \
    $(addprefix $(srcdir)/,$(tb_source_c)) \
    $(srcdir)/tb-enum-types.h
