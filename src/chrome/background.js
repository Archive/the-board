var _THE_BOARD_URL = "http://localhost:2010/";

function callMethod(methodName, args, onLoad, onError) {
    var req = new XMLHttpRequest();

    var url = _THE_BOARD_URL + methodName;

    var fileData = args.fileData;
    delete args.fileData;

    if (args) {
        url += "?";

        var first = true;
        for (argName in args) {
            url += (first ? "" : "&") +
                   argName + "=" + args[argName];

            first = false;
        }
    }

    req.open("POST", encodeURI(url), true);

    var postContent = null;

    if (fileData) {
        var boundaryString = "AaBbCcX30";
        var boundary = "--" + boundaryString;

        req.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + boundaryString);

        postContent =
            boundary + "\r\n" +
            "Content-Disposition: form-data; name=\"file\"; filename=\"uploaded-file\"\r\n" +
            "Content-Type: application/octet-stream\r\n" +
            "\r\n" +
            fileData + "\r\n" +
            boundary+"--\r\n";
    }

    var onStateChange = function() {
        if (req.readyState == 4 && req.status == 200) {
            var json;

            if (req.responseText) {
                json = JSON.parse(req.responseText);
            } else {
                json = {};
            }

            onLoad(json);
        }
    }

    var onErrorInternal = function() {
        onError();
    }

    if (onLoad) {
        req.onreadystatechange = onStateChange;
    }

    if (onError) {
        req.onerror = onErrorInternal;
    }

    req.send(postContent);
}

var errorNotification = null;

function closeErrorNofification() {
    if (!errorNotification) {
        return;
    }

    errorNotification.cancel();
    errorNotification = null;
}

function showErrorNotification() {
    if (errorNotification) {
        return;
    }

    errorNotification =
        webkitNotifications.createNotification('icon-48.png',
                                               'The Board is not running',
                                               'The Board must be running before ' +
                                               'you start adding content to it.');

    errorNotification.show();

    setTimeout(closeErrorNofification, 3000);
}

function addPhotoToTheBoard(imageUrl) {
    var args = { id: "photo" };

    var imageReq = new XMLHttpRequest();

    imageReq.open("GET", imageUrl, true);
    imageReq.overrideMimeType("text/plain; charset=x-user-defined");

    var onImageStateChange = function() {
        if (imageReq.readyState == 4 && imageReq.status == 200) {
            if (imageReq.responseText) {
                args.fileData = "";

                // Remove the text-related bits from image data
                for (var i=0; i <= imageReq.responseText.length - 1; i++) {
                    args.fileData +=
                        String.fromCharCode(imageReq.responseText.charCodeAt(i) & 0xff);
                }

                chrome.tabs.getSelected(null, function(tab) {
                    args.link = tab.url;
                    callMethod("addThing", args,
                               null, showErrorNotification);
                });
            }
        }
    }

    var onImageError = function() {
        // FIXME: handle errors here. Show a notification?
    }

    imageReq.onreadystatechange = onImageStateChange;
    imageReq.onerror = onImageError;

    imageReq.send(null);
}

function addNoteToTheBoard(text) {
    var args = { id: "note",
                 text: text };

    chrome.tabs.getSelected(null, function(tab) {
        args.link = tab.url;
        callMethod("addThing", args,
                   null, showErrorNotification);
    });
}

function addLabelToTheBoard(text) {
    var args = { id: "label",
                 text: text };

    chrome.tabs.getSelected(null, function(tab) {
        args.link = tab.url;
        callMethod("addThing", args,
                   null, showErrorNotification);
    });
}

function onMenuClick(info, tab) {
    // FIXME: Chrome doesn't handle binary uploads well
    // at the moment, so we're not doing anything on images.
    // Once Chrome starts working, enable "image" context
    // when creating the context menu and call addPhotoToTheBoard
    // when srcUrl is defined here.

    if ('linkUrl' in info) {
        addLabelToTheBoard(info.linkUrl);
    } else if ('selectionText' in info) {
        addNoteToTheBoard(info.selectionText);
    }
}

var contextMenuItemId = null;

function addContextMenuItem() {
    if (contextMenuItemId) {
        return;
    }

    contextMenuItemId =
        chrome.contextMenus.create({ title: "Add to The Board",
                                     contexts: ["selection", "link"],
                                     onclick: onMenuClick });
}

function onBrowserActionClick(tab) {
    addLabelToTheBoard(tab.url);
}

function addBrowserActionHandler() {
    chrome.browserAction.onClicked.addListener(onBrowserActionClick);
}

addContextMenuItem();
addBrowserActionHandler();
