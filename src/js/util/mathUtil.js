function clamp(num, min, max) {
    if (min > max) {
        throw new Error("min must be less than max");
    }

    var result = num;

    if (num < min)
        result = min;
    else if (num > max)
        result = max;

    return result;
}
