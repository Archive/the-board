// standard imports
const ByteArray = imports.byteArray;
const Lang = imports.lang;

// gi imports
const Soup = imports.gi.Soup;
const TheBoard = imports.gi.TheBoard;

// util imports
const JSON = imports.util.json;

function exportObject(port, iface, object) {
    log('Http: exporting object on port ' + port);

    object._httpServer = new Soup.Server({ port: port });

    let handleRequest = function(server, msg, path, query, client, method) {
        let args = {};

        // Only accept local http requests
        if (client.get_host() != "127.0.0.1") {
            return;
        }

        if (query) {
            Lang.copyProperties(query, args);
        }

        if (msg.requestHeaders.get_content_type(null) == "multipart/form-data") {
            let [params, filename, contentType, buffer] =
                Soup.form_decode_multipart(msg, "file");
            args.fileData = TheBoard.soup_buffer_get_data(buffer);
        }

        let result = object[method.name].apply(object, [args]);

        if (result) {
            let resultStr = JSON.JSON.stringify(result, null, 4);

            msg.set_response("text/plain",
                             Soup.MemoryUse.COPY,
                             resultStr,
                             resultStr.length);
        }

        msg.set_status(Soup.KnownStatusCode.OK);
    }

    if ('methods' in iface) {
        let methods = iface.methods;

        for (let i = 0; i < methods.length; ++i) {
            let method = methods[i];

            if (!('name' in method))
                throw new Error("Method definition must have a name");

            // Only export json methods
            if (('inSignature' in method && method.inSignature != "a{sv}") ||
                ('outSignature' in method && method.outSignature != "a{sv}")) {
                continue;
            }

            log('Http: exporting method ' + method.name);

            object._httpServer.add_handler("/" + method.name,
                                           Lang.bind(object, handleRequest, method),
                                           null, null);
        }
    }

    object._httpServer.run_async();
}
