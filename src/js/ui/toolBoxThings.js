// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;

// gi imports
const Mx = imports.gi.Mx;

// ui imports
const ToolBox = imports.ui.toolBox;

const _GRID_SPACING = 10;

function ToolBoxThings(args) {
    this._init(args);
}

ToolBoxThings.prototype = {
    __proto__: ToolBox.ToolBox.prototype,

    _init : function(args) {
        args = args || {};

        args.title = Gettext.gettext("Add");

        ToolBox.ToolBox.prototype._init.apply(this, [args]);

        this._addButtons();

        // we're loaded after adding all buttons 
        this.loaded = true;
    },

    _loadThings : function() {
        this._things = [];

        let thingModules = imports.ui.things;

        for (let thingId in thingModules) {
            let thingModule = thingModules[thingId];

            if (!('create' in thingModule)) {
                continue;
            }

            this._things.push({ name: thingModule.NAME,
                                id: thingId });
        }

        // FIXME: allow loading third-party thing implementations
        // from a user directory too
    },

    _addButtons : function() {
        this._loadThings();

        for (let i = 0; i < this._things.length; ++i) {
            let thing = this._things[i];

            let onClicked = Lang.bind(this,
                                      this._onThingButtonClicked,
                                      thing);

            this.addButton({ label: thing.name,
                             onClicked: onClicked });
        }
    },

    _onThingButtonClicked : function(button, thing) {
        this.emit("action",
                  "addThing",
                  { thingId: thing.id });
    },

    destroy : function() {
        ToolBox.ToolBox.prototype.destroy.apply(this, []);
    }
}
