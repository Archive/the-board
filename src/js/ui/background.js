// gi imports
const TheBoard = imports.gi.TheBoard;

function Background(args) {
    this._init(args);
}

Background.prototype = {
    _init : function(args) {
        args = args || {};

        if ('id' in args) {
            this._id = args.id;
        } else {
            throw new Error("Background id is required");
        }

        this._content = null;

        this._createMainBox();

        if ('content' in args) {
            this._setContent(args.content);
        }
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               name: "background-main-box" });
    },

    _setContent : function(content) {
        if (this._content) {
            this._mainBox.remove_actor(this._content.contentActor);
        }

        this._content = content;

        if (this._content) {
            this._mainBox.append(this._content.contentActor,
                                 TheBoard.BoxPackFlags.EXPAND);
        }
    },

    destroy : function() {
        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get id() {
        return this._id;
    },

    get mainBox() {
        // this getter is supposed to be used by subclasses
        // in case they want to set properties of main box.
        // We use a duplicate getter here to provide a more
        // obvious semantics when accessing main box instead
        // of the standard actor.
        return this._mainBox;
    },

    get actor() {
        return this._mainBox;
    }
}

function createBackgroundFromId(backgroundId) {
    if (!backgroundId) {
        backgroundId = 'cork';
    }

    let constructorModule = imports.ui.backgrounds[backgroundId];
    return constructorModule.create({ id: backgroundId });
}
