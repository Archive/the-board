// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;

// ui imports
const Tiled = imports.ui.backgrounds.tiled;

// util imports
const Path = imports.util.path;

const NAME = Gettext.gettext("Fabric");

function create(args) {
    args = args || {};

    args.bgFilename = Path.BACKGROUNDS_DATA_DIR +
                      "fabric/bg.jpg";

    return new Tiled.TiledBackground(args);
}
