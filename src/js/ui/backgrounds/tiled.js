// standard imports
const Lang = imports.lang;

// gi imports
const TheBoard = imports.gi.TheBoard;
const Clutter = imports.gi.Clutter;

// ui imports
const Background = imports.ui.background;

// This background module doesn't have a create() function
// as it's supposed to only be subclassed to implement
// concrete background implementations

function TiledBackground(args) {
    this._init(args);
}

TiledBackground.prototype = {
    __proto__: Background.Background.prototype,

    _init : function(args) {
        args = args || {};

        if ('bgFilename' in args) {
            this._bgFilename = args.bgFilename;
        } else {
            throw new Error("TiledBackground bgFilename is required");
        }

        Background.Background.prototype._init.apply(this, [args]);

        this._setBg();
    },

    _setBg : function() {
        this._bgTexture =
            new Clutter.Texture({ loadAsync: true,
                                  repeatX: true,
                                  repeatY: true });

        this._bgTexture.filename = this._bgFilename;

        this.mainBox.append(this._bgTexture,
                            TheBoard.BoxPackFlags.EXPAND);
    }
}
