// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Tweener = imports.tweener.tweener;

// gi imports
const Clutter = imports.gi.Clutter;
const GIO = imports.gi.Gio;
const Gdk = imports.gi.Gdk;
const Gtk = imports.gi.Gtk;
const GtkClutter = imports.gi.GtkClutter;
const Mx = imports.gi.Mx;
const TheBoard = imports.gi.TheBoard;

// ui imports
const MainToolbar = imports.ui.mainToolbar;
const Page = imports.ui.page;
const PhotoThing = imports.ui.things.photo;
const SelectionToolbar = imports.ui.selectionToolbar;
const Thing = imports.ui.thing;
const ToolBox = imports.ui.toolBox;
const Toolbar = imports.ui.toolbar;

// model imports
const PageManager = imports.model.pageManager;

const _LAYER_PAGE    = 0.1;
const _LAYER_TOOLBAR = 0.2;

const _SHOW_SPINNER_TIME = 0.5;
const _HIDE_SPINNER_TIME = 0.5;

const _SET_CURRENT_PAGE_TIME = 0.4;
const _SET_NEXT_PAGE_TRANSITION = 'easeInCubic';
const _SET_PREVIOUS_PAGE_TRANSITION = 'easeOutCubic';

const _SPINNER_SIZE = 20;

function MainWindow(args) {
    this._init(args);
}

MainWindow.prototype = {
    _init : function(args) {
        args = args || {};

        this._application = args.application;

        this._keyFocusActor = null;
        this._currentPage = null;

        this._activeToolbar = null;
        this._thingToolbar = null;
        this._toolbarsByThingId = {};

        this._animatingPageTurn = false;
        this._savingOldCurrentPage = false;

        this._createGtkWindow();
        this._createClutterEmbed();
        this._createStatusIcon();
        this._createPageManager();

        this._connectStageSignals();

        this._createMainBox();
        this._createContentBox();

        this._createContext();
        this._createMainToolbar();
        this._createSelectionToolbar();
        this._createSpinner();

        // show loading screen as soon as possible
        // to provide proper visual feedback
        this._createStartupBox();
    },

    _createGtkWindow : function() {
        Gtk.Window.set_default_icon_name("the-board");

        this._gtkWindow =
            new Gtk.Window({ type: Gtk.WindowType.TOPLEVEL,
                             typeHint: Gdk.WindowTypeHint.NORMAL,
                             title: "The Board",
                             focusOnMap: true,
                             hasResizeGrip: false,
                             deletable: false,
                             skipTaskbarHint: false,
                             skipPagerHint: false });

        this._updateWindowSize();

        let screen = Gdk.Screen.get_default();

        screen.connect("size-changed",
                       Lang.bind(this, this._onScreenSizeChanged));

        this._gtkWindow.connect("delete-event",
                                Lang.bind(this, this._onWindowDeleteEvent));
    },

    _createClutterEmbed : function() {
        this._clutterEmbed = new GtkClutter.Embed();
        this._gtkWindow.add(this._clutterEmbed);

        this._clutterEmbed.set_receives_default(true);
        this._clutterEmbed.set_can_default(true);

        this._stage = this._clutterEmbed.get_stage();
    },

    _createStatusIcon : function() {
        this._statusIcon =
            Gtk.StatusIcon.new_from_icon_name("the-board");

        this._statusIcon.set_tooltip_text("The Board");

        this._statusIcon.connect("button-press-event",
                                 Lang.bind(this, this._onStatusIconButtonPressEvent));
    },

    _createPageManager : function() {
        this._pageManager = new PageManager.PageManager();

        this._pageManagerStateChangedId =
            this._pageManager.connect("state-changed",
                                      Lang.bind(this, this._onPageManagerStateChanged));
    },

    _connectStageSignals : function() {
        this._stage.connect("activate",
                            Lang.bind(this, this._onStageActivate));

        this._stage.connect("deactivate",
                            Lang.bind(this, this._onStageDeactivate));

        this._stage.connect("key-press-event",
                            Lang.bind(this, this._onStageKeyPressEvent));
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               reactive: true,
                               name: "main-window-main-box" });

        this._stage.add_actor(this._mainBox);
        this._stage.show();

        let widthConstraint =
            new Clutter.BindConstraint({ source: this._stage,
                                         coordinate: Clutter.BindCoordinate.WIDTH,
                                         offset: 0.0 });
        this._mainBox.add_constraint(widthConstraint);

        let heightConstraint =
            new Clutter.BindConstraint({ source: this._stage,
                                         coordinate: Clutter.BindCoordinate.HEIGHT,
                                         offset: 0.0 });
        this._mainBox.add_constraint(heightConstraint);
    },

    _createContentBox : function() {
        this._contentBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               clipToAllocation: true,
                               name: "main-window-content-box" });

        this._mainBox.append(this._contentBox,
                             TheBoard.BoxPackFlags.EXPAND);
    },

    _createStartupBox : function() {
        this._startupBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.CENTER,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               reactive: true,
                               name: "main-window-startup-box" });

        let text = Gettext.gettext("The Board is loading. Just a sec.");

        this._startupText =
            new Mx.Label({ text: text,
                           name: "main-window-startup-label" });

        this._startupBox.append(this._startupText,
                                TheBoard.BoxPackFlags.NONE);

        this._mainBox.append(this._startupBox,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._startupBox,
                                            TheBoard.BoxAlignment.FILL,
                                            TheBoard.BoxAlignment.FILL);

        this._startupBoxTimeoutId =
            Mainloop.timeout_add_seconds(1, Lang.bind(this,
                                                      this._onStartupBoxTimeout))
    },

    _createMainToolbar : function() {
        this._mainToolbar =
            new MainToolbar.MainToolbar({ context: this._context });

        this._appendToolbar(this._mainToolbar);

        this._updateActiveToolbar();
    },

    _createSelectionToolbar : function() {
        this._selectionToolbar =
            new SelectionToolbar.SelectionToolbar({ title: Gettext.gettext("Selection"),
                                                    visible: false });

        this._appendToolbar(this._selectionToolbar);
    },

    _createSpinner : function() {
        this._spinnerBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.CENTER,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               visible: false, // starts hidden
                               name: "main-window-spinner-box" });

        this._spinner = new Mx.Spinner({ width: _SPINNER_SIZE,
                                         height: _SPINNER_SIZE,
                                         name: "main-window-spinner" });

        this._spinnerBox.append(this._spinner,
                                TheBoard.BoxPackFlags.NONE);

        this._mainBox.append(this._spinnerBox,
                                TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._spinnerBox,
                                            TheBoard.BoxAlignment.END,
                                            TheBoard.BoxAlignment.START);
    },

    _createContext : function() {
        this._context = { application: this._application,
                          gtkWindow: this._gtkWindow,
                          clutterEmbed: this._clutterEmbed,
                          pageManager: this._pageManager,
                          mainWindow: this };
    },

    _hideStartupBox : function() {
        if (!this._startupBox) {
            return;
        }

        this._setAnimatingPageTurn(true);

        // make the loading box non-reactive immediatelly
        // so that user can interact with ui as soon as it's
        // visible
        this._startupBox.reactive = false;

        let pageTurn = new Mx.DeformPageTurn({ radius: 40.0,
                                               angle: 0.80 });

        let front = new Mx.Offscreen();
        this._startupBox.reparent(front);

        let back = new Mx.Offscreen();
        back.set_child(new Mx.Button());

        pageTurn.set_textures(front, back);
        pageTurn.set_resolution(64, 64);

        this._mainBox.append(pageTurn,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(pageTurn,
                                            TheBoard.BoxAlignment.FILL,
                                            TheBoard.BoxAlignment.FILL);

        let onComplete = function() {
            delete this._startupBox;
            delete this._startupText;

            pageTurn.destroy();

            this._setAnimatingPageTurn(false);
        };

        Tweener.addTween(pageTurn,
                         { period: 1.0,
                           radius: 120.0,
                           time: _SET_CURRENT_PAGE_TIME,
                           transition: _SET_NEXT_PAGE_TRANSITION });

        Tweener.addTween(pageTurn,
                         { anchorX: 200,
                           anchorY: 200,
                           delay: _SET_CURRENT_PAGE_TIME,
                           time: _SET_CURRENT_PAGE_TIME / 4,
                           transition: 'linear',
                           onComplete: Lang.bind(this, onComplete) });
    },

    _appendToolbar : function(toolbar) {
        toolbar.actor.depth = _LAYER_TOOLBAR;

        this._contentBox.append(toolbar.actor,
                                TheBoard.BoxPackFlags.FIXED);

        this._contentBox.set_fixed_child_align(toolbar.actor,
                                               TheBoard.BoxAlignment.CENTER,
                                               TheBoard.BoxAlignment.START);
    },

    _setActiveToolbar : function(activeToolbar) {
        if (this._activeToolbar == activeToolbar) {
            return;
        }

        if (this._activeToolbar) {
            this._activeToolbar.disconnect(this._toolbarActiveChangedId);
            delete this._toolbarActiveChangedId;

            this._activeToolbar.disconnect(this._toolbarActionId);
            delete this._toolbarActionId;
        }

        this._activeToolbar = activeToolbar;

        if (this._activeToolbar) {
            this._toolbarActiveChangedId =
                this._activeToolbar.connect("active-changed",
                                            Lang.bind(this,
                                                      this._onToolbarActiveChanged));

            this._toolbarActionId =
                this._activeToolbar.connect("action",
                                            Lang.bind(this,
                                                      this._onToolbarAction));
        }
    },

    _updateActiveToolbar : function() {
        let activeToolbar = null;

        if (this._currentPage &&
            this._currentPage.activeThing) {
            activeToolbar = this._thingToolbar;
        } else if (this._currentPage &&
                   this._currentPage.selectedThings.length > 1) {
            activeToolbar = this._selectionToolbar;
        } else {
            activeToolbar = this._mainToolbar;
        }

        if (this._activeToolbar == activeToolbar) {
            return;
        }

        let oldActiveToolbar = this._activeToolbar;
        this._setActiveToolbar(activeToolbar);

        let onSlideOut = function() {
            if (activeToolbar) {
                activeToolbar.slideIn();
            }
        };

        if (oldActiveToolbar) {
            oldActiveToolbar.slideOut(onSlideOut);
        } else if (activeToolbar) {
            activeToolbar.slideIn();
        }
    },

    _createThingToolbar : function() {
        if (!this._currentPage ||
            !this._currentPage.activeThing) {
            return null;
        }

        let thingId = this._currentPage.activeThing.id;

        if (thingId in this._toolbarsByThingId) {
            return this._toolbarsByThingId[thingId];
        }

        let thingToolbar =
            Thing.createToolbar(thingId,
                                     { mainWindow: this });

        if (thingToolbar) {
            this._appendToolbar(thingToolbar);
            this._toolbarsByThingId[thingId] = thingToolbar;
        }

        return thingToolbar;
    },

    _updateThingToolbar : function() {
        let newThingToolbar = this._createThingToolbar();

        this._thingToolbar = newThingToolbar;

        if (this._currentPage.activeThing && this._thingToolbar) {
            let activeThing = this._currentPage.activeThing;
            let state = activeThing.onGetState();

            this._thingToolbar.loadState(state);
        }

        this._updateActiveToolbar();
    },

    _showSpinner : function() {
        Tweener.addTween(this._spinnerBox,
                         { opacity: 255,
                           time: _SHOW_SPINNER_TIME,
                           onStart: function() {
                               this.show();
                           }});
    },

    _hideSpinner : function() {
        Tweener.addTween(this._spinnerBox,
                         { opacity: 0,
                           time: _HIDE_SPINNER_TIME,
                           onComplete: function() {
                               this.hide();
                           }});
    },

    _hideGtkWindow : function() {
        this._gtkWindow.hide();
    },

    _updateWindowSize : function() {
        let screen = Gdk.Screen.get_default();

        let newWidth = Math.round(screen.get_width() * 0.84);
        if (newWidth % 2) {
            newWidth -= 1;
        }

        let newHeight = Math.round(screen.get_height() * 0.9);
        if (newHeight % 2) {
            newWidth -= 1;
        }

        this._gtkWindow.defaultWidth = Math.min(1114, newWidth);
        this._gtkWindow.defaultHeight = Math.min(720, newHeight);
    },

    _updateStartupBox : function() {
        if (this._currentPage &&
            this._currentPage.loaded &&
            !this._startupBoxTimeoutId &&
            this._pageManager.state == PageManager.State.LOADED) {
            this._hideStartupBox();
        }
    },

    _updateSpinner : function() {
        if (this._currentPage &&
            this._currentPage.loaded &&
            !this._savingOldCurrentPage &&
            !this._animatingPageTurn) {
            this._hideSpinner();
        } else {
            this._showSpinner();
        }
    },

    _updateLoadingState : function() {
        this._updateSpinner();
        this._updateStartupBox();
    },

    _setSavingOldCurrentPage : function(savingOldCurrentPage) {
        if (this._savingOldCurrentPage == savingOldCurrentPage) {
            return;
        }

        this._savingOldCurrentPage = savingOldCurrentPage;
        this._updateSpinner();
    },

    _setAnimatingPageTurn : function(animatingPageTurn) {
        if (this._animatingPageTurn == animatingPageTurn) {
            return;
        }

        this._animatingPageTurn = animatingPageTurn;
        this._updateSpinner();
    },

    _animateNextPage : function(newPage, oldPage) {
        this._setAnimatingPageTurn(true);

        this._contentBox.append(newPage.actor,
                                TheBoard.BoxPackFlags.EXPAND);

        let pageTurn = new Mx.DeformPageTurn({ radius: 40.0,
                                               angle: 0.80 });

        let front = new Mx.Offscreen();
        oldPage.actor.reparent(front);

        let back = new Mx.Offscreen();
        back.set_child(new Mx.Button());

        pageTurn.set_textures(front, back);
        pageTurn.set_resolution(64, 64);

        this._contentBox.append(pageTurn,
                                TheBoard.BoxPackFlags.FIXED);

        this._contentBox.set_fixed_child_align(pageTurn,
                                               TheBoard.BoxAlignment.FILL,
                                               TheBoard.BoxAlignment.FILL);

        pageTurn.raise(newPage.actor);

        Tweener.addTween(pageTurn,
                         { period: 1.0,
                           radius: 120.0,
                           time: _SET_CURRENT_PAGE_TIME,
                           transition: _SET_NEXT_PAGE_TRANSITION });

        let onComplete = function() {
            pageTurn.destroy();
            oldPage.destroy();

            this._setAnimatingPageTurn(false);
        };

        Tweener.addTween(pageTurn,
                         { anchorX: 200,
                           anchorY: 200,
                           delay: _SET_CURRENT_PAGE_TIME,
                           time: _SET_CURRENT_PAGE_TIME / 4,
                           transition: 'linear',
                           onComplete: Lang.bind(this, onComplete) });
    },

    _animatePreviousPage : function(newPage, oldPage) {
        this._setAnimatingPageTurn(true);

        let pageTurn = new Mx.DeformPageTurn({ radius: 120.0,
                                               angle: 0.80,
                                               period: 1.0,
                                               anchorX: 200,
                                               anchorY: 200 });

        let front = new Mx.Offscreen();
        newPage.actor.reparent(front);

        let back = new Mx.Offscreen();
        back.set_child(new Mx.Button());

        pageTurn.set_textures(front, back);
        pageTurn.set_resolution(64, 64);

        this._contentBox.append(pageTurn,
                                TheBoard.BoxPackFlags.FIXED);

        this._contentBox.set_fixed_child_align(pageTurn,
                                               TheBoard.BoxAlignment.FILL,
                                               TheBoard.BoxAlignment.FILL);

        pageTurn.raise(oldPage.actor);

        Tweener.addTween(pageTurn,
                         { anchorX: 0,
                           anchorY: 0,
                           time: _SET_CURRENT_PAGE_TIME / 4,
                           transition: _SET_NEXT_PAGE_TRANSITION });

        let onComplete = function() {
            oldPage.destroy();

            newPage.actor.reparent(this._contentBox);

            this._contentBox.set_child_packing(newPage.actor,
                                               TheBoard.BoxPackFlags.EXPAND);

            pageTurn.destroy();

            this._setAnimatingPageTurn(false);
        };

        Tweener.addTween(pageTurn,
                         { period: 0.0,
                           radius: 40.0,
                           delay: _SET_CURRENT_PAGE_TIME / 4,
                           time: _SET_CURRENT_PAGE_TIME,
                           transition: _SET_PREVIOUS_PAGE_TRANSITION,
                           onComplete: Lang.bind(this, onComplete) });
    },

    _doAction : function(actionName, actionArgs) {
        if (actionName == "addNewPage") {
            this._addNewPage();
        } else if (actionName == "setCurrentPage") {
            this._setCurrentPage(actionArgs.pageModel);
        } else if (actionName == "setBackground") {
            this._currentPage.setBackground(actionArgs.backgroundId);
        } else if (actionName == "addThing") {
            this._currentPage.addThingFromState({ id: actionArgs.thingId });
        }
    },

    _addNewPage : function() {
        let newPageModel = this._pageManager.addPageModel();
        this._setCurrentPage(newPageModel);
    },

    _setCurrentPage : function(pageModel) {
        if (this._currentPage &&
            this._currentPage.model.id == pageModel.id) {
            return;
        }

        let oldCurrentPage = this._currentPage;

        if (oldCurrentPage) {
            oldCurrentPage.disconnect(this._pageActiveThingChangedId);
            delete this._pageActiveThingChangedId;

            oldCurrentPage.disconnect(this._pageClickedId);
            delete this._pageClickedId;

            oldCurrentPage.disconnect(this._pageLoadedChangedId);
            delete this._pageLoadedChangedId;

            oldCurrentPage.disconnect(this._pageSelectedThingsChangedId);
            delete this._pageSelectedThingsChangedId;
        }

        this._currentPage = new Page.Page({ context: this._context,
                                            model: pageModel });

        this._mainToolbar.toolBoxPages.setCurrentPage(pageModel);

        this._currentPage.actor.depth = _LAYER_PAGE;

        this._pageActiveThingChangedId =
            this._currentPage.connect("active-thing-changed",
                                      Lang.bind(this, this._onPageActiveThingChanged));

        this._pageClickedId =
            this._currentPage.connect("clicked",
                                      Lang.bind(this, this._onPageClicked));

        this._pageLoadedChangedId =
            this._currentPage.connect("loaded-changed",
                                      Lang.bind(this, this._onPageLoadedChanged));

        this._pageSelectedThingsChangedId =
            this._currentPage.connect("selected-things-changed",
                                      Lang.bind(this, this._onPageSelectedThingsChanged));

        if (oldCurrentPage) {
            let onModelSaved = function() {
                this._setSavingOldCurrentPage(false);

                if (this._currentPage.model.id > oldCurrentPage.model.id) {
                    this._animateNextPage(this._currentPage, oldCurrentPage);
                } else {
                    this._animatePreviousPage(this._currentPage, oldCurrentPage);
                }
            };

            this._setSavingOldCurrentPage(true);
            oldCurrentPage.model.save(Lang.bind(this, onModelSaved));
        } else {
            this._contentBox.append(this._currentPage.actor,
                                    TheBoard.BoxPackFlags.EXPAND);
        }

        this._updateLoadingState();
    },

    _quitApp : function() {
        let onModelSaved = function() {
            this._setSavingOldCurrentPage(false);
            this._application.quit();
        };

        if (this._currentPage) {
            this._setSavingOldCurrentPage(true);
            this._currentPage.model.save(Lang.bind(this, onModelSaved));
        } else {
            this._application.quit();
        }
    },

    _pasteClipboardContent : function() {
        let clipboard = TheBoard.get_default_clipboard();

        let onTextReceived = Lang.bind(this,
                                       this._onClipboardTextReceived);

        clipboard.request_text(onTextReceived, null);
    },

    _onStatusIconButtonPressEvent : function() {
        if (this._gtkWindow.visible) {
            this._gtkWindow.hide();
        } else {
            this.present();
        }
    },

    _onWindowDeleteEvent : function() {
        this._hideGtkWindow();
        return true;
    },

    _onToolbarActiveChanged : function() {
        if (!this._currentPage) {
            return;
        }

        if (this._activeToolbar.active) {
            this._currentPage.setActiveThing(null);
        } else {
            this._mainBox.grab_key_focus();
        }
    },

    _onToolbarAction : function(toolbar, actionName, actionArgs) {
        if (toolbar == this._mainToolbar) {
            this._doAction(actionName, actionArgs);
            return;
        }

        if (!this._currentPage) {
            return;
        }

        this._currentPage.doAction(actionName, actionArgs);
    },

    _onClipboardTextReceived : function(clipboard, text, data) {
        if (!text) {
            return;
        }

        // FIXME: Do more smart things here like recognizing
        // urls or filenames

        let thingId;

        if (text.length < 100) {
            thingId = "label";
        } else {
            thingId = "note";
        }

        this._currentPage.addThingFromState({ id: thingId,
                                              text: text });
    },

    _onStartupBoxTimeout : function() {
        delete this._startupBoxTimeoutId;
        this._updateStartupBox();

        return false;
    },

    _onScreenSizeChanged : function() {
        this._updateWindowSize();
    },

    _onStageActivate : function() {
        this._stage.set_key_focus(this._keyFocusActor);
    },

    _onStageDeactivate : function() {
        this._keyFocusActor = this._stage.keyFocus;
    },

    _onStageKeyPressEvent : function(o, event) {
        let key = event.get_key_symbol();
        let mask = event.get_state();
        let text = String.fromCharCode(key);

        let isControl = mask & Clutter.ModifierType.CONTROL_MASK;

        if (isControl && text == 'v') {
            this._pasteClipboardContent();
            return true;
        } else if (isControl && text == 'a') {
            this._currentPage.selectAllThings();
            return true;
        } else if (key == Clutter.Escape) {
            if (this._currentPage.selectedThings.length > 0) {
                this._currentPage.unselectAllThings();
            }
            return true;
        } else if (key == Clutter.Delete) {
            this._currentPage.doAction("remove");
            return true;
        } else if (isControl && text == 'n') {
            this._addNewPage();
            return true;
        } else if (text == 't') {
            this._currentPage.addThingFromState({ id: 'note' });
            return true;
        } else if (text == 'l') {
            this._currentPage.addThingFromState({ id: 'label' });
            return true;
        } else if (text == 'p') {
            this._currentPage.addThingFromState({ id: 'photo' });
            return true;
        } else if (text == 'v') {
            this._currentPage.addThingFromState({ id: 'video' });
            return true;
        } else if (text == 's') {
            this._currentPage.addThingFromState({ id: 'sound' });
            return true;
        } else if (isControl && text == 'w') {
            this._hideGtkWindow();
            return true;
        } else if (isControl && text == 'q') {
            this._quitApp();
            return true;
        } else if (isControl && text == 'h') {
            this._currentPage.doAction("distribute",
                                       { orientation: Page.Orientation.HORIZONTAL });
            return true;
        } else if (isControl && text == 'g') {
            this._currentPage.doAction("distribute",
                                       { orientation: Page.Orientation.VERTICAL });
            return true;
        } else if (isControl && text == 'u') {
            this._currentPage.doAction("align",
                                       { alignment: Page.Alignment.LEFT });
            return true;
        } else if (isControl && text == 'i') {
            this._currentPage.doAction("align",
                                       { alignment: Page.Alignment.RIGHT });
            return true;
        } else if (isControl && text == 'j') {
            this._currentPage.doAction("align",
                                       { alignment: Page.Alignment.TOP });
            return true;
        } else if (isControl && text == 'k') {
            this._currentPage.doAction("align",
                                       { alignment: Page.Alignment.BOTTOM });
            return true;
        }

        return false;
    },

    _onPageManagerStateChanged : function() {
        this._updateLoadingState();

        if (this._pageManager.state == PageManager.State.LOADED) {
            let pageModel;

            if (this._pageManager.latestPage) {
                pageModel = this._pageManager.latestPage;
            } else {
                pageModel = this._pageManager.addPageModel();
            }

            this._setCurrentPage(pageModel);
        }
    },

    _onPageActiveThingChanged : function() {
        if (this._currentPage.activeThing === null) {
            this._mainBox.grab_key_focus();
        } else if (this._activeToolbar) {
            this._activeToolbar.deactivate();
        }

        this._updateThingToolbar();
    },

    _onPageClicked : function() {
        this._activeToolbar.deactivate();
        this._mainBox.grab_key_focus();
    },

    _onPageLoadedChanged : function() {
        this._updateLoadingState();

        if (this._currentPage.loaded) {
            // once model of current page is loaded, it's safe to
            // update tool box with current state
            let backgroundId = this._currentPage.model.background;
            this._mainToolbar.toolBoxBackgrounds.setCurrentBackground(backgroundId);
        }
    },

    _onPageSelectedThingsChanged : function() {
        this._updateActiveToolbar();
    },

    showAll : function() {
        this._gtkWindow.show_all();
    },

    present : function() {
        this._gtkWindow.present();
        this._clutterEmbed.grab_focus();
    },

    destroy : function() {
        for (let thingId in this._toolbarsByThingId) {
            let toolbar = this._toolbarsByThingId[thingId];
            toolbar.destroy();
        }

        if (this._pageManagerStateChangedId) {
            this._pageManager.disconnect(this._pageManagerStateChangedId);
            delete this._pageManagerStateChangedId;
        }

        if (this._pageActiveThingChangedId) {
            this._currentPage.disconnect(this._pageActiveThingChangedId);
            delete this._pageActiveThingChangedId;
        }

        this._setActiveToolbar(null);

        if (this._pageClickedId) {
            this._currentPage.disconnect(this._pageClickedId);
            delete this._pageClickedId;
        }

        if (this._pageLoadedChangedId) {
            this._currentPage.disconnect(this._pageLoadedChangedId);
            delete this._pageLoadedChangedId;
        }

        if (this._pageSelectedThingsChangedId) {
            this._currentPage.disconnect(this._pageSelectedThingsChangedId);
            delete this._pageSelectedThingsChangedId;
        }

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get currentPage() {
        return this._currentPage;
    },

    get actor() {
        return this._mainBox;
    }
}
