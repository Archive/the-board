// standard imports
const Lang = imports.lang;
const Signals = imports.signals;

// gi imports
const Clutter = imports.gi.Clutter;
const Mx = imports.gi.Mx;
const Pango = imports.gi.Pango;
const TheBoard = imports.gi.TheBoard;

function ToolBox(args) {
    this._init(args);
}

ToolBox.prototype = {
    _init : function(args) {
        args = args || {};

        if ('title' in args) {
            this._title = args.title;
        } else {
            this._title = null;
        }

        if ('isButtonGroup' in args) {
            this._isButtonGroup = args.isButtonGroup;
        } else {
            this._isButtonGroup = false;
        }

        this._loaded = false;
        this._active = false;
        this._buttons = [];

        this._createMainBox();

        if (this._title) {
            this._createTitleBox();
        }

        this._createButtonBox();

        if (this._isButtonGroup) {
            this._createButtonGroup();
        }
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.START,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               reactive: true,
                               name: "tool-box-main-box" });
    },

    _createTitleBox : function() {
        this._titleBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.START,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               name: "tool-box-title-box" });

        this._titleLabel =
            new Mx.Label({ text: this._title,
                           name: "tool-box-title-label" });

        this._titleBox.append(this._titleLabel,
                              TheBoard.BoxPackFlags.NONE);

        this._mainBox.append(this._titleBox,
                             TheBoard.BoxPackFlags.NONE);
    },

    _createButtonBox : function() {
        this._buttonBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.START,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               name: "tool-box-button-box" });

        this._mainBox.append(this._buttonBox,
                             TheBoard.BoxPackFlags.EXPAND);
    },

    _createButtonGroup : function() {
        this._buttonGroup =
            new Mx.ButtonGroup({ allowNoActive: true });
    },

    _destroyAllButtons : function() {
        while (this._buttons.length > 0) {
            let button = this._buttons[0];
            this.removeButton(button);
        }
    },

    _removeButton : function(button) {
        if (button._ToolBox_clickedId) {
            button.disconnect(button._ToolBox_clickedId);
            delete button._ToolBox_clickedId;
        }

        if (button._ToolBox_clickedForActionId) {
            button.disconnect(button._ToolBox_clickedForActionId);
            delete button._ToolBox_clickedForActionId;
        }

        let indexToRemove = this._buttons.indexOf(button);
        this._buttons.splice(indexToRemove, 1);

        button.destroy();
    },

    _onButtonActionClicked : function(button, actionName, actionArgs) {
        this.emit('action', actionName, actionArgs);
    },

    addButton : function(args) {
        let button =
            new Mx.Button({ label: args.label,
                            isToggle: this._isButtonGroup,
                            name: "tool-box-button" });

        if ('onClicked' in args) {
            button._ToolBox_clickedId =
                button.connect("clicked", args.onClicked);
        }

        if ('actionName' in args) {
            args.actionArgs = args.actionArgs || {};

            button._ToolBox_clickedForActionId =
                button.connect("clicked",
                               Lang.bind(this,
                                         this._onButtonActionClicked,
                                         args.actionName,
                                         args.actionArgs));
        }

        this._buttonBox.append(button,
                               TheBoard.BoxPackFlags.NONE);

        if (this._isButtonGroup) {
            this._buttonGroup.add(button);
        }

        this._buttons.push(button);

        return button;
    },

    setActiveButton : function(activeButton) {
        if (!this._isButtonGroup) {
            throw new Error("Can't set active button on " +
                            "non-button group tool box");
        }

        if (this._buttons.indexOf(activeButton) != -1) {
            this._buttonGroup.set_active_button(activeButton);
        }
    },

    setActive : function(active) {
        if (this._active === active) {
            return;
        }

        // only allow expanding the toolbox once
        // we're fully loaded
        this._active = active && this._loaded;

        this.emit("active-changed");
    },

    destroy : function() {
        this._destroyAllButtons();

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get loaded() {
        return this._loaded;
    },

    set loaded(loaded) {
        this._loaded = loaded;
    },

    get active() {
        return this._active;
    },

    get actor() {
        return this._mainBox;
    }
}

Signals.addSignalMethods(ToolBox.prototype);
