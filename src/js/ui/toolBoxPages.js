// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;
const Signals = imports.signals;
const Tweener = imports.tweener.tweener;

// gi imports
const Clutter = imports.gi.Clutter;
const Mx = imports.gi.Mx;
const TheBoard = imports.gi.TheBoard;

// ui imports
const PageButton = imports.ui.pageButton;
const ToolBox = imports.ui.toolBox;

// model imports
const PageManager = imports.model.pageManager;

const _BUTTON_BOX_NAME_NORMAL = "tool-box-pages-button-box";
const _BUTTON_BOX_NAME_EDITING = "tool-box-pages-button-box-editing";

const _ACTION_BUTTON_OK_NAME = "tool-box-pages-ok-button";
const _ACTION_BUTTON_LESS_NAME = "tool-box-pages-less-button";
const _ACTION_BUTTON_MORE_NAME = "tool-box-pages-more-button";

const _SHOW_PAGES_LIST_TIME = 0.4;
const _SHOW_PAGES_LIST_TRANSITION = "easeOutCube";

const _WIDTH = 200;

function ToolBoxPages(args) {
    this._init(args);
}

ToolBoxPages.prototype = {
    _init : function(args) {
        args = args || {};

        if ('context' in args) {
            this._context = args.context;
        } else {
            throw new Error("ToolBoxPages context is required");
        }

        this._pageModel = null;
        this._loaded = false;
        this._active = false;
        this._pageButtons = [];

        this._createMainBox();
        this._createButtonBox();
        this._createPageEntry();
        this._createListBox();
        this._createActionButton();

        this._connectPageManagerSignals();
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               width: _WIDTH,
                               name: "tool-box-pages-main-box" });
    },

    _createButtonBox : function() {
        this._buttonBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               reactive: true,
                               name: _BUTTON_BOX_NAME_NORMAL });

        this._buttonBox.connect("notify::height",
                                Lang.bind(this,
                                          this._onButtonBoxHeightChanged));

        let clickAction = new Clutter.ClickAction();

        clickAction.connect("clicked",
                            Lang.bind(this, this._onButtonBoxClicked));

        this._buttonBox.add_action(clickAction);

        this._mainBox.append(this._buttonBox,
                             TheBoard.BoxPackFlags.EXPAND);
    },

    _createPageEntry : function() {
        this._pageEntry =
            new Mx.Entry({ reactive: false,
                           name: "tool-box-pages-entry" });

        this._pageEntry.clutterText.connect("key-press-event",
                                            Lang.bind(this,
                                                      this._onPageLabelKeyPressEvent));

        this._buttonBox.append(this._pageEntry,
                               TheBoard.BoxPackFlags.EXPAND);

        this._buttonBox.set_child_align(this._pageEntry,
                                        TheBoard.BoxAlignment.FILL,
                                        TheBoard.BoxAlignment.CENTER);
    },

    _createActionButton : function() {
        this._actionButton =
            new Mx.Button({ name: "tool-box-pages-more-button" });

        this._updateActionButton();

        this._actionButton.connect("clicked",
                                   Lang.bind(this, this._onActionButtonClicked));

        this._buttonBox.append(this._actionButton,
                               TheBoard.BoxPackFlags.END);
    },

    _createListBox : function() {
        this._listContainerBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               width: _WIDTH + 16, // FIXME: 16 is the shadow width
                               visible: false,
                               name: "tool-box-pages-list-container-box" });

        this._listBox =
            new Mx.BoxLayout({ orientation: Mx.Orientation.VERTICAL,
                               enableAnimations: true,
                               name: "tool-box-pages-list-box" });

        this._listContainerBox.append(this._listBox,
                                      TheBoard.BoxPackFlags.EXPAND);

        this._buttonBox.append(this._listContainerBox,
                               TheBoard.BoxPackFlags.FIXED);

        this._buttonBox.set_fixed_child_align(this._listContainerBox,
                                              TheBoard.BoxAlignment.CENTER,
                                              TheBoard.BoxAlignment.FIXED);

        this._coverBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               width: _WIDTH,
                               anchorX: 2,
                               name: "tool-box-pages-cover-box" });

        this._buttonBox.append(this._coverBox,
                               TheBoard.BoxPackFlags.FIXED);

        this._buttonBox.set_fixed_child_align(this._coverBox,
                                              TheBoard.BoxAlignment.CENTER,
                                              TheBoard.BoxAlignment.FILL);

        this._coverBox.lower(null);
        this._listContainerBox.lower(this._coverBox);
    },

    _connectPageManagerSignals : function() {
        let pageManager = this._context.pageManager;

        this._pageManagerStateChangedId =
            pageManager.connect("state-changed",
                                Lang.bind(this, this._onPageManagerStateChanged));

        this._updateFromPageManager();
    },

    _updateFromPageManager : function() {
        let pageManager = this._context.pageManager;

        if (pageManager.state == PageManager.State.LOADED) {
            pageManager.disconnect(this._pageManagerStateChangedId);
            delete this._pageManagerStateChangedId;

            this._pageManagerPageAddedId =
                pageManager.connect("page-added",
                                    Lang.bind(this, this._onPageManagerPageAdded));

            this._pageManagerPageRemovedId =
                pageManager.connect("page-removed",
                                    Lang.bind(this, this._onPageManagerPageRemoved));

            this._loadPages();

            this._loaded = true;
        }
    },

    _addPageButton : function(pageModel, position) {
        let pageButton =
            new PageButton.PageButton({ pageModel: pageModel });

        pageButton._ToolBoxPages_clickedId =
            pageButton.connect("clicked",
                           Lang.bind(this,
                                     this._onPageButtonClicked));

        pageButton._ToolBoxPages_removeId =
            pageButton.connect("remove",
                           Lang.bind(this,
                                     this._onPageButtonRemove));

        this._updateButtonVisibility(pageButton);

        this._pageButtons.push(pageButton);

        this._listBox.add_actor(pageButton.actor, position);
    },

    _destroyAllPageButtons : function() {
        while (this._pageButtons.length > 0) {
            let pageButton = this._pageButtons[0];
            this._destroyPageButton(pageButton);
        }
    },

    _destroyPageButton : function(pageButton) {
        pageButton.disconnect(pageButton._ToolBoxPages_clickedId);
        delete pageButton._ToolBoxPages_clickedId;

        pageButton.disconnect(pageButton._ToolBoxPages_removeId);
        delete pageButton._ToolBoxPages_removeId;

        let indexToRemove = this._pageButtons.indexOf(pageButton);
        this._pageButtons.splice(indexToRemove, 1);

        pageButton.destroy();
    },

    _loadPages : function() {
        let pageManager = this._context.pageManager;

        for (let i = 0; i < pageManager.pages.length; ++i) {
            let pageModel = pageManager.pages[i];
            this._addPageButton(pageModel, -1);
        }

        this._newPageButton =
            new PageButton.PageButton({ newPageButton: true });

        this._newPageButtonClickedId =
            this._newPageButton.connect("clicked",
                                        Lang.bind(this,
                                                  this._onNewPageButtonClicked));

        this._listBox.add_actor(this._newPageButton.actor, -1);
    },

    _updateButtonVisibility : function(pageButton) {
        pageButton.actor.visible = (pageButton.pageModel !== this._pageModel);
    },

    _updateActionButton : function() {
        let name;

        if (this._pageEntry.reactive) {
            name = _ACTION_BUTTON_OK_NAME;
        } else if (this._listContainerBox.visible) {
            name = _ACTION_BUTTON_LESS_NAME;
        } else {
            name = _ACTION_BUTTON_MORE_NAME;
        }

        this._actionButton.set_name(name);
    },

    _startEditingTitle : function() {
        this._pageEntry.reactive = true;
        this._pageEntry.grab_key_focus();
        this._pageEntry.clutterText.set_cursor_position(-1);

        this._pageEntry.clutterText.set_selection(-1, 0);

        this._buttonBox.set_name(_BUTTON_BOX_NAME_EDITING);

        this._updateActionButton();
        this._hidePagesList();

        this.setActive(true);
    },

    _stopEditingTitle : function(commitChanges) {
        if (!this._pageEntry.reactive) {
            return;
        }

        this._pageEntry.reactive = false;

        this._updateActionButton();
        this._buttonBox.set_name(_BUTTON_BOX_NAME_NORMAL);

        if (commitChanges) {
            this._pageModel.title = this._pageEntry.text;
        } else {
            this._pageEntry.text = this._pageModel.title ||
                                   Gettext.gettext("Untitled");
        }

        this.setActive(false);
    },

    _findButtonByPageModel : function(pageModel) {
        let findByPageModel = function(pageButton) {
            return (pageButton.pageModel == pageModel);
        }

        let foundButton = this._pageButtons.filter(findByPageModel);

        if (foundButton.length == 1) {
            return foundButton[0];
        }

        return null;
    },

    _showPagesList : function() {
        this.setActive(true);
        this._stopEditingTitle(false /* cancel changes */);

        let padding = TheBoard.mx_stylable_get_padding(this._buttonBox);

        this._listContainerBox.anchorX = Math.round(padding.left / 2);
        this._listContainerBox.anchorY = this._listBox.height + 1;
        this._listContainerBox.show();

        Tweener.addTween(this._listContainerBox,
                         { time: _SHOW_PAGES_LIST_TIME,
                           anchorY: 0,
                           transition: _SHOW_PAGES_LIST_TRANSITION });

        this._updateActionButton();
    },

    _hidePagesList : function() {
        Tweener.removeTweens(this._listContainerBox);
        this._listContainerBox.hide();

        this._updateActionButton();
    },

    _onButtonBoxHeightChanged : function() {
        this._listContainerBox.y = this._buttonBox.height;
    },

    _onNewPageButtonClicked : function() {
        this.emit("action", "addNewPage");
    },

    _onPageButtonClicked : function(pageButton) {
        this.emit("action",
                  "setCurrentPage",
                  { pageModel: pageButton.pageModel });

        this.setCurrentPage(pageButton.pageModel);

        this._hidePagesList();
    },

    _onPageButtonRemove : function(pageButton) {
        let pageManager = this._context.pageManager;
        pageManager.removePageModel(pageButton.pageModel);
    },

    _onPageLabelKeyPressEvent : function(o, event) {
        let key = event.get_key_symbol();

        switch (key) {
        case Clutter.Return:
            this._stopEditingTitle(true /* commit changes */);
            return true;
        case Clutter.Escape:
            this._stopEditingTitle(false /* cancel changes */);
            return true;
        }

        return false;
    },

    _onButtonBoxClicked : function() {
        this._startEditingTitle();
    },

    _onPageManagerStateChanged : function() {
        this._updateFromPageManager();
    },

    _onPageManagerPageAdded : function(pageManager, pageModel) {
        let foundButton = this._findButtonByPageModel(pageModel);

        if (foundButton) {
            return;
        }

        let indexToAdd = pageManager.pages.indexOf(pageModel);
        this._addPageButton(pageModel, indexToAdd);
    },

    _onPageManagerPageRemoved : function(pageManager, pageModel) {
        let foundButton = this._findButtonByPageModel(pageModel);

        if (!foundButton) {
            return;
        }

        this._destroyPageButton(foundButton);
    },

    _onActionButtonClicked : function() {
        if (this._pageEntry.reactive) {
            this._stopEditingTitle(true /* commit changes */);
        } else if (this._listContainerBox.visible) {
            this._hidePagesList();
        } else {
            this._showPagesList();
        }
    },

    setActive : function(active) {
        if (this._active === active) {
            return;
        }

        this._active = active && this._loaded;

        if (!this._active) {
            this._stopEditingTitle(false /* cancel changes */);
            this._hidePagesList();
        }

        this.emit("active-changed");
    },

    setCurrentPage : function(pageModel) {
        if (this._pageModel == pageModel) {
            return;
        }

        this._pageModel = pageModel;
        this._pageEntry.text = pageModel.title ||
                               Gettext.gettext("Untitled");

        for (let i = 0; i < this._pageButtons.length; ++i) {
            let pageButton = this._pageButtons[i];
            this._updateButtonVisibility(pageButton);
        }

        if (pageModel.isNew()) {
            this._startEditingTitle();
        }
    },

    destroy : function() {
        let pageManager = this._context.pageManager;

        if (this._pageManagerPageAddedId) {
            pageManager.disconnect(this._pageManagerPageAddedId);
            delete this._pageManagerPageAddedId;
        }

        if (this._pageManagerPageRemovedId) {
            pageManager.disconnect(this._pageManagerPageRemovedId);
            delete this._pageManagerPageRemovedId;
        }

        if (this._newPageButtonClickedId) {
            this._newPageButton.disconnect(this._newPageButtonClickedId);
            delete this._newPageButtonClickedId;
        }

        if (this._newPageButton) {
            this._newPageButton.destroy();
            delete this._newPageButton;
        }

        this._destroyAllPageButtons();

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get loaded() {
        return this._loaded;
    },

    get active() {
        return this._active;
    },

    get actor() {
        return this._mainBox;
    }
}

Signals.addSignalMethods(ToolBoxPages.prototype);
