// standard imports
const Lang = imports.lang;
const Signals = imports.signals;
const Tweener = imports.tweener.tweener;

// gi imports
const Clutter = imports.gi.Clutter;
const Mx = imports.gi.Mx;
const TheBoard = imports.gi.TheBoard;

// ui imports
const ToolBox = imports.ui.toolBox;

const _SPACING = 10;
const _ACTIVATE_TIME = 0.25;
const _ACTIVATE_TRANSITION = "easeOutCubic";
const _SHOW_TIME = 0.2;
const _SHOW_TRANSITION = "easeOutCubic";
const _HIDE_TIME = 0.2;
const _HIDE_TRANSITION = "easeInCubic";

const _TOOLBOX_DEPTH = 0.1;
const _TOOLBOX_ACTIVE_DEPTH = 0.2;

const _TOOLBOX_ANCHOR_Y = 100;

function Toolbar(args) {
    this._init(args);
}

Toolbar.prototype = {
    _init : function(args) {
        args = args || {};

        if ('visible' in args) {
            this._visible = args.visible;
        } else {
            this._visible = true;
        }

        this._activeToolBox = null;
        this._toolBoxes = [];

        this._createMainBox();

        if ('title' in args) {
            let title = args.title;

            if (title) {
                this._createTitleBox(title);
            }
        }

        this._createContentBox();
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               anchorY: this._visible ? 0 : _TOOLBOX_ANCHOR_Y,
                               name: "toolbar-main-box" });
    },

    _createContentBox : function() {
        this._contentBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.CENTER,
                               yAlign: TheBoard.BoxAlignment.START,
                               name: "toolbar-content-box" });

        this._mainBox.append(this._contentBox,
                             TheBoard.BoxPackFlags.EXPAND);
    },

    _createTitleBox : function(title) {
        this._titleBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.CENTER,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               name: "toolbar-title-box" });

        this._titleLabel =
            new Mx.Label({ text: title,
                           name: "toolbar-title-label" });

        this._titleBox.append(this._titleLabel,
                              TheBoard.BoxPackFlags.EXPAND);

        this._mainBox.append(this._titleBox,
                             TheBoard.BoxPackFlags.EXPAND);
    },

    _setActiveToolBox : function(toolBox) {
        if (this._activeToolBox === toolBox) {
            return;
        }

        let oldActivateToolBox = this._activeToolBox;

        this._activeToolBox = toolBox;

        if (oldActivateToolBox) {
            oldActivateToolBox.setActive(false);
            oldActivateToolBox.actor.depth = _TOOLBOX_DEPTH;
        }

        if (this._activeToolBox) {
            this._activeToolBox.setActive(true);
            this._activeToolBox.actor.depth = _TOOLBOX_ACTIVE_DEPTH;
        }

        this.emit("active-changed");
    },

    _destroyAllToolBoxes : function() {
        while (this._toolBoxes.length > 0) {
            let toolBox = this._toolBoxes[0];
            this.removeToolBox(toolBox);
        }
    },

    _onToolBoxActiveChanged : function(toolBox) {
        if (toolBox.active) {
            this._setActiveToolBox(toolBox);
        } else if (this._activeToolBox === toolBox) {
            this._setActiveToolBox(null);
        }
    },

    _onToolBoxAction : function(toolBox, actionName, actionArgs) {
        this.emit("action", actionName, actionArgs);
    },

    addToolBox : function(toolBox) {
        toolBox._Toolbar_activatedId =
            toolBox.connect("active-changed",
                            Lang.bind(this, this._onToolBoxActiveChanged));

        toolBox._Toolbar_actionId =
            toolBox.connect("action",
                            Lang.bind(this, this._onToolBoxAction));

        this._contentBox.append(toolBox.actor,
                                TheBoard.BoxPackFlags.NONE);

        this._toolBoxes.push(toolBox);
    },

    removeToolBox : function(toolBox) {
        toolBox.disconnect(toolBox._Toolbar_activatedId);
        delete toolBox._Toolbar_activatedId;

        toolBox.disconnect(toolBox._Toolbar_actionId);
        delete toolBox._Toolbar_actionId;

        let indexToRemove = this._toolBoxes.indexOf(toolBox);
        this._toolBoxes.splice(indexToRemove, 1);

        toolBox.destroy();
    },

    deactivate : function() {
        this._setActiveToolBox(null);
    },

    loadState : function(state) {
        // do nothing by default
    },

    slideIn : function(onComplete) {
        if (this._visible) {
            return;
        }

        this._visible = true;

        Tweener.addTween(this._mainBox,
                         { anchorY: 0,
                           time: _SHOW_TIME,
                           transition: _SHOW_TRANSITION,
                           onComplete: onComplete });
    },

    slideOut : function(onComplete) {
        if (!this._visible) {
            return;
        }

        this._visible = false;

        Tweener.addTween(this._mainBox,
                         { anchorY: _TOOLBOX_ANCHOR_Y,
                           time: _HIDE_TIME,
                           transition: _HIDE_TRANSITION,
                           onComplete: onComplete });
    },

    destroy : function() {
        this._destroyAllToolBoxes();

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get active() {
        return (this._activeToolBox !== null);
    },

    get actor() {
        return this._mainBox;
    }
}

Signals.addSignalMethods(Toolbar.prototype);
