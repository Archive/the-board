// ui imports
const ToolBoxBackgrounds = imports.ui.toolBoxBackgrounds;
const ToolBoxPages = imports.ui.toolBoxPages;
const ToolBoxThings = imports.ui.toolBoxThings;
const Toolbar = imports.ui.toolbar;

function MainToolbar(args) {
    this._init(args);
}

MainToolbar.prototype = {
    __proto__: Toolbar.Toolbar.prototype,

    _init : function(args) {
        if ('context' in args) {
            this._context = args.context;
        } else {
            throw new Error("MainToolbar context is required");
        }

        Toolbar.Toolbar.prototype._init.apply(this, [args]);

        this._createToolBoxPages();
        this._createToolBoxThings();
        this._createToolBoxBackgrounds();
    },

    _createToolBoxPages : function() {
        this._toolBoxPages =
            new ToolBoxPages.ToolBoxPages({ context: this._context });

        this.addToolBox(this._toolBoxPages);
    },

    _createToolBoxThings : function() {
        this._toolBoxThings =
            new ToolBoxThings.ToolBoxThings();

        this.addToolBox(this._toolBoxThings);
    },

    _createToolBoxBackgrounds : function() {
        this._toolBoxBackgrounds =
            new ToolBoxBackgrounds.ToolBoxBackgrounds();

        this.addToolBox(this._toolBoxBackgrounds);
    },

    // FIXME: remove once MainWindow is not using
    // this getter anymore
    get toolBoxBackgrounds() {
        return this._toolBoxBackgrounds;
    },

    // FIXME: remove once MainWindow is not using
    // this getter anymore
    get toolBoxPages() {
        return this._toolBoxPages;
    }
}
