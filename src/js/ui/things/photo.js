// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;
const Tweener = imports.tweener.tweener;

// util imports
const Features = imports.util.features;
const Path = imports.util.path;

if (Features.HAVE_CHEESE) {
    const Cheese = imports.gi.Cheese;
}

// gi imports
const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;
const Gst = imports.gi.Gst;
const Gtk = imports.gi.Gtk;
const Mx = imports.gi.Mx;
const Pango = imports.gi.Pango;
const TheBoard = imports.gi.TheBoard;

// ui imports
const Thing = imports.ui.thing;
const ToolBox = imports.ui.toolBox;
const Toolbar = imports.ui.toolbar;

const NAME = Gettext.gettext("Photo");
const STYLE = Path.THINGS_DATA_DIR + "photo/style.css";

const _TAPE_IMAGE = Path.THINGS_DATA_DIR + "photo/tape.png"

const _SHOW_BUTTON_BOX_TIME = 0.5;
const _UPDATE_SIZE_TIME = 0.3;
const _UPDATE_SIZE_TRANSITION = 'easeOutCubic';

const _CAMERA_SPINNER_SIZE = 20;

const _CAMERA_X_RESOLUTION = 640;
const _CAMERA_Y_RESOLUTION = 480;

const _CAPTION_LABEL_NAME_NORMAL = "photo-thing-caption-label";
const _CAPTION_LABEL_NAME_CAMERA = "photo-thing-camera-label";

const _PHOTO_TAKING_COUNTDOWN = 3; // in seconds

function Camera(args) {
    this._init(args);
}

Camera.prototype = {
    _init : function(args) {
        this._style = args.style;
        this._photoTakingCountdown = -1;

        this._createMainBox();
        this._createVideoTexture();
        this._createSpinner();
        this._createCamera();
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "photo-thing-camera-box" });
    },

    _createVideoTexture : function() {
        this._videoTexture =
            new Clutter.Texture({ keepAspectRatio: true });

        this._mainBox.append(this._videoTexture,
                             TheBoard.BoxPackFlags.EXPAND);
    },

    _createSpinner : function() {
        this._spinner =
            new Mx.Spinner({ width: _CAMERA_SPINNER_SIZE,
                             height: _CAMERA_SPINNER_SIZE,
                             name: "photo-thing-camera-spinner" });

        this._mainBox.append(this._spinner,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._spinner,
                                            TheBoard.BoxAlignment.CENTER,
                                            TheBoard.BoxAlignment.CENTER);
    },

    _createCamera : function() {
        // We use FileUtil.new() here because FileUtil
        // is a supposed to be a singleton which is returned
        // from cheese_fileutil_new()
        let fileUtil = Cheese.FileUtil.new();

        this._filename =
            fileUtil.get_new_media_filename(Cheese.MediaMode.PHOTO);

        this._camera = Cheese.Camera.new(this._videoTexture,
                                         null,
                                         _CAMERA_X_RESOLUTION,
                                         _CAMERA_Y_RESOLUTION);

        this._camera.connect("state-flags-changed",
                             Lang.bind(this,
                                       this._onCameraStateChanged));

        this._camera.connect("photo-saved",
                             Lang.bind(this,
                                       this._onCameraPhotoSaved));

        TheBoard.cheese_camera_start_async(this._camera,
                                           Lang.bind(this,
                                                     this._onCameraStartAsync));
    },

    _onCameraStartAsync : function(camera, result, data) {
        try {
            TheBoard.cheese_camera_start_finish(camera, result);
        } catch(e) {
            // FIXME: Show error message to user
        }
    },

    _onCameraStateChanged : function(camera, gstState) {
        if (gstState != Gst.State.PLAYING) {
            return;
        }

        this._spinner.hide();

        this._photoTakingCountdown = _PHOTO_TAKING_COUNTDOWN;

        this.emit("caption-changed");

        this._photoTakingTimeoutId =
            Mainloop.timeout_add_seconds(1,
                                         Lang.bind(this,
                                                   this._onPhotoTakingTimeout));
    },

    _onCameraPhotoSaved : function(camera, filename) {
        // We have to emit this signal on idle here
        // because apparently libcheese doesn't handle
        // very well the case where we stop camera in
        // response to "photo-saved" signal.
        let emitPhotoSaved = function() {
            this.emit("photo-saved");
        };

        Mainloop.idle_add(Lang.bind(this, emitPhotoSaved));
    },

    _onPhotoTakingTimeout : function(filename) {
        this._photoTakingCountdown--;

        // We want to give one more second for the
        // "Say cheese" message just before actually
        // taking the photo
        if (this._photoTakingCountdown < 0) {
            this._camera.take_photo(this._filename);
        } else {
            this.emit("caption-changed");
        }

        return (this._photoTakingCountdown >= 0);
    },

    destroy : function() {
        if (this._photoTakingTimeoutId) {
            Mainloop.source_remove(this._photoTakingTimeoutId);
            delete this._photoTakingTimeoutId;
        }

        if (this._camera) {
            this._camera.stop();
            delete this._camera;
        }

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get caption() {
        if (this._photoTakingCountdown == 0) {
            return Gettext.gettext("Say cheese!");
        } else if (this._photoTakingCountdown > 0) {
            return "" + this._photoTakingCountdown;
        } else {
            return Gettext.gettext("Starting webcam");
        }
    },

    get filename() {
        return this._filename;
    },

    get actor() {
        return this._mainBox;
    }
}

Signals.addSignalMethods(Camera.prototype);

function PhotoThing(args) {
    this._init(args);
}

PhotoThing.prototype = {
    __proto__: Thing.Thing.prototype,

    _init : function(args) {
        args = args || {};

        args.content = this;

        if ('initialWidth' in args) {
            this._initialWidth = args.initialWidth;
        } else {
            this._initialWidth = 320;
        }

        if ('initialHeight' in args) {
            this._initialHeight = args.initialHeight;
        } else {
            this._initialHeight = 213;
        }

        if ('minWidth' in args) {
            this._minWidth = args.minWidth;
        } else {
            this._minWidth = 320;
        }

        if ('minHeight' in args) {
            this._minHeight = args.minHeight;
        } else {
            this._minHeight = 213;
        }

        this._style = new Mx.Style();
        this._style.load_from_file(STYLE);

        this._createPhotoBox();
        this._createContentBox();
        this._createPhoto();
        this._createCaptionText();
        this._createTapes();

        Thing.Thing.prototype._init.apply(this, [args]);
    },

    _createPhotoBox : function() {
        this._photoBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "photo-thing-photo-box" });
    },

    _createContentBox : function() {
        this._contentBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL });

        this._photoBox.append(this._contentBox,
                              TheBoard.BoxPackFlags.EXPAND);
    },

    _createPhoto : function() {
        this._photo =
            new Clutter.Texture({ keepAspectRatio: true,
                                  loadAsync: true });

        this._contentBox.append(this._photo,
                                TheBoard.BoxPackFlags.EXPAND);
    },

    _createCaptionText : function() {
        this._captionLabel =
            new Mx.Label({ xAlign: Mx.Align.MIDDLE,
                           style: this._style,
                           name: _CAPTION_LABEL_NAME_NORMAL });

        this._captionLabel.clutterText.lineAlignment = Pango.Alignment.CENTER;

        this._captionLabel.connect("key-press-event",
                                   Lang.bind(this, this._onCaptionTextKeyPressEvent));

        this._captionLabel.clutterText.connect("text-changed",
                                               Lang.bind(this,
                                                         this._onCaptionTextChanged));

        this._photoBox.append(this._captionLabel,
                              TheBoard.BoxPackFlags.NONE);
    },

    _createTapes : function() {
        this._topTape =
            new Clutter.Texture({ keepAspectRatio: true,
                                  loadAsync: true,
                                  filename: _TAPE_IMAGE });

        this._photoBox.append(this._topTape,
                              TheBoard.BoxPackFlags.FIXED);

        this._topTape.anchorX = 25;
        this._topTape.anchorY = 15;

        this._bottomTape =
            new Clutter.Texture({ keepAspectRatio: true,
                                  loadAsync: true,
                                  filename: _TAPE_IMAGE });

        this._photoBox.append(this._bottomTape,
                              TheBoard.BoxPackFlags.FIXED);

        this._photoBox.set_fixed_child_align(this._bottomTape,
                                             TheBoard.BoxAlignment.END,
                                             TheBoard.BoxAlignment.END);

        this._bottomTape.anchorX = -35;
        this._bottomTape.anchorY = -25;
    },

    _connectPhotoSignals : function(fromState) {
        this._disconnectPhotoSignals();

        this._photoSizeChangeId =
            this._photo.connect("size-change",
                                Lang.bind(this,
                                          this._onPhotoSizeChange));

        this._photoLoadFinishedId =
            this._photo.connect("load-finished",
                                Lang.bind(this,
                                          this._onPhotoLoadFinished,
                                          fromState));
    },

    _disconnectPhotoSignals : function() {
        if (this._photoSizeChangeId) {
            this._photo.disconnect(this._photoSizeChangeId);
            delete this._photoSizeChangeId
        }

        if (this._photoLoadFinishedId) {
            this._photo.disconnect(this._photoLoadFinishedId);
            delete this._photoLoadFinishedId;
        }
    },

    _updateSpinner : function() {
        // FIXME: show/hide spinner depending on the
        // loading state of the photo
    },

    _updateImageFilename : function(imageFilename, fromState) {
        if (this._imageFilename == imageFilename) {
            return;
        }

        this._imageFilename = imageFilename;

        if (this._imageFilename) {
            this._connectPhotoSignals(fromState);
            this._updateSpinner();

            // hide photo while loading the new image file
            this._photo.opacity = 0;

            // start loading the new image file
            this._photo.filename = this._imageFilename;
        }

        if (!fromState) {
            this.emit('save');
        }
    },

    _updateInitialSize : function(width, height) {
        let aspectRatio = width / height;

        let newWidth;
        let newHeight;

        // FIXME: the image might be actually smaller
        // than the space available. What do we do in
        // that case?

        if (width >= height) {
            newWidth = this._initialWidth;
            newHeight = newWidth / aspectRatio;
        } else {
            newHeight = this._initialHeight;
            newWidth = newHeight * aspectRatio;
        }

        this._photoWidth = newWidth;
        this._photoHeight = newHeight;
    },

    _updatePhotoWithFileChooser : function() {
        let chooser = new Gtk.FileChooserDialog();

        chooser.add_button(Gtk.STOCK_CANCEL,
                           Gtk.ResponseType.REJECT);
        chooser.add_button(Gtk.STOCK_OK,
                           Gtk.ResponseType.ACCEPT);

        let imageFilter = new Gtk.FileFilter();

        imageFilter.set_name('Images');
        imageFilter.add_pixbuf_formats()

        chooser.add_filter(imageFilter);

        let picsDir = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_PICTURES);
        chooser.set_current_folder(picsDir);

        chooser.set_transient_for(this.context.gtkWindow);

        let response = chooser.run();

        if (response != Gtk.ResponseType.ACCEPT) {
            chooser.destroy();
            return;
        }

        let filename = chooser.get_filename();

        // Destroy dialog first, then set image
        chooser.destroy();

        // we have to restore key focus on stage
        // because the chooser grabs key focus
        this._captionLabel.clutterText.grab_key_focus();

        this._updateImageFilename(filename,
                                  false /* not from state*/);
    },

    _startEditingCaption : function() {
        this._captionLabel.clutterText.editable = true;
        this._captionLabel.clutterText.grab_key_focus();
    },

    _stopEditingCaption : function() {
        this._captionLabel.clutterText.editable = false;
    },

    _startTakingPhoto : function() {
        if (this._camera) {
            return;
        }

        this._updateInitialSize(_CAMERA_X_RESOLUTION,
                                _CAMERA_Y_RESOLUTION);

        this._animateToPhotoSize(false /* not from state */);

        this._camera = new Camera({ style: this._style });

        this._cameraPhotoTakenId =
            this._camera.connect("photo-saved",
                                 Lang.bind(this, this._onPhotoSavedFromCamera));

        this._cameraCaptionChangedId =
            this._camera.connect("caption-changed",
                                 Lang.bind(this, this._onCameraCaptionChanged));

        this._contentBox.append(this._camera.actor,
                                TheBoard.BoxPackFlags.FIXED);

        this._contentBox.set_fixed_child_align(this._camera.actor,
                                               TheBoard.BoxAlignment.FILL,
                                               TheBoard.BoxAlignment.FILL);

        this._textBeforeTakingPhoto = this._captionLabel.text;

        this._captionLabel.name = _CAPTION_LABEL_NAME_CAMERA;
        this._updateCaptionFromCamera();
        this._stopEditingCaption();
    },

    _stopTakingPhoto : function() {
        if (!this._camera) {
            return;
        }

        if (this._cameraPhotoTakenId) {
            this._camera.disconnect(this._cameraPhotoTakenId);
            delete this._cameraPhotoTakenId;
        }

        if (this._cameraCaptionChangedId) {
            this._camera.disconnect(this._cameraCaptionChangedId);
            delete this._cameraCaptionChangedId;
        }

        this._camera.destroy();
        delete this._camera;

        this._captionLabel.name = _CAPTION_LABEL_NAME_NORMAL;
        this._captionLabel.text = this._textBeforeTakingPhoto;
        delete this._textBeforeTakingPhoto;
    },

    _animateToPhotoSize : function(fromState) {
        let [minTextHeight, naturalTextHeight] =
            this._captionLabel.get_preferred_height(-1);

        let thingWidth = this._photoWidth;
        let thingHeight = this._photoHeight + naturalTextHeight;

        this._minWidth = thingWidth;
        this._minHeight = thingHeight;

        if (!fromState) {
            Tweener.addTween(this.actor,
                             { width: thingWidth,
                               height: thingHeight,
                               time: fromState ? 0 : _UPDATE_SIZE_TIME,
                               transition: _UPDATE_SIZE_TRANSITION });
        }

        Tweener.addTween(this._photo,
                         { opacity: 255,
                           delay: fromState ? 0 : _UPDATE_SIZE_TIME,
                           time: fromState ? 0 : _UPDATE_SIZE_TIME });

        delete this._photoWidth;
        delete this._photoHeight;
    },

    _updateCaptionFromCamera : function() {
        this._captionLabel.text = this._camera.caption;
    },

    _maybeSetDateTimeCaption : function(textBeforeTakingPhoto) {
        if (textBeforeTakingPhoto) {
            return;
        }

        let now = new Date();

        this._captionLabel.text =
            now.toLocaleDateString() + " " + now.toLocaleTimeString();
    },

    _onPhotoSavedFromCamera : function(camera) {
        // Copy previous caption value before calling
        // _stopTakingPhoto() because it will clear and
        // possibly restore this value in captionLabel
        let textBeforeTakingPhoto = this._textBeforeTakingPhoto;

        this._stopTakingPhoto();

        // This will set captionLabel value to a date
        // in case a photo was taken while no caption had
        // been defined
        this._maybeSetDateTimeCaption(textBeforeTakingPhoto);

        this._startEditingCaption();

        this._updateImageFilename(camera.filename,
                                  false /* not from state */);
    },

    _onCameraCaptionChanged : function(camera) {
        this._updateCaptionFromCamera();
    },

    _onCaptionTextKeyPressEvent : function(o, event) {
        let key = event.get_key_symbol();

        switch (key) {
        case Clutter.Return:
            this._updatePhotoWithFileChooser();
            return true;
        case Clutter.Escape:
            this.emit("deactivate");
            return true;
        }

        return false;
    },

    _onCaptionTextChanged : function() {
        // Don't trigger save when we set the label
        // text as part of the photo taking countdown
        if (this._camera) {
            return;
        }

        this.emit('save');
    },

    _onPhotoSizeChange : function(photo, width, height) {
        if (this._photoWidth > 0 && this._photoHeight > 0) {
            return;
        }

        this._updateInitialSize(width, height);
    },

    _onPhotoLoadFinished : function(texture, error, fromState) {
        this._disconnectPhotoSignals();
        this._updateSpinner();
        this._animateToPhotoSize(fromState);
    },

    activate : function() {
        this._startEditingCaption();
    },

    deactivate : function() {
        this._stopEditingCaption();
        this._stopTakingPhoto();
    },

    loadState : function(state) {
        if ('imageFilename' in state) {
            let fromState = 'width' in state &&
                            'height' in state;

            this._updateImageFilename(state.imageFilename,
                                      fromState);
        }

        if ('text' in state) {
            this._captionLabel.text = state.text;
        }
    },

    getState : function() {
        return { imageFilename: this._imageFilename,
                 text: this._captionLabel.text };
    },

    doAction : function(actionName, actionArgs) {
        if (actionName == "chooseFile") {
            this._updatePhotoWithFileChooser();
        } else if (actionName == "takePhoto") {
            this._startTakingPhoto();
        }
    },

    validateSize : function(width, height) {
        // minWidth and minHeight always have a valid aspect
        // ratio once the image is loaded (see _onPhotoLoadFinished)
        let aspectRatio = this._minWidth / this._minHeight;

        // the point here is to keep aspect ratio while
        // resize the photo thing
        if (this._minWidth > this._minHeight) {
            return [width, width / aspectRatio];
        } else {
            return [height * aspectRatio, height];
        }
    },

    destroy : function() {
        this._disconnectPhotoSignals();
        this._stopTakingPhoto();

        if (this._photoBox) {
            this._photoBox.destroy();
            delete this._photoBox;
        }
    },

    get initialWidth() {
        return this._initialWidth;
    },

    get initialHeight() {
        return this._initialHeight;
    },

    get minWidth() {
        return this._minWidth;
    },

    get minHeight() {
        return this._minHeight;
    },

    get contentActor() {
        return this._photoBox;
    }
}

function create(args) {
    return new PhotoThing(args);
}

function createToolbar(args) {
    let toolbar =
        new Toolbar.Toolbar({ title: NAME,
                              visible: false });

    let toolBox =
        new ToolBox.ToolBox({ title: Gettext.gettext("Load from") });

    toolBox.addButton({ label: Gettext.gettext("File"),
                        actionName: "chooseFile" });

    if (Features.HAVE_CHEESE) {
        toolBox.addButton({ label: Gettext.gettext("Webcam"),
                            actionName: "takePhoto" });
    }

    toolbar.addToolBox(toolBox);

    return toolbar;
}
