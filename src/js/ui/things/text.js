// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;
const Mainloop = imports.mainloop;

// gi imports
const Clutter = imports.gi.Clutter;
const Mx = imports.gi.Mx;
const Pango = imports.gi.Pango;
const TheBoard = imports.gi.TheBoard;

// ui imports
const Thing = imports.ui.thing;
const ToolBox = imports.ui.toolBox;
const Toolbar = imports.ui.toolbar;

const _FONT_SIZE_SMALL  = "small";
const _FONT_SIZE_MEDIUM = "medium";
const _FONT_SIZE_BIG    = "big";

function TextThing(args) {
    this._init(args);
}

TextThing.prototype = {
    __proto__: Thing.Thing.prototype,

    _init : function(args) {
        args = args || {};

        if ('initialWidth' in args) {
            this._initialWidth = args.initialWidth;
        } else {
            this._initialWidth = 320;
        }

        if ('initialHeight' in args) {
            this._initialHeight = args.initialHeight;
        } else {
            this._initialHeight = 344;
        }

        if ('minWidth' in args) {
            this._minWidth = args.minWidth;
        } else {
            this._minWidth = 120;
        }

        if ('minHeight' in args) {
            this._minHeight = args.minHeight;
        } else {
            this._minHeight = 150;
        }

        if ('singleLine' in args) {
            this._singleLine = args.singleLine;
        } else {
            this._singleLine = false;
        }

        if ('editable' in args) {
            this._editable = args.editable;
        } else {
            this._editable = true;
        }

        if ('style' in args) {
            this._style = args.style;
        } else {
            this._style = null;
        }

        if ('tiledBgFile' in args) {
            this._tiledBgFile = args.tiledBgFile;
        } else {
            this._tiledBgFile = null;
        }

        this._loadingState = false;

        this._fontSize = _FONT_SIZE_MEDIUM;

        args.content = this;

        this._createBgBox();

        if (this._tiledBgFile) {
            this._createTiledBackground();
        }

        this._createScrollView();

        this._createTextBox();
        this._createText();

        Thing.Thing.prototype._init.apply(this, [args]);
    },

    _createBgBox : function() {
        this._bgBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "text-thing-bg-box" });
    },

    _createTiledBackground : function() {
        this._tiledBackground =
            new Clutter.Texture({ loadAsync: true,
                                  repeatX: true,
                                  repeatY: true });

        this._tiledBackground.filename = this._tiledBgFile;

        this._bgBox.append(this._tiledBackground,
                           TheBoard.BoxPackFlags.FIXED);

        this._bgBox.set_fixed_child_align(this._tiledBackground,
                                          TheBoard.BoxAlignment.FILL,
                                          TheBoard.BoxAlignment.FILL);
    },

    _createScrollView : function() {
        this._scrollView =
            new Mx.ScrollView({ reactive: false,
                                style: this._style,
                                name: "text-thing-scroll-view" });

        if (this._singleLine) {
            this._scrollView.scrollPolicy = Mx.ScrollPolicy.NONE;
        } else {
            this._scrollView.scrollPolicy = Mx.ScrollPolicy.VERTICAL;
        }

        let clickAction = new Clutter.ClickAction();

        clickAction.connect("clicked",
                            Lang.bind(this, this._onScrollViewClicked));

        this._scrollView.add_action(clickAction);

        this._bgBox.append(this._scrollView,
                           TheBoard.BoxPackFlags.EXPAND);
    },

    _createTextBox : function() {
        this._textBox =
            new Mx.BoxLayout({ orientation: Mx.Orientation.VERTICAL,
                               style: this._style,
                               name: "text-thing-text-box" });

        if (this._singleLine) {
            this._textBox.xAlign = TheBoard.BoxAlignment.START;
            this._textBox.yAlign = TheBoard.BoxAlignment.CENTER;
        } else {
            this._textBox.xAlign = TheBoard.BoxAlignment.FILL;
            this._textBox.yAlign = TheBoard.BoxAlignment.START;
        }

        this._scrollView.add_actor(this._textBox);
    },

    _createText : function() {
        this._label = new Mx.Label({ style: this._style });

        this._updateFont();

        this._label.clutterText.lineWrap = !this._singleLine;
        this._label.clutterText.singleLineMode = this._singleLine;
        this._label.clutterText.ellipsize = Pango.EllipsizeMode.NONE;

        this._label.clutterText.lineWrapMode = this._singleLine ?
                                               Pango.WrapMode.WORD :
                                               Pango.WrapMode.WORD_CHAR;

        this._label.connect("key-press-event",
                            Lang.bind(this, this._onTextKeyPressEvent));

        this._label.clutterText.connect("notify::position",
                                        Lang.bind(this, this._onCursorPositionChanged));

        this._label.clutterText.connect("text-changed",
                                        Lang.bind(this, this._onTextChanged));

        this._label.clutterText.connect("activate",
                                        Lang.bind(this, this._onTextActivate));

        this._textBox.add_actor(this._label, 0);
        this._textBox.get_child_meta(this._label).expand = true;
    },

    _maybeUpdateSizeFromText : function() {
        if (this._singleLine && !this._loadingState) {
            let [minWidth, naturalWidth] =
                this._label.get_preferred_width(-1);

            let [minHeight, naturalHeight] =
                this._label.get_preferred_height(naturalWidth);

            let thingPadding =
                TheBoard.mx_stylable_get_padding(this.actor);

            let textBoxPadding =
                TheBoard.mx_stylable_get_padding(this._textBox);

            let newWidth = naturalWidth +
                           thingPadding.left + textBoxPadding.left +
                           thingPadding.right + textBoxPadding.right;

            let newHeight = naturalHeight +
                            thingPadding.top + textBoxPadding.top +
                            thingPadding.bottom + textBoxPadding.bottom;

            this.setSize(newWidth, newHeight);
        }
    },

    _ensureCursorIsVisible : function() {
        let position = this._label.clutterText.position;

        let [success, x, y, lineHeight] =
            this._label.clutterText.position_to_coords(position);

        let visibleArea = new Clutter.Geometry();

        visibleArea.x = x;
        visibleArea.y = y;
        visibleArea.width = 0;
        visibleArea.height = lineHeight;

        // FIXME: This doesn't seem to be working realibly
        // with MxBoxLayout.
        this._scrollView.ensure_visible(visibleArea);
    },

    _updateFont : function() {
        this._label.set_name("text-thing-label-" + this._fontSize);
        Mainloop.idle_add(Lang.bind(this, this._ensureCursorIsVisible));
    },

    _setFontSize : function(fontSize) {
        if (this._fontSize == fontSize) {
            return;
        }

        this._fontSize = fontSize;
        this._updateFont();

        if (this._singleLine) {
            this._maybeUpdateSizeFromText();
        }

        this.emit('save');
    },

    _onTextKeyPressEvent : function(o, event) {
        let key = event.get_key_symbol();

        switch (key) {
        case Clutter.Return:
            let position = this._label.clutterText.position;
            this._label.clutterText.insert_text("\n", position);
            return true;
        case Clutter.Escape:
            this.emit("deactivate");
            return true;
        }

        return false;
   },

    _onScrollViewClicked : function() {
        this.emit("activate");
        return true;
    },

    _onTextChanged : function() {
        Mainloop.idle_add(Lang.bind(this, this._ensureCursorIsVisible));
        this._maybeUpdateSizeFromText();
    },

    _onCursorPositionChanged : function() {
        Mainloop.idle_add(Lang.bind(this, this._ensureCursorIsVisible));
    },

    _onTextActivate : function(o) {
        if (this._singleLine) {
            this.emit("deactivate");
        }
    },

    activate : function() {
        if (this._editable) {
            this._label.clutterText.editable = true;
            this._label.clutterText.grab_key_focus();

            this._ensureCursorIsVisible();
        }
    },

    deactivate : function() {
        if (this._editable) {
            this._label.clutterText.editable = false;
        }
    },

    loadState : function(state) {
        this._loadingState = true;

        if ('text' in state) {
            this._label.text = state.text;
        }

        if ('fontSize' in state) {
            this._setFontSize(state.fontSize);
        }

        this._loadingState = false;
    },

    getState : function() {
        return { text: this._label.text,
                 fontSize: this._fontSize };
    },

    doAction : function(actionName, actionArgs) {
        if (actionName == "setFontSize") {
            this._setFontSize(actionArgs.fontSize);
        }
    },

    validateSize : function(width, height) {
        return [width, height];
    },

    destroy : function() {
        if (this._bgBox) {
            this._bgBox.destroy();
            delete this._bgBox;
        }
    },

    get initialWidth() {
        return this._initialWidth;
    },

    get initialHeight() {
        return this._initialHeight;
    },

    get minWidth() {
        return this._minWidth;
    },

    get minHeight() {
        return this._minHeight;
    },

    get mainBox() {
        return this._bgBox;
    },

    get contentActor() {
        return this._bgBox;
    }
}

function TextToolbar(args) {
    this._init(args);
}

TextToolbar.prototype = {
    __proto__: Toolbar.Toolbar.prototype,

    _init : function(args) {
        Toolbar.Toolbar.prototype._init.apply(this, [args]);

        this._createFontSizeToolBox();
    },

    _createFontSizeToolBox : function() {
        this._fontSizeToolBox =
            new ToolBox.ToolBox({ title: Gettext.gettext("Font Size"),
                                  isButtonGroup: true });

        this._fontSizeButtons = {};

        this._fontSizeButtons[_FONT_SIZE_SMALL] =
            this._fontSizeToolBox.addButton({
                label: Gettext.gettext("Small"),
                actionName: "setFontSize",
                actionArgs: { fontSize: _FONT_SIZE_SMALL }
            });

        this._fontSizeButtons[_FONT_SIZE_MEDIUM] =
            this._fontSizeToolBox.addButton({
                label: Gettext.gettext("Medium"),
                actionName: "setFontSize",
                actionArgs: { fontSize: _FONT_SIZE_MEDIUM }
            });

        this._fontSizeButtons[_FONT_SIZE_BIG] =
            this._fontSizeToolBox.addButton({
                label: Gettext.gettext("Big"),
                actionName: "setFontSize",
                actionArgs: { fontSize: _FONT_SIZE_BIG }
            });

        this.addToolBox(this._fontSizeToolBox);
    },

    loadState : function(state) {
        if ('fontSize' in state) {
            let activeButton = this._fontSizeButtons[state.fontSize];
            this._fontSizeToolBox.setActiveButton(activeButton);
        }
    }
}

function createToolbar(args) {
    return new TextToolbar({ title: args.title,
                             visible: false });
}
