// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;

// gi imports
const Mx = imports.gi.Mx;

// ui imports
const Text = imports.ui.things.text;

// util imports
const Path = imports.util.path;

const NAME = Gettext.gettext("Label");
const STYLE = Path.THINGS_DATA_DIR + "label/style.css";

function create(args) {
    args = args || {};

    args.canResize = false;
    args.singleLine = true;
    args.initialWidth = 150;
    args.initialHeight = 50;
    args.minWidth = 150;
    args.minHeight = 0;

    args.style = new Mx.Style();
    args.style.load_from_file(STYLE);

    return new Text.TextThing(args);
}

function createToolbar(args) {
    args = args || {};

    args.title = NAME;

    return Text.createToolbar(args);
}
