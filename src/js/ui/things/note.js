// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;

// gi imports
const Mx = imports.gi.Mx;

// ui imports
const Text = imports.ui.things.text;

// util imports
const Path = imports.util.path;

const NAME = Gettext.gettext("Note");
const STYLE = Path.THINGS_DATA_DIR + "note/style.css";

const _TILED_BG_FILE = Path.THINGS_DATA_DIR + "note/lines.png";

function create(args) {
    args = args || {};

    args.style = new Mx.Style();
    args.style.load_from_file(STYLE);

    args.tiledBgFile = _TILED_BG_FILE;

    return new Text.TextThing(args);
}

function createToolbar(args) {
    args = args || {};

    args.title = NAME;

    return Text.createToolbar(args);
}
