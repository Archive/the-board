// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Tweener = imports.tweener.tweener;

// gi imports
const Clutter = imports.gi.Clutter;
const ClutterGst = imports.gi.ClutterGst;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Mx = imports.gi.Mx;
const Pango = imports.gi.Pango;
const TheBoard = imports.gi.TheBoard;

// ui imports
const Thing = imports.ui.thing;
const ToolBox = imports.ui.toolBox;
const Toolbar = imports.ui.toolbar;

// util imports
const Path = imports.util.path;

const NAME = Gettext.gettext("Video");
const STYLE = Path.THINGS_DATA_DIR + "video/style.css";

const _STRIP_CELL_IMAGE = Path.THINGS_DATA_DIR + "video/strip-cell.png";

const _SHOW_BUTTON_BOX_TIME = 0.5;
const _UPDATE_SIZE_TIME = 0.3;
const _UPDATE_SIZE_TRANSITION = 'easeOutCubic';

function VideoThing(args) {
    this._init(args);
}

VideoThing.prototype = {
    __proto__: Thing.Thing.prototype,

    _init : function(args) {
        args = args || {};

        args.content = this;

        if ('initialWidth' in args) {
            this._initialWidth = args.initialWidth;
        } else {
            this._initialWidth = 438;
        }

        if ('initialHeight' in args) {
            this._initialHeight = args.initialHeight;
        } else {
            this._initialHeight = 260;
        }

        if ('minWidth' in args) {
            this._minWidth = args.minWidth;
        } else {
            this._minWidth = 438;
        }

        if ('minHeight' in args) {
            this._minHeight = args.minHeight;
        } else {
            this._minHeight = 260;
        }

        this._style = new Mx.Style();
        this._style.load_from_file(STYLE);

        this._createVideoBox();
        this._createPlaybackBox();
        this._createCaptionText();

        Thing.Thing.prototype._init.apply(this, [args]);
    },

    _createVideoBox : function() {
        this._videoBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "video-thing-video-box" });

        this._createStrip();
        this._createContentBox();
        this._createStrip();
    },

    _createContentBox : function() {
        this._contentBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "video-thing-content-box" });

        this._videoBox.append(this._contentBox,
                              TheBoard.BoxPackFlags.EXPAND);
    },

    _createStrip : function() {
        let strip =
            new Clutter.Texture({ repeatX: false,
                                  repeatY: true,
                                  filename: _STRIP_CELL_IMAGE });

        this._videoBox.append(strip,
                              TheBoard.BoxPackFlags.NONE);
    },

    _createPlaybackBox : function() {
        this._playbackBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "video-thing-playback-box" });

        this._createVideo();
        this._createVideoControlsBox();
        this._createVideoBorderBox();

        this._contentBox.append(this._playbackBox,
                                TheBoard.BoxPackFlags.EXPAND);
    },

    _createVideo : function() {
        this._video =
            new ClutterGst.VideoTexture({ syncSize: false,
                                          audioVolume: 0.5 });

        this._video.connect("notify::progress",
                            Lang.bind(this, this._onVideoProgressChanged));

        this._playbackBox.append(this._video,
                                 TheBoard.BoxPackFlags.EXPAND);
    },

    _createVideoControlsBox : function() {
        // This box is needed to add proper spacing between the
        // video controls and the edge of video playback frame
        let controlsBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               style: this._style,
                               name: "video-thing-controls-box" });

        this._videoControlsBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               opacity: 0,
                               visible: false,
                               style: this._style,
                               name: "video-thing-video-controls-box" });

        this._createPlayButton();
        this._createProgressBar();
        this._createTimeLabel();

        controlsBox.append(this._videoControlsBox,
                           TheBoard.BoxPackFlags.EXPAND);

        this._playbackBox.append(controlsBox,
                                 TheBoard.BoxPackFlags.FIXED);

        this._playbackBox.set_fixed_child_align(controlsBox,
                                                TheBoard.BoxAlignment.FILL,
                                                TheBoard.BoxAlignment.END);
    },

    _createPlayButton : function() {
        this._playButton =
            new Mx.Button({ isToggle: true,
                            style: this._style,
                            name: "video-thing-play-button" });

        this._playButton.connect("notify::toggled",
                                 Lang.bind(this,
                                           this._onPlayButtonToggled));

        this._videoControlsBox.append(this._playButton,
                                      TheBoard.BoxPackFlags.NONE);
    },

    _createProgressBar : function() {
        this._progressBar =
            new Mx.ProgressBar({ reactive: true,
                                 height: 12,
                                 style: this._style,
                                 name: "video-thing-progress-bar" });

        this._updateProgressBar();

        this._progressBar.connect("button-press-event",
                                  Lang.bind(this,
                                            this._onProgressBarButtonPressEvent));

        this._videoControlsBox.append(this._progressBar,
                                      TheBoard.BoxPackFlags.EXPAND);

        this._videoControlsBox.set_child_align(this._progressBar,
                                               TheBoard.BoxAlignment.FILL,
                                               TheBoard.BoxAlignment.CENTER);
    },

    _createTimeLabel : function() {
        this._timeLabel =
            new Mx.Label({ yAlign: Mx.Align.MIDDLE,
                           text: "00:12:00",
                           anchorY: -2,
                           style: this._style,
                           name: "video-thing-time-label" });

        this._updateTimeLabel();

        this._videoControlsBox.append(this._timeLabel,
                                      TheBoard.BoxPackFlags.END);

        this._videoControlsBox.set_child_align(this._timeLabel,
                                               TheBoard.BoxAlignment.START,
                                               TheBoard.BoxAlignment.CENTER);
    },

    _createVideoBorderBox : function() {
        this._videoBorderBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "video-thing-video-border-box" });

        this._playbackBox.append(this._videoBorderBox,
                                 TheBoard.BoxPackFlags.FIXED);

        this._playbackBox.set_fixed_child_align(this._videoBorderBox,
                                                TheBoard.BoxAlignment.FILL,
                                                TheBoard.BoxAlignment.FILL);
    },

    _createCaptionText : function() {
        this._captionLabel =
            new Mx.Label({ xAlign: Mx.Align.MIDDLE,
                           style: this._style,
                           name: "video-thing-caption-label" });

        this._captionLabel.clutterText.lineAlignment = Pango.Alignment.CENTER;

        this._captionLabel.connect("key-press-event",
                                   Lang.bind(this, this._onCaptionTextKeyPressEvent));

        this._captionLabel.clutterText.connect("text-changed",
                                               Lang.bind(this,
                                                         this._onCaptionTextChanged));

        this._contentBox.append(this._captionLabel,
                                TheBoard.BoxPackFlags.NONE);
    },

    _connectVideoSignals : function(fromState) {
        this._disconnectVideoSignals();

        this._videoEosId =
            this._video.connect("eos",
                                Lang.bind(this,
                                          this._onVideoEos));

        this._videoErrorId =
            this._video.connect("error",
                                Lang.bind(this,
                                          this._onVideoError));

        this._videoSizeChangeId =
            this._video.connect("size-change",
                                Lang.bind(this,
                                          this._onVideoSizeChange,
                                          fromState));
    },

    _disconnectVideoSignals : function() {
        if (this._videoEosId) {
            this._video.disconnect(this._videoEosId);
            delete this._videoEosId
        }

        if (this._videoErrorId) {
            this._video.disconnect(this._videoErrorId);
            delete this._videoErrorId;
        }

        if (this._videoSizeChangeId) {
            this._video.disconnect(this._videoSizeChangeId);
            delete this._videoSizeChangeId
        }
    },

    _updateSpinner : function() {
        // FIXME: show/hide spinner depending on the
        // loading state of the video
    },

    _updateVideoFilename : function(videoFilename, fromState) {
        if (this._videoFilename == videoFilename) {
            return;
        }

        this._videoFilename = videoFilename;

        if (this._videoFilename) {
            this._connectVideoSignals(fromState);
            this._updateSpinner();

            // hide video while loading the new video file
            this._video.opacity = 0;

            // start loading the new video file
            this._video.set_filename(this._videoFilename);

            this._video.playing = true;
            this._playButton.toggled = true;

            if (fromState) {
                let pauseVideo = function() {
                    this._video.playing = false;
                    this._playButton.toggled = false;
                };

                Mainloop.timeout_add(500,
                                     Lang.bind(this, pauseVideo));
            }
        }

        this._updateVideoControlsVisibility();

        if (!fromState) {
            this.emit('save');
        }
    },

    _updateInitialSize : function(width, height) {
        let aspectRatio = width / height;

        let newWidth;
        let newHeight;

        // FIXME: the video might be actually smaller
        // than the space available. What do we do in
        // that case?

        if (width >= height) {
            newWidth = this._initialWidth;
            newHeight = newWidth / aspectRatio;
        } else {
            newHeight = this._initialHeight;
            newWidth = newHeight * aspectRatio;
        }

        this._videoWidth = newWidth;
        this._videoHeight = newHeight;
    },

    _showVideoControlsBox : function() {
        Tweener.addTween(this._videoControlsBox,
                         { opacity: 255,
                           time: _SHOW_BUTTON_BOX_TIME,
                           onStart: function() {
                               this.show();
                           }});
    },

    _hideVideoControlsBox : function() {
        Tweener.addTween(this._videoControlsBox,
                         { opacity: 0,
                           time: _SHOW_BUTTON_BOX_TIME,
                           onComplete: function() {
                               this.hide();
                           }});
    },

    _updateVideoWithFileChooser : function() {
        let chooser = new Gtk.FileChooserDialog();

        chooser.add_button(Gtk.STOCK_CANCEL,
                           Gtk.ResponseType.REJECT);
        chooser.add_button(Gtk.STOCK_OK,
                           Gtk.ResponseType.ACCEPT);

        let videoFilter = new Gtk.FileFilter();

        //videoFilter.set_name('Videos');
        //chooser.add_filter(videoFilter);

        let videosDir = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS);
        chooser.set_current_folder(videosDir);

        chooser.set_transient_for(this.context.gtkWindow);

        let response = chooser.run();

        if (response != Gtk.ResponseType.ACCEPT) {
            chooser.destroy();
            return;
        }

        let filename = chooser.get_filename();

        // Destroy dialog first, then set video
        chooser.destroy();

        // we have to restore key focus on stage
        // because the chooser grabs key focus
        this._captionLabel.clutterText.grab_key_focus();

        this._updateVideoFilename(filename,
                                  false /* not from state*/);
    },

    _updateForVideoLoaded : function(fromState) {
        this._disconnectVideoSignals();
        this._updateSpinner();

        [minTextHeight, naturalTextHeight] =
            this._captionLabel.get_preferred_height(-1);

        let thingWidth = this._videoWidth;
        let thingHeight = this._videoHeight + naturalTextHeight;

        this._minWidth = thingWidth;
        this._minHeight = thingHeight;

        if (!fromState) {
            Tweener.addTween(this.actor,
                             { width: thingWidth,
                               height: thingHeight,
                               time: fromState ? 0 : _UPDATE_SIZE_TIME,
                               transition: _UPDATE_SIZE_TRANSITION });
        }

        Tweener.addTween(this._video,
                         { opacity: 255,
                           delay: fromState ? 0 : _UPDATE_SIZE_TIME,
                           time: fromState ? 0 : _UPDATE_SIZE_TIME });

        delete this._videoWidth;
        delete this._videoHeight;
    },

    _updateProgressBar : function() {
        this._progressBar.progress = this._video.get_progress();
    },

    _formatTimeComponent : function(n) {
        // FIXME: we need a sprinf equivalent to do
        // proper formatting here.
        return (n >= 10 ? n : "0" + n);
    },

    _updateTimeLabel : function() {
        let currentTime =
            Math.floor(this._video.duration * this._video.get_progress());

        let hours = Math.floor(currentTime / 3600);
        currentTime -= hours * 3600;

        let minutes = Math.floor(currentTime / 60);
        currentTime -= minutes * 60;

        let seconds = currentTime;

        this._timeLabel.text = this._formatTimeComponent(hours) + ":" +
                               this._formatTimeComponent(minutes) + ":" +
                               this._formatTimeComponent(seconds);
    },

    _updateVideoControlsVisibility : function() {
        let visible = this._videoFilename &&
                      (this.hover || this.active);

        if (visible) {
            this._showVideoControlsBox();
        } else {
            this._hideVideoControlsBox();
        }
    },

    _onCaptionTextKeyPressEvent : function(o, event) {
        let key = event.get_key_symbol();

        switch (key) {
        case Clutter.Return:
            this._updateVideoWithFileChooser();
            return true;
        case Clutter.Escape:
            this.emit("deactivate");
            return true;
        }

        return false;
    },

    _onCaptionTextChanged : function() {
        this.emit('save');
    },

    _onPlayButtonToggled : function() {
        this._video.playing = this._playButton.toggled;
    },

    _onProgressBarButtonPressEvent : function(progressBar, event) {
        let [eventX, eventY] = event.get_coords();

        let [transformedX, transformedY] =
            this._progressBar.get_transformed_position();

        let progress = (eventX - transformedX) / this._progressBar.width;
        this._video.set_progress(progress);
    },

    _onVideoProgressChanged : function() {
        this._updateProgressBar();
        this._updateTimeLabel();
    },

    _onVideoEos : function(video) {
        print('VIDEO EOS');
    },

    _onVideoError : function(video) {
        print('VIDEO ERROR');
    },

    _onVideoSizeChange : function(video, width, height, fromState) {
        if (this._videoWidth > 0 && this._videoHeight > 0) {
            return;
        }

        this._updateInitialSize(width, height);
        this._updateForVideoLoaded(fromState);
    },

    enter : function() {
        this._updateVideoControlsVisibility();
    },

    leave : function() {
        this._updateVideoControlsVisibility();
    },

    activate : function() {
        this._captionLabel.clutterText.editable = true;
        this._captionLabel.clutterText.grab_key_focus();
        this._updateVideoControlsVisibility();
    },

    deactivate : function() {
        this._captionLabel.clutterText.editable = false;
        this._updateVideoControlsVisibility();
    },

    loadState : function(state) {
        if ('videoFilename' in state) {
            let fromState = 'width' in state &&
                            'height' in state;

            this._updateVideoFilename(state.videoFilename,
                                      fromState);
        }

        if ('text' in state) {
            this._captionLabel.text = state.text;
        }
    },

    getState : function() {
        return { videoFilename: this._videoFilename,
                 text: this._captionLabel.text };
    },

    doAction : function(actionName, actionArgs) {
        if (actionName == "chooseFile") {
            this._updateVideoWithFileChooser();
        }
    },

    validateSize : function(width, height) {
        // minWidth and minHeight always have a valid aspect
        // ratio once the video is loaded (see _onVideoLoadFinished)
        let aspectRatio = this._minWidth / this._minHeight;

        // the point here is to keep aspect ratio while
        // resize the video thing
        if (this._minWidth > this._minHeight) {
            return [width, width / aspectRatio];
        } else {
            return [height * aspectRatio, height];
        }
    },

    destroy : function() {
        this._disconnectVideoSignals();

        if (this._videoBox) {
            this._videoBox.destroy();
            delete this._videoBox;
        }
    },

    get initialWidth() {
        return this._initialWidth;
    },

    get initialHeight() {
        return this._initialHeight;
    },

    get minWidth() {
        return this._minWidth;
    },

    get minHeight() {
        return this._minHeight;
    },

    get contentActor() {
        return this._videoBox;
    }
}

function create(args) {
    return new VideoThing(args);
}

function createToolbar(args) {
    let toolbar =
        new Toolbar.Toolbar({ title: NAME,
                              visible: false });

    let toolBox =
        new ToolBox.ToolBox({ title: Gettext.gettext("Load from") });

    toolBox.addButton({ label: Gettext.gettext("File"),
                        actionName: "chooseFile" });

    toolbar.addToolBox(toolBox);

    return toolbar;
}
