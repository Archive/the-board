// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Tweener = imports.tweener.tweener;

// gi imports
const Clutter = imports.gi.Clutter;
const GIO = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Mx = imports.gi.Mx;
const Pango = imports.gi.Pango;
const TheBoard = imports.gi.TheBoard;

// ui imports
const Thing = imports.ui.thing;
const ToolBox = imports.ui.toolBox;
const Toolbar = imports.ui.toolbar;

// util imports
const Path = imports.util.path;

const NAME = Gettext.gettext("Sound");
const STYLE = Path.THINGS_DATA_DIR + "sound/style.css";

const _INITIAL_WIDTH = 233;
const _INITIAL_HEIGHT = 152;

const _CASSETE_IMAGE = Path.THINGS_DATA_DIR + "sound/cassete.png";

const _SHOW_BUTTON_BOX_TIME = 0.5;

const _CAPTION_BOX_NAME_NORMAL = "sound-thing-caption-box";
const _CAPTION_BOX_NAME_RECORDING = "sound-thing-caption-box-recording";

// Magic numbers based on the graphics of the
// compact cassete
const _SPOOLS_Y = 49;
const _STOP_BUTTON_Y = _SPOOLS_Y + 35;

function SoundThing(args) {
    this._init(args);
}

SoundThing.prototype = {
    __proto__: Thing.Thing.prototype,

    _init : function(args) {
        args = args || {};

        args.content = this;
        args.canResize = false;

        // Storing voice messages in the music directory for now as, apparently,
        // there's no standard location for sound files
        let musicDir = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC);
        this._targetVoicePath = musicDir + "/The Board/";

        this._style = new Mx.Style();
        this._style.load_from_file(STYLE);

        this._createSoundPlayer();
        this._createSoundBox();
        this._createCaptionText();
        this._createSpools();

        Thing.Thing.prototype._init.apply(this, [args]);
    },

    _createSoundBox : function() {
        this._soundBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "sound-thing-sound-box" });

        this._createCasseteBg();
        this._createContentBox();
    },

    _createSoundPlayer : function() {
        this._player = new TheBoard.SoundPlayer();

        this._player.connect("notify::progress",
                             Lang.bind(this,
                                       this._onPlayerProgressChanged));

        this._player.connect("notify::duration",
                             Lang.bind(this,
                                       this._onPlayerDurationChanged));

        this._player.connect("notify::state",
                             Lang.bind(this,
                                       this._onPlayerStateChanged));
    },

    _createCasseteBg : function() {
        this._casseteBg =
            new Clutter.Texture({ filename: _CASSETE_IMAGE });

        this._soundBox.append(this._casseteBg,
                              TheBoard.BoxPackFlags.FIXED);

        this._soundBox.set_fixed_child_align(this._casseteBg,
                                             TheBoard.BoxAlignment.FILL,
                                             TheBoard.BoxAlignment.FILL);
    },

    _createContentBox : function() {
        this._contentBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "sound-thing-content-box" });

        this._createControlsBox();

        this._soundBox.append(this._contentBox,
                              TheBoard.BoxPackFlags.EXPAND);
    },

    _createControlsBox : function() {
        this._controlsBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.START,
                               style: this._style,
                               name: "sound-thing-controls-box" });

        this._createPlaybackBox();
        this._createTimeLabel();

        this._contentBox.append(this._controlsBox,
                                TheBoard.BoxPackFlags.FIXED);

        this._contentBox.set_fixed_child_align(this._controlsBox,
                                               TheBoard.BoxAlignment.FILL,
                                               TheBoard.BoxAlignment.END);
    },

    _createPlaybackBox : function() {
        this._playbackBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.CENTER,
                               visible: false,
                               opacity: 0,
                               style: this._style,
                               name: "sound-thing-playback-box" });

        this._createPlayButton();
        this._createProgressBar();

        this._controlsBox.append(this._playbackBox,
                                 TheBoard.BoxPackFlags.NONE);
    },

    _createPlayButton : function() {
        this._playButton =
            new Mx.Button({ isToggle: true,
                            style: this._style,
                            name: "sound-thing-play-button" });

        this._playButton.connect("notify::toggled",
                                 Lang.bind(this,
                                           this._onPlayButtonToggled));

        this._playbackBox.append(this._playButton,
                                 TheBoard.BoxPackFlags.NONE);
    },

    _createProgressBar : function() {
        this._progressBar =
            new Mx.ProgressBar({ reactive: true,
                                 height: 12,
                                 style: this._style,
                                 name: "sound-thing-progress-bar" });

        this._updateProgressBar();

        this._progressBar.connect("button-press-event",
                                  Lang.bind(this,
                                            this._onProgressBarButtonPressEvent));

        this._playbackBox.append(this._progressBar,
                                 TheBoard.BoxPackFlags.EXPAND);

        this._playbackBox.set_child_align(this._progressBar,
                                          TheBoard.BoxAlignment.FILL,
                                          TheBoard.BoxAlignment.CENTER);
    },

    _createTimeLabel : function() {
        this._timeLabel =
            new Mx.Label({ yAlign: Mx.Align.MIDDLE,
                           anchorY: -2,
                           visible: false,
                           opacity: 0,
                           style: this._style,
                           name: "sound-thing-time-label" });

        this._updateTimeLabel(this._player.duration);

        this._controlsBox.append(this._timeLabel,
                                 TheBoard.BoxPackFlags.NONE);

        this._controlsBox.set_child_align(this._timeLabel,
                                          TheBoard.BoxAlignment.CENTER,
                                          TheBoard.BoxAlignment.END);
    },

    _createCaptionText : function() {
        this._captionBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: _CAPTION_BOX_NAME_NORMAL });

        this._captionLabel =
            new Mx.Label({ xAlign: Mx.Align.MIDDLE,
                           style: this._style,
                           name: "sound-thing-caption-label" });

        this._captionLabel.clutterText.maxLength = 25;
        this._captionLabel.clutterText.lineAlignment = Pango.Alignment.CENTER;

        this._captionLabel.connect("key-press-event",
                                   Lang.bind(this, this._onCaptionTextKeyPressEvent));

        this._captionLabel.clutterText.connect("text-changed",
                                               Lang.bind(this,
                                                         this._onCaptionTextChanged));

        this._captionBox.append(this._captionLabel,
                                TheBoard.BoxPackFlags.EXPAND);

        this._contentBox.append(this._captionBox,
                                TheBoard.BoxPackFlags.NONE);
    },

    _createSpools : function() {
        this._spoolsBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               y: _SPOOLS_Y,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               style: this._style,
                               name: "sound-thing-spools-box" });

        for (let i = 0; i < 2; i++) {
            let spool = new Mx.Spinner({ width: 27,
                                        height: 27,
                                        animating: false,
                                        style: this._style,
                                        name: "sound-thing-spool" });

            this._spoolsBox.append(spool,
                                   TheBoard.BoxPackFlags.NONE);
        }

        this._contentBox.append(this._spoolsBox,
                                TheBoard.BoxPackFlags.FIXED);

        this._contentBox.set_fixed_child_align(this._spoolsBox,
                                               TheBoard.BoxAlignment.CENTER,
                                               TheBoard.BoxAlignment.FIXED);
    },

    _updateSpinner : function() {
        // FIXME: show/hide spinner depending on the
        // loading state of the sound
    },

    _updateURI : function(uri, fromState) {
        if (this._uri == uri) {
            return;
        }

        this._maybeDeleteVoiceFile();

        this._uri = uri;

        if (this._uri) {
            this._updateSpinner();

            // start loading the new sound file
            this._player.uri = this._uri;

            //this._player.playing = false;

            if (!fromState) {
                this._playButton.toggled = true;
            }
        }

        this._updateSoundControlsVisibility();

        if (!fromState) {
            this.emit('save');
        }
    },

    _showSoundControlActors : function(actors) {
        Tweener.addTween(actors,
                         { opacity: 255,
                           time: _SHOW_BUTTON_BOX_TIME,
                           onStart: function() {
                               this.show();
                           }});
    },

    _hideSoundControlActors : function(actors) {
        Tweener.addTween(actors,
                         { opacity: 0,
                           time: _SHOW_BUTTON_BOX_TIME,
                           onComplete: function() {
                               this.hide();
                           }});
    },

    _updateSoundWithFileChooser : function() {
        let chooser = new Gtk.FileChooserDialog();

        chooser.add_button(Gtk.STOCK_CANCEL,
                           Gtk.ResponseType.REJECT);
        chooser.add_button(Gtk.STOCK_OK,
                           Gtk.ResponseType.ACCEPT);

        let soundFilter = new Gtk.FileFilter();

        // FIXME: add filter only allow selecting sound files

        let soundsDir = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC);
        chooser.set_current_folder(soundsDir);

        chooser.set_transient_for(this.context.gtkWindow);

        let response = chooser.run();

        if (response != Gtk.ResponseType.ACCEPT) {
            chooser.destroy();
            return;
        }

        let uri = chooser.get_uri();

        // Destroy dialog first, then set sound
        chooser.destroy();

        // we have to restore key focus on stage
        // because the chooser grabs key focus
        this._captionLabel.clutterText.grab_key_focus();

        this._updateURI(uri,
                        false /* not from state*/);
    },

    _ensureTargetVoiceDir : function() {
        try {
            let targetDir = GIO.file_new_for_path(this._targetVoicePath);

            // FIXME: we should not be creating this directory
            // synchronously
            targetDir.make_directory_with_parents(null);
        } catch(e) {
        }
    },

    _generateVoiceFileURI : function() {
        this._ensureTargetVoiceDir();

        let now = new Date();

        let filename = "VoiceMessage-" +
                       now.getFullYear() + "-" +
                       (now.getMonth() + 1) + "-" +
                       now.getDate() + "-" +
                       now.getHours() +
                       now.getMinutes() +
                       now.getSeconds() +
                       now.getMilliseconds() +
                       ".ogg";

        let file = GIO.file_new_for_path(this._targetVoicePath + filename);

        return file.get_uri();
    },

    _startVoiceRecording : function() {
        if (this._recorder) {
            return;
        }

        let uri = this._generateVoiceFileURI();

        if (!uri) {
            return;
        }

        this._recorder =
            new TheBoard.SoundRecorder({ uri: uri });

        this._recorder.connect("notify::duration",
                               Lang.bind(this,
                                         this._onRecorderDurationChanged));

        this._stopButton =
            new Mx.Button({ label: Gettext.gettext("Stop"),
                            y: _STOP_BUTTON_Y,
                            style: this._style,
                            name: "sound-thing-stop-button" });

        this._stopButton.connect("clicked",
                                 Lang.bind(this,
                                           this._onStopButtonClicked));

        this._contentBox.append(this._stopButton,
                                TheBoard.BoxPackFlags.FIXED);

        this._contentBox.set_fixed_child_align(this._stopButton,
                                               TheBoard.BoxAlignment.CENTER,
                                               TheBoard.BoxAlignment.FIXED);

        this._recorder.start();

        this._setSpoolsAnimating(true);

        this._captionBox.name = _CAPTION_BOX_NAME_RECORDING;

        this._stopEditingCaption();
        this._textBeforeRecording = this._captionLabel.text;
        this._captionLabel.text = Gettext.gettext("Recording");

        this._updateSoundControlsVisibility();
    },

    _stopVoiceRecording : function() {
        if (!this._recorder) {
            return;
        }

        this._setSpoolsAnimating(false);

        let uri = this._recorder.uri;

        this._recorder.stop();
        delete this._recorder;

        this._stopButton.destroy();
        delete this._stopButton;

        this._captionBox.name = _CAPTION_BOX_NAME_NORMAL;

        this._captionLabel.text = this._textBeforeRecording;
        delete this._textBeforeRecording;

        this._updateURI(uri,
                        true /* avoid autoplaying */);

        this.emit("save");

        this._updateSoundControlsVisibility();
    },

    _startEditingCaption : function() {
        this._captionLabel.clutterText.editable = true;
        this._captionLabel.clutterText.grab_key_focus();
    },

    _stopEditingCaption : function() {
        this._captionLabel.clutterText.editable = false;
    },

    _maybeDeleteVoiceFile : function() {
        if (!this._uri) {
            return;
        }

        let targetDir = GIO.file_new_for_path(this._targetVoicePath);

        let soundFile = GIO.file_new_for_path(this._uri);
        let parentDir = soundFile.get_parent();

        if (targetDir.equal(parentDir)) {
            TheBoard.g_file_delete_async(soundFile, null,
                                         function() {},
                                         null);
        }
    },

    _updateForSoundLoaded : function() {
        this._updateSpinner();
    },

    _updateProgressBar : function() {
        this._progressBar.progress = this._player.progress;
    },

    _formatTimeComponent : function(n) {
        // FIXME: we need a sprinf equivalent to do
        // proper formatting here.
        return (n >= 10 ? n : "0" + n);
    },

    _updateTimeLabel : function(currentTime) {
        let hours = Math.floor(currentTime / 3600);
        currentTime -= hours * 3600;

        let minutes = Math.floor(currentTime / 60);
        currentTime -= minutes * 60;

        let seconds = Math.floor(currentTime);

        this._timeLabel.text = this._formatTimeComponent(hours) + ":" +
                               this._formatTimeComponent(minutes) + ":" +
                               this._formatTimeComponent(seconds);
    },

    _updateSoundControlsVisibility : function() {
        let actorsToShow = [];
        let actorsToHide = [];

        let showTimeLabel = this._recorder || this._uri;

        if (showTimeLabel) {
            actorsToShow.push(this._timeLabel);
        } else {
            actorsToHide.push(this._timeLabel);
        }

        let showPlaybackBox =
            !this._recorder && this._uri &&
            (this.hover || this.active);

        if (showPlaybackBox) {
            actorsToShow.push(this._playbackBox);
        } else {
            actorsToHide.push(this._playbackBox);
        }

        this._showSoundControlActors(actorsToShow);
        this._hideSoundControlActors(actorsToHide);
    },

    _setSpoolsAnimating : function(animating) {
        let spools = this._spoolsBox.get_children();

        for (let i = 0; i < spools.length; i++) {
            let spool = spools[i];
            spool.animating = animating;
        }
    },

    _onCaptionTextKeyPressEvent : function(o, event) {
        let key = event.get_key_symbol();

        switch (key) {
        case Clutter.Return:
            this._updateSoundWithFileChooser();
            return true;
        case Clutter.Escape:
            this.emit("deactivate");
            return true;
        }

        return false;
    },

    _onCaptionTextChanged : function() {
        if (this._recorder) {
            return;
        }

        this.emit('save');
    },

    _onPlayButtonToggled : function() {
        this._player.playing = this._playButton.toggled;
        this._setSpoolsAnimating(this._playButton.toggled);
        this._updateSoundControlsVisibility();
    },

    _onRecorderDurationChanged : function() {
        this._updateTimeLabel(this._recorder.duration);
    },

    _onStopButtonClicked : function() {
        this._stopVoiceRecording();
        this._startEditingCaption();
    },

    _onProgressBarButtonPressEvent : function(progressBar, event) {
        let [eventX, eventY] = event.get_coords();

        let [transformedX, transformedY] =
            this._progressBar.get_transformed_position();

        let progress = (eventX - transformedX) / this._progressBar.width;
        this._player.progress = progress;
    },

    _onPlayerProgressChanged : function() {
        if (this._recorder) {
            return;
        }

        let currentTime =
            Math.floor(this._player.duration * this._player.progress);

        this._updateProgressBar();

        if (this._player.progress != 0 &&
            this._player.duration != 0) {
            this._updateTimeLabel(currentTime);
        }
    },

    _onPlayerDurationChanged : function() {
        if (this._recorder || this._player.playing) {
            return;
        }

        this._updateTimeLabel(this._player.duration);
    },

    _onPlayerStateChanged : function() {
        switch(this._player.state) {
        case TheBoard.SoundPlayerState.IDLE:
            this._updateForSoundLoaded();
            this._updateTimeLabel(this._player.duration);
            break;

        case TheBoard.SoundPlayerState.DONE:
            this._updateTimeLabel(this._player.duration);
            this._playButton.toggled = false;
            break;

        case TheBoard.SoundPlayerState.ERROR:
            // FIXME: show error message in the UI
            break;

        default:
            // do nothing
        }
    },

    enter : function() {
        this._updateSoundControlsVisibility();
    },

    leave : function() {
        this._updateSoundControlsVisibility();
    },

    activate : function() {
        this._startEditingCaption();
        this._updateSoundControlsVisibility();
    },

    deactivate : function() {
        this._stopVoiceRecording();
        this._stopEditingCaption();
        this._updateSoundControlsVisibility();
    },

    loadState : function(state) {
        if ('uri' in state) {
            let fromState = 'width' in state &&
                            'height' in state;

            this._updateURI(state.uri, fromState);
        }

        if ('text' in state) {
            this._captionLabel.text = state.text;
        }
    },

    getState : function() {
        return { uri: this._uri,
                 text: this._captionLabel.text };
    },

    doAction : function(actionName, actionArgs) {
        if (actionName == "chooseFile") {
            this._updateSoundWithFileChooser();
        } else if (actionName == "recordVoice") {
            this._startVoiceRecording();
        }
    },

    validateSize : function(width, height) {
        return [width, height];
    },

    destroy : function() {
        this._maybeDeleteVoiceFile();
        this._stopVoiceRecording();

        if (this._soundBox) {
            this._soundBox.destroy();
            delete this._soundBox;
        }
    },

    get initialWidth() {
        return _INITIAL_WIDTH;
    },

    get initialHeight() {
        return _INITIAL_HEIGHT;
    },

    get minWidth() {
        return _INITIAL_WIDTH;
    },

    get minHeight() {
        return _INITIAL_HEIGHT;
    },

    get contentActor() {
        return this._soundBox;
    }
}

function create(args) {
    return new SoundThing(args);
}

function createToolbar(args) {
    let toolbar =
        new Toolbar.Toolbar({ title: NAME,
                              visible: false });

    let toolBox =
        new ToolBox.ToolBox({ title: Gettext.gettext("Load from") });

    toolBox.addButton({ label: Gettext.gettext("File"),
                        actionName: "chooseFile" });

    toolBox.addButton({ label: Gettext.gettext("Mic"),
                        actionName: "recordVoice" });

    toolbar.addToolBox(toolBox);

    return toolbar;
}
