// standard imports
const Gettext = imports.gettext.domain("the-board");

// ui imports
const Page = imports.ui.page;
const ToolBox = imports.ui.toolBox;
const Toolbar = imports.ui.toolbar;

function SelectionToolbar(args) {
    this._init(args);
}

SelectionToolbar.prototype = {
    __proto__: Toolbar.Toolbar.prototype,

    _init : function(args) {
        Toolbar.Toolbar.prototype._init.apply(this, [args]);

        this._createToolBoxAlign();
        this._createToolBoxDistribute();
        this._createToolBoxRemove();
    },

    _createToolBoxAlign : function() {
        this._toolBoxAlign =
            new ToolBox.ToolBox({ title: Gettext.gettext("Align") });

        this._toolBoxAlign.addButton({ label: Gettext.gettext("Left"),
                                       actionName: "align",
                                       actionArgs: { alignment: Page.Alignment.LEFT } });

        this._toolBoxAlign.addButton({ label: Gettext.gettext("Right"),
                                       actionName: "align",
                                       actionArgs: { alignment: Page.Alignment.RIGHT } });

        this._toolBoxAlign.addButton({ label: Gettext.gettext("Top"),
                                       actionName: "align",
                                       actionArgs: { alignment: Page.Alignment.TOP } });

        this._toolBoxAlign.addButton({ label: Gettext.gettext("Bottom"),
                                       actionName: "align",
                                       actionArgs: { alignment: Page.Alignment.BOTTOM } });

        this.addToolBox(this._toolBoxAlign);
    },

    _createToolBoxDistribute : function() {
        this._toolBoxDistribute =
            new ToolBox.ToolBox({ title: Gettext.gettext("Distribute") });

        let verticalArgs =
            { label: Gettext.gettext("Vertical"),
              actionName: "distribute",
              actionArgs: { orientation: Page.Orientation.VERTICAL } };

        this._toolBoxDistribute.addButton(verticalArgs);

        let horizontalArgs =
            { label: Gettext.gettext("Horizontal"),
              actionName: "distribute",
              actionArgs: { orientation: Page.Orientation.HORIZONTAL } };

        this._toolBoxDistribute.addButton(horizontalArgs);

        this.addToolBox(this._toolBoxDistribute);
    },

    _createToolBoxRemove : function() {
        this._toolBoxRemove = new ToolBox.ToolBox();

        this._toolBoxRemove.addButton({ label: Gettext.gettext("Remove"),
                                        actionName: "remove" });

        this.addToolBox(this._toolBoxRemove);
    }
}
