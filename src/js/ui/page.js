// standard imports
const Lang = imports.lang;
const Signals = imports.signals;
const Tweener = imports.tweener.tweener;

// gi imports
const Clutter = imports.gi.Clutter;
const TheBoard = imports.gi.TheBoard;

// ui imports
const Background = imports.ui.background;
const Thing = imports.ui.thing;

// util imports
const MathUtil = imports.util.mathUtil;

// model imports
const PageModel = imports.model.pageModel;

const _DRAGGING_THRESHOLD = 2;

const _LAYER_BACKGROUND   = 0.1;
const _LAYER_THING        = 0.2;
const _LAYER_DIMMING      = 0.3;
const _LAYER_SELECTION    = 0.4;

const _ADD_THING_TIME = 0.5;
const _ADD_THING_TRANSITION = 'easeOutCubic';

const _MOVE_THING_TIME = 0.5;
const _MOVE_THING_TRANSITION = 'easeOutCubic';

const _SET_BACKGROUND_TIME = 0.3;
const _SET_BACKGROUND_TRANSITION = 'easeOutCubic';

const _NEW_THING_TOP_MARGIN = 60;
const _NEW_THING_LEFT_MARGIN = 60;
const _NEW_THING_H_SPACING = 30;
const _NEW_THING_V_SPACING = 20;
const _NEW_THING_COLS_INC = 30;
const _NEW_THING_ROWS_INC = 10;

const _N_COLS_SLIDE_IN = 3;
const _N_ROWS_SLIDE_IN = 3;

const _DISTRIBUTE_VERTICAL_SPACING = 10;
const _DISTRIBUTE_HORIZONTAL_SPACING = 20;

let Alignment = {
    LEFT   : 0,
    RIGHT  : 1,
    TOP    : 2,
    BOTTOM : 3
};

let Orientation = {
    VERTICAL   : 0,
    HORIZONTAL : 1
};

function Page(args) {
    this._init(args);
}

Page.prototype = {
    _init : function(args) {
        args = args || {};

        if ('model' in args) {
            this._model = args.model;
        } else {
            throw new Error("Page model is required");
        }

        if ('context' in args) {
            this._context = args.context;
        } else {
            throw new Error("Page context is required");
        }

        this._loaded = false;
        this._thingsLoaded = false;
        this._things = [];
        this._selectedThings = [];
        this._activeThing = null;

        this._cancelSelectionOnClick = true;

        this._createMainBox();
        this._createDimBox();
        this._createSelectionBox();
        this._connectModelSignals();

        this._updateFromModel();

        if (!this._model.isNew()) {
            this._model.load();
        }
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               reactive: true,
                               name: "page-main-box" });

        let clickAction = new Clutter.ClickAction();

        clickAction.connect("clicked",
                            Lang.bind(this, this._onMainBoxClicked));

        this._mainBox.add_action(clickAction);

        let dragAction = new Clutter.DragAction();

        dragAction.set_drag_threshold(_DRAGGING_THRESHOLD,
                                      _DRAGGING_THRESHOLD);

        dragAction.connect("drag-motion",
                           Lang.bind(this, this._onMainBoxDragMotion));
        dragAction.connect("drag-begin",
                           Lang.bind(this, this._onMainBoxDragBegin));
        dragAction.connect("drag-end",
                           Lang.bind(this, this._onMainBoxDragEnd));

        this._mainBox.add_action(dragAction);
    },

    _createDimBox : function() {
        this._dimBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               depth: _LAYER_DIMMING,
                               opacity: 0,
                               name: "page-dim-box" });

        this._mainBox.append(this._dimBox,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._dimBox,
                                            TheBoard.BoxAlignment.FILL,
                                            TheBoard.BoxAlignment.FILL);
    },

    _createSelectionBox : function() {
        this._selectionBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               depth: _LAYER_SELECTION,
                               visible: false,
                               name: "page-selection-box" });

        this._mainBox.append(this._selectionBox,
                             TheBoard.BoxPackFlags.FIXED);
    },

    _connectModelSignals : function() {
        this._modelStateChangedId =
            this._model.connect("state-changed",
                                Lang.bind(this, this._onModelStateChanged));
    },

    _updateFromModel : function() {
        this._updateLoaded();

        if (this._model.state == PageModel.State.LOADED) {
            this._loadModel();
        }
    },

    _updateLoaded : function() {
        let loaded = this._thingsLoaded &&
                     this._model.state == PageModel.State.LOADED;

        if (this._loaded == loaded) {
            return;
        }

        this._loaded = loaded;

        this.emit("loaded-changed");
    },

    _getThingsInArea : function(x1, y1, x2, y2) {
        let thingsInArea = [];

        for (let i = 0; i < this._things.length; ++i) {
            let thing = this._things[i];

            if (x1 > thing.actor.x + thing.actor.width ||
                x2 < thing.actor.x ||
                y1 > thing.actor.y + thing.actor.height ||
                y2 < thing.actor.y) {
                continue;
            }

            thingsInArea.push(thing);
        }

        return thingsInArea;
    },

    _findPositionForNewThing : function(thing) {
        let [windowWidth, windowHeight] = this._context.gtkWindow.get_size();

        let totalWidth = windowWidth +_NEW_THING_LEFT_MARGIN;
        let totalHeight = windowHeight + _NEW_THING_TOP_MARGIN;

        let nCols = Math.floor(totalWidth / _NEW_THING_COLS_INC);

        let nRows = Math.floor(totalHeight / _NEW_THING_ROWS_INC);

        for (let i = 0; i < nRows; ++i) {
            for (let j = 0; j < nCols; ++j) {
                let x1 = (j * _NEW_THING_COLS_INC) +
                         (j * _NEW_THING_H_SPACING) +
                         _NEW_THING_LEFT_MARGIN;

                let y1 = (i * _NEW_THING_ROWS_INC) +
                         (i * _NEW_THING_V_SPACING) +
                         _NEW_THING_TOP_MARGIN;

                let x2 = x1 + thing.initialWidth;
                let y2 = y1 + thing.initialHeight;

                if (x2 > windowWidth || y2 > windowHeight) {
                    continue;
                }

                if (this._getThingsInArea(x1, y1, x2, y2).length == 0) {
                    return [x1, y1];
                }
            }
        }

        return [(windowWidth - thing.initialWidth) / 2,
                (windowHeight - thing.initialHeight) / 2];
    },

    _loadThingsFromModel : function() {
        let things = this._model.things;

        for (let i = 0; i < things.length; ++i) {
            let state = things[i];
            this.addThingFromState(state, false /* add to model */);
        }
    },

    _loadBackgroundFromModel : function() {
        // If background is undefined in the model, we'll
        // just use the default one (see createBackgroundFromId)
        let backgroundId = this._model.background;
        this.setBackground(backgroundId);
    },

    _loadModel : function() {
        this._loadBackgroundFromModel();
        this._loadThingsFromModel();

        // FIXME: This should be done in a more involved way
        // by checking the loaded state of background and the
        // things in this page
        this._thingsLoaded = true;

        this._updateLoaded();
    },

    _addThing : function(thing, state) {
        thing.actor.depth = _LAYER_THING;

        let x;
        let y;

        if (state && 'x' in state && 'y' in state) {
            x = state.x;
            y = state.y;
        } else {
            [x, y] = this._findPositionForNewThing(thing);
        }

        thing.actor.x = x;
        thing.actor.y = y;

        thing._Page_activateId =
            thing.connect("activate",
                          Lang.bind(this, this._onThingActivate));

        thing._Page_deactivateId =
            thing.connect("deactivate",
                          Lang.bind(this, this._onThingDeactivate));

        thing._Page_dragBeginId =
            thing.connect("drag-begin",
                          Lang.bind(this, this._onThingDragBegin));

        thing._Page_dragMotionId =
            thing.connect("drag-motion",
                          Lang.bind(this, this._onThingDragMotion));

        thing._Page_dragEndId =
            thing.connect("drag-end",
                          Lang.bind(this, this._onThingDragEnd));

        thing._Page_removeId =
            thing.connect("remove",
                          Lang.bind(this, this._onThingRemove));

        thing._Page_saveId =
            thing.connect("save",
                          Lang.bind(this, this._onThingSave));

        thing._Page_selectedChangedId =
            thing.connect("selected-changed",
                          Lang.bind(this, this._onThingSelectedChanged));

        this._mainBox.append(thing.actor,
                             TheBoard.BoxPackFlags.FIXED);

        this._things.push(thing);
    },

    _getAnimationAnchorForNewThing : function(thing) {
        let [windowWidth, windowHeight] = this._context.gtkWindow.get_size();

        let col = Math.floor(thing.actor.x / (windowWidth / _N_COLS_SLIDE_IN));
        let row = Math.floor(thing.actor.y / (windowHeight / _N_ROWS_SLIDE_IN));

        switch (col) {
        case 0:
        case 2:
            // slide in horizontally if coming from sides
            let anchorX = (col == 0 ? 1 : -1) *
                          (thing.actor.x +
                          (col == 0 ? thing.initialWidth : 0));
            return [anchorX, 0];

        case 1:
            // slide in vertically if coming from center
            let anchorY = (row == 0 || row == 2 ? 1 : -1) *
                          (thing.actor.y +
                          (row == 0 || row == 2 ? thing.initialHeight : 0));
            return [0, anchorY];
        }

        return [0, 0];
    },

    _destroyAllThings : function() {
        while (this._things.length > 0) {
            let thing = this._things[0];
            this.removeThing(thing);
        }
    },

    _animateNewThing : function(thing) {
        let [anchorX, anchorY] =
            this._getAnimationAnchorForNewThing(thing);

        thing.actor.anchorX = anchorX;
        thing.actor.anchorY = anchorY;

        Tweener.addTween(thing.actor,
                         { anchorX: 0,
                           anchorY: 0,
                           time: _ADD_THING_TIME,
                           transition: _ADD_THING_TRANSITION });
    },

    _showDimBox : function() {
        Tweener.addTween(this._dimBox,
                         { opacity: 255,
                           time: _ADD_THING_TIME,
                           transition: _ADD_THING_TRANSITION });
    },

    _hideDimBox : function() {
        Tweener.addTween(this._dimBox,
                         { opacity: 0,
                           time: _ADD_THING_TIME,
                           transition: _ADD_THING_TRANSITION });
    },

    _areaUnion : function(area1, area2) {
        let union = { x1: 0,
                      y1: 0,
                      x2: 0,
                      y2: 0 };

        if (area1 && area2) {
            union.x1 = Math.min(area1.x1, area2.x1);
            union.y1 = Math.min(area1.y1, area2.y1);
            union.x2 = Math.max(area1.x2, area2.x2);
            union.y2 = Math.max(area1.y2, area2.y2);
        } else if (area1) {
            union = area1;
        } else if (area2) {
            union = area2;
        }

        return union;
    },

    _updateDragArea : function() {
        delete this._dragArea;

        let updateArea = function(thing) {
            this._dragArea = this._areaUnion(this._dragArea,
                                             thing.area);
        };

        this._selectedThings.forEach(Lang.bind(this, updateArea));
    },

    _updateSelectedThingsFromBox : function() {
        let selectedThings;

        if (this._selectionBox.visible) {
            let x1 = this._selectionBox.x - this._selectionBox.anchorX;
            let y1 = this._selectionBox.y - this._selectionBox.anchorY;
            let x2 = x1 + this._selectionBox.width;
            let y2 = y1 + this._selectionBox.height;

            selectedThings = this._getThingsInArea(x1, y1, x2, y2);
        } else {
            selectedThings = [];
        }

        let updateSelection = function(thing) {
            thing.selected = selectedThings.indexOf(thing) >= 0;
        };

        this._things.forEach(updateSelection);
    },

    _ensureDragArea : function(draggedThing) {
        this._updateDragArea();

        if (!this._dragArea) {
            this._dragArea = draggedThing.area;
        }
    },

    _animateMoveThing : function(thing, x, y) {
        let [deltaX, deltaY] =
            this._clampMoveDelta(thing.area,
                                 x - thing.area.x1,
                                 y - thing.area.y1);

        thing.actor.anchorX = deltaX;
        thing.actor.anchorY = deltaY;

        thing.setPosition(thing.area.x1 + deltaX,
                          thing.area.y1 + deltaY);

        thing.emit("save");

        Tweener.addTween(thing.actor,
                         { anchorX: 0,
                           anchorY: 0,
                           time: _MOVE_THING_TIME,
                           transition: _MOVE_THING_TRANSITION });
    },

    _alignSelectedThings : function(alignment) {
        let alignmentCoord = -1;

        let updateAlignmentCoord = function(thing) {
            let area = thing.area;

            switch(alignment) {
            case Alignment.LEFT:
                if (alignmentCoord < 0 ||
                    alignmentCoord > area.x1) {
                    alignmentCoord = area.x1;
                }
                break;

            case Alignment.RIGHT:
                if (alignmentCoord < 0 ||
                    alignmentCoord < area.x2) {
                    alignmentCoord = area.x2;
                }
                break;

            case Alignment.TOP:
                if (alignmentCoord < 0 ||
                    alignmentCoord > area.y1) {
                    alignmentCoord = area.y1;
                }
                break;

            case Alignment.BOTTOM:
                if (alignmentCoord < 0 ||
                    alignmentCoord < area.y2) {
                    alignmentCoord = area.y2;
                }
                break;
            }
        };

        this._selectedThings.forEach(updateAlignmentCoord);

        let moveThing = function(thing) {
            let area = thing.area;

            let x, y;

            switch(alignment) {
            case Alignment.LEFT:
                x = alignmentCoord;
                y = area.y1;
                break;

            case Alignment.RIGHT:
                x = alignmentCoord - (area.x2 - area.x1);
                y = area.y1;
                break;

            case Alignment.TOP:
                x = area.x1;
                y = alignmentCoord;
                break;

            case Alignment.BOTTOM:
                x = area.x1;
                y = alignmentCoord - (area.y2 - area.y1);
                break;
            }

            this._animateMoveThing(thing, x, y);
        };

        this._selectedThings.forEach(Lang.bind(this, moveThing));

        this._updateDragArea();
    },

    _distributeSelectedThings : function(orientation) {
        let sortByCoord = function(thingA, thingB) {
            switch(orientation) {
            case Orientation.VERTICAL:
                return thingA.area.y1 - thingB.area.y1;
                break;

            case Orientation.HORIZONTAL:
                return thingA.area.x1 - thingB.area.x1;
                break;
            }

            return 0;
        };

        this._selectedThings.sort(sortByCoord);

        let pageWidth = this._mainBox.allocation.x2 -
                        this._mainBox.allocation.x1;

        let pageHeight = this._mainBox.allocation.y2 -
                         this._mainBox.allocation.y1;

        let centerCoord = -1;
        let currentCoord = -1;
        let totalSize = 0;

        let updateCoords = function(thing) {
            switch(orientation) {
            case Orientation.VERTICAL:
                totalSize += thing.area.y2 - thing.area.y1 +
                             _DISTRIBUTE_VERTICAL_SPACING;
                break;

            case Orientation.HORIZONTAL:
                totalSize += thing.area.x2 - thing.area.x1 +
                             _DISTRIBUTE_HORIZONTAL_SPACING;
                break;
            }
        };

        this._selectedThings.forEach(updateCoords);

        let moveThing = function(thing) {
            let area = thing.area;

            let x, y, thingCenter;

            switch(orientation) {
            case Orientation.VERTICAL:
                if (currentCoord < 0) {
                    if (totalSize > pageHeight - area.y1) {
                        currentCoord = pageHeight - totalSize;
                    } else {
                        currentCoord = area.y1;
                    }
                }

                let width = area.x2 - area.x1;
                thingCenter = area.x1 + (width / 2);

                if (centerCoord < 0) {
                    centerCoord = thingCenter;
                }

                x = area.x1 + (centerCoord - thingCenter);
                y = currentCoord;

                currentCoord += area.y2 - area.y1 +
                                _DISTRIBUTE_VERTICAL_SPACING;

                break;

            case Orientation.HORIZONTAL:
                if (currentCoord < 0) {
                    if (totalSize > pageWidth - area.x1) {
                        currentCoord = pageWidth - totalSize;
                    } else {
                        currentCoord = area.x1;
                    }
                }

                let height = area.y2 - area.y1;
                thingCenter = area.y1 + (height / 2);

                if (centerCoord < 0) {
                    centerCoord = thingCenter;
                }

                x = currentCoord;
                y = area.y1 + (centerCoord - thingCenter);

                currentCoord += area.x2 - area.x1 +
                                _DISTRIBUTE_HORIZONTAL_SPACING;

                break;
            }

            this._animateMoveThing(thing, x, y);
        };

        this._selectedThings.forEach(Lang.bind(this, moveThing));

        this._updateDragArea();
    },

    _removeSelectedThings : function() {
        // Store current list of selected things
        let selectedThings = this._selectedThings;

        // Clean up the list of selected things
        // as they are not removed
        this._selectedThings = [];

        // Actually remove them from page
        selectedThings.forEach(Lang.bind(this, this.removeThing));
    },

    _clampMoveDelta : function(area, deltaX, deltaY) {
        let minDeltaX = -area.x1;
        let maxDeltaX = this._mainBox.allocation.x2 -
                        this._mainBox.allocation.x1 -
                        area.x2;

        let minDeltaY = -area.y1;
        let maxDeltaY = this._mainBox.allocation.y2 -
                        this._mainBox.allocation.y1 -
                        area.y2;

        deltaX = MathUtil.clamp(deltaX, minDeltaX, maxDeltaX);
        deltaY = MathUtil.clamp(deltaY, minDeltaY, maxDeltaY);

        return [deltaX, deltaY];
    },

    _onMainBoxClicked : function(o, event) {
        if (this._cancelSelectionOnClick) {
            this.unselectAllThings();
        }

        this._cancelSelectionOnClick = true;

        this.setActiveThing(null);
        this.emit("clicked");
    },

    _onMainBoxDragMotion : function(action, actor, deltaX, deltaY) {
        if (this._activeThing) {
            return;
        }

        if (deltaX >= 0 && deltaY >= 0) {
            this._selectionBox.set_anchor_point_from_gravity(Clutter.Gravity.NORTH_WEST);
        } else if (deltaX >= 0) {
            this._selectionBox.set_anchor_point_from_gravity(Clutter.Gravity.SOUTH_WEST);
        } else if (deltaY >= 0) {
            this._selectionBox.set_anchor_point_from_gravity(Clutter.Gravity.NORTH_EAST);
        } else {
            this._selectionBox.set_anchor_point_from_gravity(Clutter.Gravity.SOUTH_EAST);
        }

        this._selectionBox.width = Math.abs(deltaX);
        this._selectionBox.height = Math.abs(deltaY);

        this._updateSelectedThingsFromBox();
    },

    _onMainBoxDragBegin : function(action, actor, eventX, eventY, modifiers) {
        if (this._activeThing) {
            return;
        }

        this._cancelSelectionOnClick = false;

        action.dragHandle = actor;

        this._selectionBox.show();

        this._selectionBox.x = eventX;
        this._selectionBox.y = eventY;
        this._selectionBox.width = 0;
        this._selectionBox.height = 0;
    },

    _onMainBoxDragEnd : function(action, actor, eventX, eventY, modifiers) {
        this._selectionBox.hide();
    },

    _onModelStateChanged : function() {
        this._updateFromModel();
    },

    _onThingActivate : function(thing) {
        this.setActiveThing(thing);
    },

    _onThingDeactivate : function(thing) {
        this.setActiveThing(null);
    },

    _onThingDragBegin : function(thing) {
        thing.actor.raise(null);

        if (!thing.selected) {
            this.unselectAllThings();
        }

        this._ensureDragArea(thing);

        if (this._activeThing && this._activeThing != thing) {
            this.setActiveThing(null);
        }
    },

    _onThingDragMotion : function(thing, deltaX, deltaY) {
        [deltaX, deltaY] =
            this._clampMoveDelta(this._dragArea, deltaX, deltaY);

        this._dragArea.x1 += deltaX;
        this._dragArea.x2 += deltaX;
        this._dragArea.y1 += deltaY;
        this._dragArea.y2 += deltaY;

        let moveThing = function(thingToMove) {
            let newX = thingToMove.area.x1 + deltaX;
            let newY = thingToMove.area.y1 + deltaY;

            thingToMove.setPosition(newX, newY);
        };

        if (!thing.selected) {
            moveThing(thing);
        }

        this._selectedThings.forEach(moveThing);
    },

    _onThingDragEnd : function(thing) {
        let saveThing = function(thingToSave) {
            thingToSave.emit("save");
        };

        this._selectedThings.forEach(saveThing);

        if (this._activeThing !== thing) {
            thing.actor.lower(this._dimBox);
        }
    },

    _onThingRemove : function(thing) {
        this.removeThing(thing);
    },

    _onThingSave : function(thing) {
        let index = this._things.indexOf(thing);
        this._model.updateThing(index, thing.onGetState());
    },

    _onThingSelectedChanged : function(thing) {
        let index = this._selectedThings.indexOf(thing);

        if (thing.selected && index < 0) {
            this._selectedThings.push(thing);
        } else if (!thing.selected && index >= 0) {
            this._selectedThings.splice(index, 1);
        }

        this._updateDragArea();

        this.emit("selected-things-changed");
    },

    setActiveThing : function(activeThing) {
        if (this._activeThing === activeThing) {
            return;
        }

        if (this._activeThing) {
            this._activeThing.onDeactivate();
            this._activeThing.actor.depth = _LAYER_THING;
        }

        this._activeThing = activeThing;

        if (this._activeThing) {
            this._activeThing.onActivate();

            this.unselectAllThings();

            this._dimBox.raise(null);
            this._activeThing.actor.raise(this._dimBox);

            this._showDimBox();
        } else {
            this._hideDimBox();
        }

        this.emit("active-thing-changed");
    },

    addThingFromState : function(state, addToModel) {
        // add to model by default
        if (addToModel === undefined) {
            addToModel = true;
        }

        let thingId = state.id;

        let args = { context: this._context };

        let thing = Thing.createThingFromId(thingId, args);
        thing.onLoadState(state);

        this._addThing(thing, state);

        if (addToModel) {
            this._model.addThing(state);

            // animate only new things if window is visible
            if (this._context.gtkWindow.visible) {
                this.setActiveThing(thing);
                this._animateNewThing(thing);
            }
        }
    },

    removeThing : function(thing) {
        let indexToRemove = this._things.indexOf(thing);
        this._things.splice(indexToRemove, 1);

        // Ensure the removed thing is unselected
        // before being destroyed to avoid any dangling
        // reference to it
        thing.selected = false;

        thing.disconnect(thing._Page_activateId);
        thing.disconnect(thing._Page_deactivateId);
        thing.disconnect(thing._Page_dragBeginId);
        thing.disconnect(thing._Page_dragMotionId);
        thing.disconnect(thing._Page_dragEndId);
        thing.disconnect(thing._Page_removeId);
        thing.disconnect(thing._Page_saveId);
        thing.disconnect(thing._Page_selectedChangedId);

        if (this._activeThing == thing) {
            this.setActiveThing(null);
        }

        thing.onRemove();

        this._model.removeThing(indexToRemove);
    },

    setBackground : function(backgroundId) {
        if (this._background && this._background.id == backgroundId) {
            return;
        }

        let oldBackground = this._background;

        this._background = Background.createBackgroundFromId(backgroundId);

        this._background.actor.depth = _LAYER_BACKGROUND;
        this._background.actor.opacity = 0;

        this._mainBox.append(this._background.actor,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._background.actor,
                                            TheBoard.BoxAlignment.FILL,
                                            TheBoard.BoxAlignment.FILL);

        this._model.setBackground(this._background.id);

        if (oldBackground) {
            this._background.actor.raise(oldBackground.actor);
        }

        Tweener.addTween(this._background.actor,
                         { opacity: 255,
                           time: oldBackground ?_SET_BACKGROUND_TIME : 0,
                           transition: _SET_BACKGROUND_TRANSITION,
                           onComplete: function() {
                               if (oldBackground) {
                                   Tweener.removeTweens(oldBackground.actor);
                                   oldBackground.destroy();
                               }
                           }});
    },

    selectAllThings : function() {
        let selectThing = function(thing) {
            thing.selected = true;
        };

        this._things.forEach(selectThing);
    },

    unselectAllThings : function() {
        let unselectThing = function(thing) {
            thing.selected = false;
        };

        this._things.forEach(unselectThing);
    },

    doAction : function(actionName, actionArgs) {
        if (this._activeThing) {
            this._activeThing.doAction(actionName, actionArgs);
        } else if (this._selectedThings.length > 1) {
            if (actionName == "align") {
                this._alignSelectedThings(actionArgs.alignment);
            } else if (actionName == "distribute") {
                this._distributeSelectedThings(actionArgs.orientation);
            } else if (actionName == "remove") {
                this._removeSelectedThings();
            }
        }
    },

    destroy : function() {
        this._destroyAllThings();

        if (this._modelStateChangedId) {
            this._model.disconnect(this._modelStateChangedId);
            delete this._modelStateChangedId;
        }

        this._model.unload();

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get loaded() {
        return this._loaded;
    },

    get activeThing() {
        return this._activeThing;
    },

    get selectedThings() {
        return this._selectedThings;
    },

    get model() {
        return this._model;
    },

    get actor() {
        return this._mainBox;
    }
}

Signals.addSignalMethods(Page.prototype);
