// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;
const Signals = imports.signals;

// gi imports
const Clutter = imports.gi.Clutter;
const Mx = imports.gi.Mx;
const Pango = imports.gi.Pango;
const TheBoard = imports.gi.TheBoard;

function PageButton(args) {
    this._init(args);
}

PageButton.prototype = {
    _init : function(args) {
        args = args || {};

        if ('newPageButton' in args) {
            this._newPageButton = args.newPageButton;
        } else {
            this._newPageButton = false;
        }

        if (this._newPageButton) {
            this._mainBoxName = "new-page-button-main-box";
        } else {
            this._mainBoxName = "page-button-main-box";
        }

        this._createMainBox();
        this._createTitleLabel();

        if (!this._newPageButton) {
            this._createRemoveButton();
        }

        if ('pageModel' in args) {
            this.pageModel = args.pageModel;
        } else if (this._newPageButton) {
            this._updateTitle();
        } else {
            throw new Error("PageButton pageModel is required");
        }
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.HORIZONTAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               reactive: true,
                               name: this._mainBoxName });

        let clickAction = new Clutter.ClickAction();

        clickAction.connect("clicked",
                            Lang.bind(this, this._onMainBoxClicked));

        this._mainBox.add_action(clickAction);

        if (this._newPageButton) {
            return;
        }

        this._mainBox.connect("enter-event",
                              Lang.bind(this, this._onMainBoxEnter));

        this._mainBox.connect("leave-event",
                              Lang.bind(this, this._onMainBoxLeave));

    },

    _createTitleLabel : function() {
        this._titleLabel = new Mx.Label({ xAlign: Mx.Align.START,
                                          yAlign: Mx.Align.MIDDLE,
                                          anchorY: -2,
                                          name: "page-button-title-label" });

        this._mainBox.append(this._titleLabel,
                             TheBoard.BoxPackFlags.EXPAND);

        this._mainBox.set_child_align(this._titleLabel,
                                      TheBoard.BoxAlignment.START,
                                      TheBoard.BoxAlignment.CENTER);
    },

    _createRemoveButton : function() {
        this._removeButton =
            new Mx.Button({ name: "page-button-remove-button" });

        this._removeButton.connect("clicked",
                                   Lang.bind(this, this._onRemoveButtonClicked));

        this._removeButton.opacity = 0;

        this._mainBox.append(this._removeButton,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._removeButton,
                                            TheBoard.BoxAlignment.END,
                                            TheBoard.BoxAlignment.FILL);
    },

    _updateTitle : function() {
        let title;

        if (this._newPageButton) {
            title = Gettext.gettext("Add Page");
        } else if (this._pageModel) {
            title = this._pageModel.title || Gettext.gettext("Untitled");
        }

        if (title) {
            this._titleLabel.text = title;
        }
    },

    _onMainBoxClicked : function() {
        this.emit('clicked');
    },

    _onMainBoxEnter : function() {
        this._removeButton.opacity = 255;
    },

    _onMainBoxLeave : function() {
        this._removeButton.opacity = 0;
    },

    _onPageModelTitleChanged : function() {
        this._updateTitle();
    },

    _onRemoveButtonClicked : function() {
        this.emit("remove");
    },

    destroy : function() {
        this.pageModel = null;

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    get pageModel() {
        return this._pageModel;
    },

    set pageModel(pageModel) {
        if (this._newPageButton) {
            return;
        }

        if (this._pageModel == pageModel) {
            return;
        }

        if (this._pageModel) {
            this._pageModel.disconnect(this._pageModelTitleChangedId);
            delete this._pageModelTitleChangedId;
        }

        this._pageModel = pageModel;

        if (this._pageModel) {
            this._pageModelTitleChangedId =
                this._pageModel.connect("title-changed",
                                        Lang.bind(this, this._onPageModelTitleChanged));
        }

        this._updateTitle();
    },

    get actor() {
        return this._mainBox;
    }
}

Signals.addSignalMethods(PageButton.prototype);
