// standard imports
const Lang = imports.lang;
const Signals = imports.signals;
const Tweener = imports.tweener.tweener;

// gi imports
const Clutter = imports.gi.Clutter;
const GIO = imports.gi.Gio;
const GObject = imports.gi.GObject;
const Mx = imports.gi.Mx;
const TheBoard = imports.gi.TheBoard;

// util imports
const MathUtil = imports.util.mathUtil;

const _DRAGGING_THRESHOLD = 2;

const _SHOW_CONTROLS_LENGTH = 12;
const _SHOW_CONTROLS_TIME = 0.3;
const _SHOW_CONTROLS_TRANSITION = "easeOutCubic";

const _MAIN_BOX_STYLE_NORMAL = "thing-main-box";
const _MAIN_BOX_STYLE_SELECTED = "thing-main-box-selected";

function Thing(args) {
    this._init(args);
}

Thing.prototype = {
    _init : function(args) {
        args = args || {};

        if ('context' in args) {
            this._context = args.context;
        } else {
            throw new Error("Thing context is required");
        }

        if ('id' in args) {
            this._id = args.id;
        } else {
            throw new Error("Thing id is required");
        }

        let content = null;

        if ('content' in args) {
            content = args.content;
        } else {
            throw new Error("Thing content is required");
        }

        if ('canResize' in args) {
            this._canResize = args.canResize;
        } else {
            this._canResize = true;
        }

        this._link = null;

        this._active = false;
        this._hover = false;
        this._selected = false;

        this._activateOnClick = true;
        this._content = null;

        this._createMainBox();
        this._createControls();

        this._setContent(content);
    },

    _createMainBox : function() {
        this._mainBox =
            new TheBoard.Box({ orientation: TheBoard.BoxOrientation.VERTICAL,
                               xAlign: TheBoard.BoxAlignment.FILL,
                               yAlign: TheBoard.BoxAlignment.FILL,
                               width: this.initialWidth,
                               height: this.initialHeight,
                               reactive: true });

        this._updateSelectedStyle();

        let clickAction = new Clutter.ClickAction();

        clickAction.connect("clicked",
                            Lang.bind(this, this._onMainBoxClicked));

        this._mainBox.add_action(clickAction);

        let dragAction = new Clutter.DragAction();

        dragAction.set_drag_threshold(_DRAGGING_THRESHOLD,
                                      _DRAGGING_THRESHOLD);

        dragAction.connect("drag-motion",
                           Lang.bind(this, this._onMainBoxDragMotion));
        dragAction.connect("drag-begin",
                           Lang.bind(this, this._onMainBoxDragBegin));
        dragAction.connect("drag-end",
                           Lang.bind(this, this._onMainBoxDragEnd));

        this._mainBox.add_action(dragAction);

        this._mainBox.connect("enter-event",
                              Lang.bind(this, this._onMainBoxEnterEvent));

        this._mainBox.connect("leave-event",
                              Lang.bind(this, this._onMainBoxLeaveEvent));
    },

    _createControls : function() {
        this._createRemoveButton();
        this._createLinkButton();

        if (this._canResize) {
            this._createResizeButton();
        }
    },

    _createLinkButton : function() {
        this._linkButton =
            new Mx.Button({ visible: false,
                            opacity: 0,
                            anchorX: _SHOW_CONTROLS_LENGTH,
                            anchorY: _SHOW_CONTROLS_LENGTH,
                            name: "thing-link-button" });

        this._linkButton.connect("clicked",
                                  Lang.bind(this, this._onLinkButtonClicked));

        this._mainBox.append(this._linkButton,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._linkButton,
                                            TheBoard.BoxAlignment.START,
                                            TheBoard.BoxAlignment.START);
    },

    _createRemoveButton : function() {
        this._removeButton =
            new Mx.Button({ visible: false,
                            opacity: 0,
                            anchorX: -_SHOW_CONTROLS_LENGTH,
                            anchorY: _SHOW_CONTROLS_LENGTH,
                            name: "thing-remove-button" });

        this._removeButton.connect("clicked",
                                  Lang.bind(this, this._onRemoveButtonClicked));

        this._mainBox.append(this._removeButton,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._removeButton,
                                            TheBoard.BoxAlignment.END,
                                            TheBoard.BoxAlignment.START);
    },

    _createResizeButton : function() {
        this._resizeButton =
            new Mx.Button({ visible: false,
                            opacity: 0,
                            anchorX: -_SHOW_CONTROLS_LENGTH,
                            anchorY: -_SHOW_CONTROLS_LENGTH,
                            name: "thing-resize-button" });

        let dragAction = new Clutter.DragAction();

        dragAction.set_drag_threshold(_DRAGGING_THRESHOLD,
                                      _DRAGGING_THRESHOLD);

        dragAction.connect("drag-motion",
                           Lang.bind(this, this._onResizeButtonDragMotion));
        dragAction.connect("drag-begin",
                           Lang.bind(this, this._onResizeButtonDragBegin));
        dragAction.connect("drag-end",
                           Lang.bind(this, this._onResizeButtonDragEnd));

        this._resizeButton.add_action(dragAction);

        this._mainBox.append(this._resizeButton,
                             TheBoard.BoxPackFlags.FIXED);

        this._mainBox.set_fixed_child_align(this._resizeButton,
                                            TheBoard.BoxAlignment.END,
                                            TheBoard.BoxAlignment.END);
    },

    _updatePosition : function(newX, newY) {
        this._mainBox.x = newX;
        this._mainBox.y = newY;
    },

    _updateSize : function(newWidth, newHeight, fromState) {
        let validWidth;
        let validHeight;

        if (fromState) {
            validWidth = newWidth;
            validHeight = newHeight;
        } else {
            newWidth = Math.max(this.minWidth, newWidth);
            newHeight = Math.max(this.minHeight, newHeight);

            [validWidth, validHeight] =
                this.validateSize(newWidth, newHeight);
        }

        this._mainBox.width = validWidth;
        this._mainBox.height = validHeight;
    },

    _setContent : function(content) {
        if (this._content) {
            this._mainBox.remove_actor(this._content.contentActor);
        }

        this._content = content;

        if (this._content) {
            this._mainBox.append(this._content.contentActor,
                                 TheBoard.BoxPackFlags.EXPAND);

            this._removeButton.raise(this._content.contentActor);
            this._linkButton.raise(this._content.contentActor);

            if (this._canResize) {
                this._resizeButton.raise(this._content.contentActor);
            }
        }
    },

    _updateControlsVisibility : function() {
        if (!this._selected && (this._hover || this._active)) {
            this._showControls();
        } else {
            this._hideControls();
        }
    },

    _showControls : function() {
        let actorsToShow = [this._removeButton];

        if (this._link) {
            actorsToShow.push(this._linkButton);
        }

        if (this._canResize) {
            actorsToShow.push(this._resizeButton);
        }

        Tweener.addTween(actorsToShow,
                         { opacity: 255,
                           time: _SHOW_CONTROLS_TIME,
                           transition: _SHOW_CONTROLS_TRANSITION,
                           onStart: function() {
                               this.show();
                           }});
    },

    _hideControls : function() {
        let actorsToHide = [this._linkButton,
                            this._removeButton];

        if (this._canResize) {
            actorsToHide.push(this._resizeButton);
        }

        Tweener.addTween(actorsToHide,
                         { opacity: 0,
                           time: _SHOW_CONTROLS_TIME,
                           transition: _SHOW_CONTROLS_TRANSITION,
                           onComplete: function() {
                               this.hide();
                           }});
    },

    _updateSelectedStyle : function() {
        if (this._selected) {
            this._mainBox.name = _MAIN_BOX_STYLE_SELECTED;
        } else {
            this._mainBox.name = _MAIN_BOX_STYLE_NORMAL;
        }
    },

    _onMainBoxClicked : function(action) {
        if (!this._activateOnClick) {
            this._activateOnClick = true;
            return;
        }

        let modifierType = action.get_state();

        if (modifierType & Clutter.ModifierType.CONTROL_MASK) {
            this.selected = !this.selected;
        } else {
            this.emit("activate");
        }

        this._activateOnClick = true;
    },

    _onMainBoxDragMotion : function(action, actor, deltaX, deltaY) {
        TheBoard.signal_stop_emission_by_name(action, "drag-motion");
        this.emit("drag-motion", deltaX, deltaY);
    },

    _onMainBoxDragBegin : function(action, actor, eventX, eventY, modifiers) {
        action.dragHandle = actor;
        this._activateOnClick = false;

        this.emit("drag-begin");
    },

    _onMainBoxDragEnd : function(action, actor, eventX, eventY, modifiers) {
        this.emit("drag-end");
        this.emit("save");
    },

    _onMainBoxEnterEvent : function() {
        this._hover = true;
        this._updateControlsVisibility();

        this.enter();
    },

    _onMainBoxLeaveEvent : function() {
        this._hover = false;
        this._updateControlsVisibility();

        this.leave();
    },

    _onLinkButtonClicked : function() {
        if (!this._link) {
            return;
        }

        try {
            GIO.app_info_launch_default_for_uri(this._link, null);
        } catch (e) {
            // FIXME: Need a way to show UI errors
        }
    },

    _onRemoveButtonClicked : function() {
        this.emit("remove");
    },

    _onResizeButtonDragMotion : function(action, actor, deltaX, deltaY) {
        TheBoard.signal_stop_emission_by_name(action, "drag-motion");

        let newWidth = this._mainBox.width + deltaX;
        let newHeight = this._mainBox.height + deltaY;

        this._updateSize(newWidth, newHeight,
                         false /* not from state */);
    },

    _onResizeButtonDragBegin : function(action, actor, eventX, eventY, modifiers) {
        action.dragHandle = actor;
    },

    _onResizeButtonDragEnd : function(action, actor, eventX, eventY, modifiers) {
        this.emit("save");
    },

    enter : function() {
        // do nothing by default
    },

    leave : function() {
        // do nothing by default
    },

    doAction : function(actionName, actionArgs) {
        // do nothing by default
    },

    onActivate : function() {
        this._active = true;
        this._updateControlsVisibility();

        this.activate();
    },

    onDeactivate : function() {
        this._active = false;
        this._updateControlsVisibility();

        this.deactivate();
    },

    onLoadState : function(state) {
        if ('width' in state && 'height' in state) {
            this._updateSize(state.width, state.height,
                             true /* from state */);
        }

        if ('x' in state && 'y' in state) {
            this._updatePosition(state.x, state.y);
        }

        this._link = state.link || null;

        this.loadState(state);
    },

    onGetState : function() {
        let state = this.getState();

        // do not allow implementation-specific state to
        // override the state of base class
        state.id = this._id;
        state.x = this._mainBox.x;
        state.y = this._mainBox.y;
        state.width = this._mainBox.width;
        state.height = this._mainBox.height;

        if (this._link) {
            state.link = this._link;
        }

        return state;
    },

    onRemove : function() {
        if (this._content) {
            this._content.destroy();
            delete this._content;
        }

        if (this._mainBox) {
            this._mainBox.destroy();
            delete this._mainBox;
        }
    },

    setPosition : function(x, y) {
        this._updatePosition(x, y);
    },

    setSize : function(width, height) {
        this._updateSize(width, height,
                         false /* not from state */);
    },

    get id() {
        return this._id;
    },

    get context() {
        return this._context;
    },

    get active() {
        return this._active;
    },

    get hover() {
        return this._hover;
    },

    get selected() {
        return this._selected;
    },

    set selected(selected) {
        if (this._selected == selected) {
            return;
        }

        this._selected = selected && !this._active;

        this._updateSelectedStyle();
        this._updateControlsVisibility();

        this.emit("selected-changed");
    },

    get area() {
        return { x1: this._mainBox.x,
                 y1: this._mainBox.y,
                 x2: this._mainBox.x + this._mainBox.width,
                 y2: this._mainBox.y + this._mainBox.height };
    },

    get actor() {
        return this._mainBox;
    }
}

function createThingFromId(thingId, args) {
    if (!thingId) {
        thingId = 'text';
    }

    let constructorModule = imports.ui.things[thingId];

    args.id = thingId;

    return constructorModule.create(args);
}

function createToolbar(thingId, args) {
    if (!thingId) {
        throw new Error("Can't create tool box group without thing id");
    }

    let constructorModule = imports.ui.things[thingId];

    if ('createToolbar' in constructorModule) {
        return constructorModule.createToolbar(args);
    }

    return null;
}

Signals.addSignalMethods(Thing.prototype);
