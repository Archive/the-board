// standard imports
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;

// gi imports
const Mx = imports.gi.Mx;

// ui imports
const ToolBox = imports.ui.toolBox;

const _GRID_SPACING = 10;

function ToolBoxBackgrounds(args) {
    this._init(args);
}

ToolBoxBackgrounds.prototype = {
    __proto__: ToolBox.ToolBox.prototype,

    _init : function(args) {
        args = args || {};

        args.title = Gettext.gettext("Background");
        args.isButtonGroup = true;

        ToolBox.ToolBox.prototype._init.apply(this, [args]);

        this._loadBackgrounds();

        // we're loaded after adding all buttons 
        this.loaded = true;
    },

    _loadBackgrounds : function() {
        this._backgrounds = {};

        let backgroundModules = imports.ui.backgrounds;

        for (let backgroundId in backgroundModules) {
            let backgroundModule = backgroundModules[backgroundId];

            if (!('create' in backgroundModule)) {
                continue;
            }

            let onClicked = Lang.bind(this,
                                      this._onBgButtonClicked,
                                      backgroundId);

            let button = this.addButton({ label: backgroundModule.NAME,
                                          onClicked: onClicked });

            this._backgrounds[backgroundId] =
                { name: backgroundModule.NAME,
                  id: backgroundId,
                  button: button };
        }

        // FIXME: allow loading third-party background implementations
        // from a user directory too
    },

    _onBgButtonClicked : function(button, backgroundId) {
        button.set_toggled(true);

        this.emit("action",
                  "setBackground",
                  { backgroundId: backgroundId });
    },

    setCurrentBackground : function(backgroundId) {
        this._backgrounds[backgroundId].button.set_toggled(true);
    },

    destroy : function() {
        ToolBox.ToolBox.prototype.destroy.apply(this, []);
    }
}
