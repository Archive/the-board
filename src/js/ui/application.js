// standard imports
const DBus = imports.dbus;
const Gettext = imports.gettext.domain("the-board");
const Lang = imports.lang;

// util imports
const Features = imports.util.features;
const Path = imports.util.path;

if (Features.HAVE_LIBSOUP) {
    const Http = imports.util.http;
}

// gi imports
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Mx = imports.gi.Mx;

if (Features.HAVE_LIBNOTIFY) {
    const Notify = imports.gi.Notify;
}

// ui imports
const MainWindow = imports.ui.mainWindow;

const _THE_BOARD_DBUS_PATH = "/org/gnome/TheBoard";
const _THE_BOARD_HTTP_PORT = 2010;

const TheBoardIface = {
    name: "org.gnome.TheBoard",

    methods: [ { name: "activate",
                 inSignature: '',
                 outSignature: '' },
               { name: "addThing" },
               { name: "addThings",
                 inSignature: 'aa{sv}',
                 outSignature: '' },
               { name: "getUserDirs" } ],

    signals: [],

    properties: []
};

function RemoteApplication(args) {
    this._init(args);
}

RemoteApplication.prototype = {
    _init : function(args) {
        DBus.session.proxifyObject(this,
                                   TheBoardIface.name,
                                   _THE_BOARD_DBUS_PATH);
    }
}

DBus.proxifyPrototype(RemoteApplication.prototype,
                      TheBoardIface);

function Application(args) {
    this._init(args);
}

Application.prototype = {
    _init : function(args) {
        DBus.session.acquire_name(TheBoardIface.name,
                                  DBus.SINGLE_INSTANCE,
                                  Lang.bind(this, this._onNameAcquired),
                                  Lang.bind(this, this._onNameNotAcquired));
    },

    _createMainWindow : function() {
        this._mainWindow =
            new MainWindow.MainWindow({ application: this });
    },

    _defineStyleAndThemes : function() {
        let mxIcontheme = Mx.IconTheme.get_default();
        mxIcontheme.set_search_paths([Path.ICONS_DIR]);

        let gtkIcontheme = Gtk.IconTheme.get_default();
        gtkIcontheme.append_search_path(Path.ICONS_DIR);

        let style = Mx.Style.get_default();
        style.load_from_file(Path.STYLE_DIR + "style.css");
    },

    _notifyInit : function() {
        if (Features.HAVE_LIBNOTIFY) {
            Notify.init("The Board");
        }
    },

    _showNotification : function(text) {
        if (Features.HAVE_LIBNOTIFY) {
            let notification =
                Notify.Notification.new(text, null, "the-board");

            notification.set_timeout(3000);
            notification.show();
        }
    },

    _addOneThing : function(args) {
        if (!('id' in args)) {
            log('Application: addThing(s) needs an id argument');
            return;
        }

        if (this._mainWindow.currentPage) {
            this._mainWindow.currentPage.addThingFromState(args);
        }
    },

    _onNameAcquired : function() {
        log('Application: acquired ' + TheBoardIface.name);

        DBus.session.exportObject(_THE_BOARD_DBUS_PATH, this);

        if (Features.HAVE_LIBSOUP) {
            Http.exportObject(_THE_BOARD_HTTP_PORT, TheBoardIface, this);
        }

        this._defineStyleAndThemes();
        this._notifyInit();
        this._createMainWindow();

        this._mainWindow.showAll();
    },

    _onNameNotAcquired : function() {
        log('Application: could not acquire ' +
            TheBoardIface.name +
            ". Activating existing instance.");

        let remoteApp = new RemoteApplication();
        remoteApp.activateRemote();

        this.quit();
    },

    // DBus method
    activate : function() {
        log('Application: activated via DBus'); 

        this._mainWindow.present();
    },

    // DBus method
    addThing : function(args) {
        log('Application: addThing via DBus');

        this._addOneThing(args);

        this._showNotification(Gettext.gettext("Added to The Board"));
    },

    // DBus method
    addThings : function(things) {
        log('Application: addThings via DBus');

        for (let i = 0; i < things.length; ++i) {
            this._addOneThing(things[i]);
        }

        this._showNotification(Gettext.gettext("Added to The Board"));
    },

    // DBus method
    getUserDirs : function(args) {
        let userDirs = {};

        let picsDir = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_PICTURES);
        userDirs.pictures = picsDir + "/The Board";

        let videosDir = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS);
        userDirs.videos = videosDir + "/The Board";

        return userDirs;
    },

    quit : function() {
        Gtk.main_quit();
    }
}

DBus.conformExport(Application.prototype, TheBoardIface);
