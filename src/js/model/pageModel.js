// standard imports
const ByteArray = imports.byteArray;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;

// gi imports
const GLib = imports.gi.GLib;
const GIO = imports.gi.Gio;
const TheBoard = imports.gi.TheBoard;

// util imports
const JSON = imports.util.json;

let State = {
    UNLOADED : 0,
    LOADING : 1,
    LOADED : 2
}

const _monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

function PageModel(args) {
    this._init(args);
}

PageModel.prototype = {
    _init : function(args) {
        args = args || {};

        this._dirty = false;
        this._state = State.UNLOADED;

        if ('id' in args) {
            this._id = args.id;
        } else {
            throw new Error("PageModel id is required");
        }

        if ('path' in args) {
            this._path = args.path;
        } else {
            throw new Error("PageModel path is required");
        }

        if ('index' in args) {
            this._index = args.index;
        } else {
            this._index = 0;
        }

        if ('title' in args) {
            this._title = args.title;
        } else {
            this._title = "";
        }

        if ('isNew' in args) {
            this._isNew = args.isNew;
        } else {
            this._isNew = false;
        }

        log('PageModel: Id ' + this._id);

        this._loadDateFromId();

        if (this._isNew) {
            this._createInitialContent();
        }
    },

    _loadDateFromId : function() {
        let date = new Date(this._id * 1000);
        this._day = date.getDate();
        this._month = date.getMonth() + 1;
        this._year = date.getFullYear();

        log('PageModel: Date ' +
            this._day + "/" +
            this._month + "/" +
            this._year);
    },

    _createInitialContent : function() {
        this._content = {};

        this._content.background = null;
        this._content.things = [];

        this._setState(State.LOADED);
        this._setDirty(true);
    },

    _loadContentFromPath : function() {
        if (this._state == State.LOADING) {
            return;
        }

        this._setState(State.LOADING);

        let file = GIO.file_new_for_path(this._path);

        this._contentLoadingCancellable = new GIO.Cancellable();

        log('PageModel: file.load_contents_async()');
        file.load_contents_async(this._contentLoadingCancellable,
                                 Lang.bind(this, this._onLoadContentsAsync),
                                 null);
    },

    _saveContentToPath : function(onResult) {
        if (this._state != State.LOADED) {
            return;
        }

        let file = GIO.file_new_for_path(this._path);
        let contentStr = JSON.JSON.stringify(this._content, null, 4);
        let contentBytes = ByteArray.fromString(contentStr);

        this._contentSavingCancellable = new GIO.Cancellable();

        log('PageModel: file.replace_contents_async()');
        TheBoard.g_file_replace_contents_async(file,
                                               contentBytes,
                                               this._contentSavingCancellable,
                                               Lang.bind(this,
                                                         this._onReplaceContentsAsync,
                                                         onResult));
    },

    _onLoadContentsAsync : function(file, result, data) {
        log('PageModel: file.load_contents_finish()');

        // discard cancellable
        delete this._contentLoadingCancellable;

        let content = "";

        try {
            content = TheBoard.g_file_load_contents_finish(file, result);
        } catch(e) {
            // FIXME: report io error in some way
            log('PageModel: failed to read file');
            return;
        }

        try {
            let newContent = JSON.JSON.parse(content);

            this._setContent(newContent);
            this._setState(State.LOADED);

            log('PageModel: content successfully loaded');
        } catch(e) {
            // FIXME: report parse error in some way
            log('PageModel: content parse error ' + e);
        }
    },

    _onReplaceContentsAsync : function(file, result, onResult) {
        log('PageModel: file.replace_contents_finish()');
        let [success, etagOut] =
            file.replace_contents_finish(result);

        // discard cancellable
        delete this._contentSavingCancellable;

        if (!success) {
            // FIXME: report io error in some way
            log('PageModel: failed to save file');
        }

        // no dirty anymore as we've just saved
        // the contents to disk
        this._setDirty(false);

        // not a 'new' model anymore as we've just
        // saved it to disk
        this._isNew = false;

        log('PageModel: content successfully saved');

        onResult();
    },

    _setContent : function(content) {
        if (this._content == content) {
            return;
        }

        this._content = content;
    },

    _setState : function(state) {
        if (this._state == state) {
            return;
        }

        this._state = state;

        this.emit("state-changed");
    },

    _setDirty : function(dirty) {
        if (this._dirty == dirty) {
            return;
        }

        this._dirty = dirty;

        this.emit("dirty-changed");
    },

    setBackground : function(background) {
        if (this._content.background == background) {
            return;
        }

        this._content.background = background;
        this._setDirty(true);
    },

    addThing : function(thing) {
        this._content.things.push(thing);
        this._setDirty(true);

        return this._content.things.indexOf(thing);
    },

    removeThing : function(index) {
        if (!(index in this._content.things)) {
            throw new Error("Removing undefined thing in PageModel");
        }

        this._content.things.splice(index, 1);
        this._setDirty(true);
    },

    updateThing : function(index, thing) {
        if (!(index in this._content.things)) {
            throw new Error("Updating undefined thing in PageModel");
        }

        this._content.things[index] = thing;
        this._setDirty(true);
    },

    load : function() {
        // FIXME: cancel any loading process

        if (this._state == State.LOADED ||
            this._state == State.LOADING) {
            return;
        }

        this._loadContentFromPath();
    },

    reload : function() {
        // FIXME: cancel any loading process

        if (this._state == State.LOADING) {
            return;
        }

        this._loadContentFromPath();
    },

    unload : function() {
        // FIXME: cancel any loading process

        this._content = null;
        this._setDirty(false);
        this._setState(State.UNLOADED);
    },

    save : function(onResult) {
        if (this._dirty) {
            this._saveContentToPath(onResult);
        } else {
            Mainloop.idle_add(onResult);
        }
    },

    isNew : function() {
        return this._isNew;
    },

    get dirty() {
        return this._dirty;
    },

    get id() {
        return this._id;
    },

    get day() {
        return this._day;
    },

    get month() {
        return _monthNames[this._month - 1];
    },

    get year() {
        return this._year;
    },

    get index() {
        return this._index;
    },

    get title() {
        return this._title;
    },

    set title(title) {
        if (this._title == title) {
            return;
        }

        this._title = title;
        this._setDirty(true);

        this.emit("title-changed");
    },

    get background() {
        return this._content.background || null;
    },

    get things() {
        return this._content.things || [];
    },

    get state() {
        return this._state;
    },

    get path() {
        return this._path;
    },

    destroy : function() {
    }
}

Signals.addSignalMethods(PageModel.prototype);
