// standard imports
const ByteArray = imports.byteArray;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;

// gi imports
const GIO = imports.gi.Gio;
const GLib = imports.gi.GLib;
const TheBoard = imports.gi.TheBoard;

// model imports
const PageModel = imports.model.pageModel;

// util imports
const JSON = imports.util.json;

let State = {
    IDLE : 0,
    LOADING_PAGES_FILE : 1,
    LOADING_PAGES : 2,
    LOADED : 3
}

const _SAVE_PAGES_TIMEOUT = 5;

const _PAGE_FILE_EXTENSION = "json";
const _DEFAULT_NUM_FILES = 10;

function PageManager(args) {
    this._init(args);
}

PageManager.prototype = {
    _init : function(args) {
        args = args || {};

        let envUserDir = GLib.getenv("THE_BOARD_USER_DIR");

        if (envUserDir) {
            this._userDir = envUserDir;
        } else {
            this._userDir = GLib.get_user_data_dir() +
                            '/the-board';
        }

        this._pagesDir = this._userDir + "/pages";

        this._pages = [];
        this._pagesIndex = {};

        this._pagesToSave = [];
        this._saving = false;

        this._state = State.IDLE; 

        this._loadPagesIndex();
    },

    _getPagesIndexPath : function() {
        return this._userDir + "/pages.json";
    },

    _loadPagesIndex : function() {
        this._setState(State.LOADING_PAGES_FILE);

        let file = GIO.file_new_for_path(this._getPagesIndexPath());

        this._pagesFileLoadingCancellable = new GIO.Cancellable();

        this._pagesIndex = {};

        log('PageManager: file.load_contents_async()');
        file.load_contents_async(this._pagesFileLoadingCancellable,
                                 Lang.bind(this, this._onPagesIndexLoadContentsAsync),
                                 null);
    },

    _loadPages : function() {
        this._setState(State.LOADING_PAGES);

        let dir = GIO.file_new_for_path(this._pagesDir);

        this._pageListingCancellable = new GIO.Cancellable();

        this._pages = [];

        log('PageManager: dir.enumerate_children_async()');
        dir.enumerate_children_async(GIO.FILE_ATTRIBUTE_STANDARD_NAME,
                                     GIO.FileQueryInfoFlags.NONE,
                                     GLib.PRIORITY_DEFAULT,
                                     this._pageListingCancellable,
                                     Lang.bind(this, this._onEnumerateChildrenAsync),
                                     null);
    },

    _findPageByPath : function(path) {
        let findByPath = function(page) {
            return (page.path == path);
        }

        let foundPage = this._pages.filter(findByPath);

        if (foundPage.length == 1) {
            return foundPage[0];
        }

        return null;
    },

    _removePageWithPath : function(path) {
        let pageToRemove = this._findPageByPath(path);
        let indexToRemove = this._pages.indexOf(pageToRemove)

        if (indexToRemove == -1) {
            return;
        }

        this._pages.splice(indexToRemove, 1);
        this.emit("page-removed", pageToRemove);
    },

    _addPageFromFilename : function(filename) {
        let [idStr, extension] = filename.split('.');

        if (extension !== _PAGE_FILE_EXTENSION) {
            return;
        }

        log('PageManager: Found page ' + idStr);

        let pageModel = this._createPageModel(parseInt(idStr), filename);
    },

    _setState : function(state) {
        if (this._state == state) {
            return;
        }

        this._state = state;

        this.emit("state-changed");
    },

    _sortPages : function() {
        let sortPagesById = function(modelA, modelB) {
            return modelA.id + modelA.index -
                   modelB.id - modelB.index;
        };

        // order pages by id/date and index
        this._pages.sort(sortPagesById);
    },

    _finishLoading : function() {
        // discard cancellable
        delete this._pageListingCancellable;

        this._setState(State.LOADED);

        this._monitorPagesDir();
    },

    _monitorPagesDir : function() {
        let dir = GIO.file_new_for_path(this._pagesDir);

        this._pagesDirMonitor =
            dir.monitor_directory(GIO.FileMonitorFlags.NONE,
                                  null);

        this._pagesDirMonitor.connect("changed",
                                      Lang.bind(this,
                                                this._onPagesDirMonitorChanged));
    },

    _ensureDirectory : function(dir, cancellable) {
        // FIXME: we're doing this synchronously which is not a
        // good idea in general for IO stuff. This should probably
        // be async

        try {
            log('PageManager: make_directory_with_parents ' + dir.get_path());
            dir.make_directory_with_parents(cancellable);
            this._finishLoading();
        } catch(e) {
            log('PageManager: failed make_directory_with_parents ' + e);
            // FIXME: report error somewhere in the ui
        }
    },

    _createPageModel : function(id, filename) {
        let isNew = false;

        if (!id) {
            id = Math.floor(Date.now() / 1000);
            filename = id + '.json';
            isNew = true;
        }

        let modelArgs = { id: id,
                          path: this._pagesDir + '/' + filename,
                          isNew: isNew };

        if (id in this._pagesIndex) {
            modelArgs.title = this._pagesIndex[id].title;
        }

        let pageModel = new PageModel.PageModel(modelArgs);

        pageModel._PageManager_dirtyChangedId =
            pageModel.connect("dirty-changed",
                              Lang.bind(this, this._onPageModelDirtyChanged));

        this._pages.push(pageModel);
        this._sortPages();

        this.emit("page-added", pageModel);

        return pageModel;
    },

    _scheduleSavePages : function() {
        this._savePagesTimeoutId =
            Mainloop.timeout_add_seconds(_SAVE_PAGES_TIMEOUT,
                                         Lang.bind(this,
                                                   this._onSavePagesTimeout));
    },

    _savePagesIndex : function(onComplete) {
        let file = GIO.file_new_for_path(this._getPagesIndexPath());
        let contentStr = JSON.JSON.stringify(this._pagesIndex, null, 4);
        let contentBytes = ByteArray.fromString(contentStr);

        this._pagesIndexSavingCancellable = new GIO.Cancellable();

        log('PageManager: file.replace_contents_async()');
        TheBoard.g_file_replace_contents_async(file,
                                               contentBytes,
                                               this._pagesIndexSavingCancellable,
                                               Lang.bind(this,
                                                         this._onPagesIndexReplaceContentsAsync,
                                                         onComplete));
    },

    _savePageModels : function(pageModels, onComplete) {
        let nToSave = pageModels.length;

        if (nToSave == 0) {
            Mainloop.idle_add(function() {
                                  onComplete();
                                  return false;
                              });
            return;
        }

        for (let i = 0; i < pageModels.length; ++i) {
            let pageModel = pageModels[i];

            let onPageModelSaved = function() {
                // FIXME: handle page model save errors
                // here when needed

                nToSave--;

                if (nToSave == 0) {
                    log('PageManager: all pages saved');
                    onComplete();
                }
            };

            pageModel.save(Lang.bind(this, onPageModelSaved));
        }
    },

    _savePages : function() {
        this._saving = true;

        let onPageModelsSaved = function() {
            this._saving = false;
        };

        let onPagesIndexSaved = function() {
            this._savePageModels(this._pagesToSave,
                                 Lang.bind(this, onPageModelsSaved));
        };

        this._syncPagesWithIndex(this._pagesToSave);
        this._savePagesIndex(Lang.bind(this, onPagesIndexSaved));
    },

    _deletePageFile : function(pageModel) {
        let file = GIO.file_new_for_path(pageModel.path);

        log('PageManager: file.delete_async()');
        TheBoard.g_file_delete_async(file, null,
                                     Lang.bind(this,
                                               this._onPageFileDeleteAsync),
                                     null);
    },

    _syncPagesWithIndex : function(pageModels) {
        for (let i = 0; i < pageModels.length; ++i) {
            let pageModel = pageModels[i];

            if (!(pageModel.id in this._pagesIndex)) {
                this._pagesIndex[pageModel.id] = {};
            }

            if (pageModel.title) {
                this._pagesIndex[pageModel.id].title = pageModel.title;
            } else {
                delete this._pagesIndex[pageModel.id].title;
            }
        }
    },

    _onPagesIndexLoadContentsAsync : function(file, result, data) {
        log('PageManager: pages file load_contents_finish()');

        let success;
        let content;
        let length;
        let etagOut;

        try {
            [success, content, length, etagOut] =
                file.load_contents_finish(result);
        } catch(e) {
            // FIXME: report io error in some way
            log('PageManager: failed to read pages file');

            let dir = GIO.file_new_for_path(this._userDir);
            this._ensureDirectory(dir, this._pagesFileLoadingCancellable);

            // It's possible that the pages file is not present but
            // we might have existing page files in the pages directory
            // anyway. So, we try to load the pages directory in any case.
            this._loadPages();
            return;
        }

        // discard cancellable
        delete this._pagesFileLoadingCancellable;

        try {
            if (content) {
                this._pagesIndex = JSON.JSON.parse(content);
            }

            log('PageManager: pages file successfully loaded');

            this._loadPages();
        } catch(e) {
            // FIXME: report parse error in some way
            log('PageManager: pages file parse error ' + e);
        }
    },

    _onPagesIndexReplaceContentsAsync : function(file, result, onComplete) {
        log('PageManager: pages file replace_contents_finish()');
        let [success, etagOut] =
            file.replace_contents_finish(result);

        // discard cancellable
        delete this._pagesIndexSavingCancellable;

        if (!success) {
            // FIXME: report io error in some way
            log('PageManager: failed to save pages file');
        }

        log('PageManager: pages file successfully saved');

        onComplete();
    },

    _onEnumerateChildrenAsync : function(dir, result, data) {
        log('PageManager: dir.enumerate_children_finish()');
        let enumerator = null;

        try {
            enumerator = dir.enumerate_children_finish(result);
        } catch (e) {
            // maybe this is because the directory doesn't exist yet
            // try to create the directory in that case
            this._ensureDirectory(dir, this._pageListingCancellable);
            return;
        }

        log('PageManager: enumerator.next_files_async()');
        enumerator.next_files_async(_DEFAULT_NUM_FILES,
                                    GLib.PRIORITY_DEFAULT,
                                    this._pageListingCancellable,
                                    Lang.bind(this, this._onNextFilesAsync),
                                    null);
    },

    _onNextFilesAsync : function(enumerator, result, data) {
        let infos = enumerator.next_files_finish(result);

        log('PageManager: onNextFileAsync');

        if (infos.length == 0) {
            log('PageManager: enumerator.close_async()');
            enumerator.close_async(GLib.PRIORITY_DEFAULT,
                                   this._pageListingCancellable,
                                   Lang.bind(this, this._onEnumeratorCloseAsync),
                                   null);
            return;
        }

        for (let i = 0; i < infos.length; ++i) {
            let info = infos[i];

            let filename = info.get_name();
            this._addPageFromFilename(filename);
        }

        log('PageManager: enumerator.next_files_async()');
        enumerator.next_files_async(_DEFAULT_NUM_FILES,
                                    GLib.PRIORITY_DEFAULT,
                                    this._pageListingCancellable,
                                    Lang.bind(this, this._onNextFilesAsync),
                                    null);
    },

    _onEnumeratorCloseAsync : function(enumerator, result, data) {
        log('PageManager: enumerator.close_finish()');
        enumerator.close_finish(result);

        this._finishLoading();
    },

    _onPageFileDeleteAsync : function(file, result, data) {
        log('PageManager: file.delete_finish()');

        try {
            TheBoard.g_file_delete_finish(file, result);
        } catch(e) {
            // FIXME: report io error in some way
            log('PageManager: failed to delete page file');
        }
    },

    _onPageModelDirtyChanged : function(pageModel) {
        log('PageManager: page model dirty changed to ' +
            pageModel.dirty + ' (' + pageModel.id + ')');

        let pageIndex = this._pagesToSave.indexOf(pageModel);

        if (pageModel.dirty && pageIndex == -1) {
            this._pagesToSave.push(pageModel);

            if (!this._savePagesTimeoutId) {
                this._scheduleSavePages();
            }
        } else if (!pageModel.dirty && pageModel != -1) {
            this._pagesToSave.splice(pageIndex, 1);
        }
    },

    _onSavePagesTimeout : function() {
        if (this._saving) {
            return true;
        }

        if (this._pagesToSave.length == 0) {
            delete this._savePagesTimeoutId;
            return false;
        }

        this._savePages();

        delete this._savePagesTimeoutId;

        return false;
    },

    _onPagesDirMonitorChanged : function(monitor, file, otherFile, eventType) {
        if (monitor.cancelled) {
            return;
        }

        // only care about directory changes after we have
        // fully loaded all pages from it
        if (this._state != State.LOADED) {
            return;
        }

        let path = file.get_path();
        let foundPage = this._findPageByPath(path);

        switch(eventType) {
        case GIO.FileMonitorEvent.DELETED:
            if (foundPage != null) {
                this._removePageWithPath(path);
            }
            break;
        case GIO.FileMonitorEvent.CREATED:
            if (foundPage == null) {
                let filename = file.get_basename();
                this._addPageFromFilename(filename);
            }
            break;
        }
    },

    addPageModel : function() {
        let pageModel = this._createPageModel();

        this._pagesToSave.push(pageModel);
        this._scheduleSavePages();

        return pageModel;
    },

    removePageModel : function(pageModel) {
        this._removePageWithPath(pageModel.path);
        this._deletePageFile(pageModel);
    },

    destroy : function() {
        if (this._pagesDirMonitor) {
            this._pagesDirMonitor.cancel();
            delete this._pagesDirMonitor;
        }

        if (this._savePagesTimeoutId) {
            Mainloop.source_remove(this._savePagesTimeoutId);
            delete this._savePagesTimeoutId;
        }
    },

    get latestPage() {
        return this._pages[this._pages.length - 1] || null;
    },

    get pages() {
        return this._pages;
    },

    get state() {
        return this._state;
    }
}

Signals.addSignalMethods(PageManager.prototype);
