GTESTER = $(TESTS_ENVIRONMENT) gtester
GTESTER_REPORT = gtester-report
TEST_PROGS_OPTIONS ?= --keep-going

TEST_PROGS = tb-js-unit
bin_PROGRAMS = tb-js-unit

tb_js_unit_CPPFLAGS = \
	-DGJS_JS_DIR=\"$(GJS_JS_DIR)\" \
	$(AM_CPPFLAGS) \
	$(THE_BOARD_CFLAGS)

tb_js_unit_LDADD = \
	$(THE_BOARD_LIBS)

## -rdynamic makes backtraces work
## we -export-dynamic so we can dlopen ourselves and use gobject-introspection
tb_js_unit_LDFLAGS=-export-dynamic -rdynamic
tb_js_unit_SOURCES = tb-js-unit.c

THE_BOARD_GJS_PATH = $(abs_top_builddir)/.libs:$(abs_top_srcdir)/src/js

# Default log output, overrideable from environment, thanks to
# conditional assignment operator '?=' .
THE_BOARD_TEST_LOGS_OUTPUT ?= $(builddir)/user-data/the-board-%u.log

TESTS_ENVIRONMENT = \
	DISPLAY='' \
	XDG_DATA_HOME=$(abs_top_builddir)/user-data \
    GJS_DEBUG_OUTPUT=$(THE_BOARD_TEST_LOGS_OUTPUT) \
	GI_TYPELIB_PATH=$(abs_top_builddir)/src \
	JS_TEST_DIR=$(abs_top_builddir)/test/js \
	GJS_PATH=$(THE_BOARD_GJS_PATH)

### gtester testing rules

# run one test only, e.g. test-/js/Array
test-/%:
	make test TEST_PROGS_OPTIONS="-p $(@:test-%=%)"

# test: run all tests in cwd
test: ${TEST_PROGS}
	@rm -rf user-data
	@mkdir -p user-data
	@test -z "${TEST_PROGS}" || ${GTESTER} --verbose ${TEST_PROGS} ${TEST_PROGS_OPTIONS}

# test-report: run tests in cwd and generate report
# perf-report: run tests in cwd with -m perf and generate report
# full-report: like test-report: with -m perf and -m slow
test-report perf-report full-report: ${TEST_PROGS}
	@test -z "${TEST_PROGS}" || { \
	  case $@ in \
	  test-report) test_options="-k";; \
	  perf-report) test_options="-k -m=perf";; \
	  full-report) test_options="-k -m=perf -m=slow";; \
	  esac ; \
	  ${GTESTER} --verbose $$test_options -o $@.xml ${TEST_PROGS} ${TEST_PROGS_OPTIONS}; \
	}
	@${GTESTER_REPORT} --version 2>/dev/null 1>&2 ; test "$$?" != 0 || ${GTESTER_REPORT} $@.xml >$@.html

test-console:
	@$(TESTS_ENVIRONMENT) rlwrap gjs-console

CLEANFILES = \
	unittestdb # Remove this when the tests remove the dbs themselves

.PHONY: test test-report perf-report full-report test-coverage

# run make test as part of make check
check-local: test

clean-local:
	-rm -rf test-coverage
	-rm -f *.gcda *.gcno
	-rm -rf user-data

noinst_jsdirs = \
	js

-include $(top_srcdir)/git.mk
