/* Copyright 2008 litl, LLC. All Rights Reserved. */

#include <config.h>

#include <glib.h>
#include <gjs/gjs.h>

#include <string.h>

typedef struct {
    GjsContext *context;
} TbTestJSFixture;

static const char *js_test_dir;

static void
setup (TbTestJSFixture *fix,
       gconstpointer    test_data)
{
  GError *error = NULL;
  gboolean success;
  int code;

  fix->context = gjs_context_new ();

  /* Load jsUnit.js directly into global scope, rather than
   * requiring each test to import it as a module, among other
   * things this lets us test importing modules without relying on
   * importing a module, but also it's just less typing to have
   * "assert*" without a prefix.
   */
  success = gjs_context_eval_file (fix->context, GJS_JS_DIR"/jsUnit.js", &code, &error);

  if (!success)
    g_error("%s", error->message);
}

static void
teardown (TbTestJSFixture *fix,
          gconstpointer     test_data)
{
  g_object_unref (fix->context);
}

static void
test (TbTestJSFixture *fix,
      gconstpointer     test_data)
{
  char *test_script;
  GError *error = NULL;
  gboolean success;
  int code;

  /* verify that last line of test is either 'gjstestRun();' or 'st;'
     this ensures that we accurately report the results of the test!
     otherwise gjstestRun() can fail silently, which is Not Good.
     Use:
      let st = gjstestRun();
      // some other stuff
      st;
     if you really want to do cleanup after gjstestRun().
  */
  if (!g_file_get_contents (test_data, &test_script, NULL, &error))
    g_error ("Couldn't read test: %s", (const char *) test_data);

  if (!g_regex_match_simple("(gjstestRun\\s*\\(\\s*\\)|st)\\s*;?\\s*"
                            "(/[*][^*]*[*]/\\s*)?$",
                            test_script, 0, 0))
    g_error("Return value from gjstestRun() may be being ignored! "
            "The test case should end in 'gjstestRun()' or 'st'.");

  g_free (test_script);

  /* ok, now run the rest. */
  success = gjs_context_eval_file (fix->context,
                                   (const char*) test_data,
                                   &code, &error);

  if (!success)
    g_error ("%s", error->message);

  g_assert (error == NULL);
  g_assert_cmpint (code, ==, 0);
}

static gint
mystrcmp (gconstpointer a, gconstpointer b)
{
  // filenames are not UTF8; we compare using native strcmp order, not
  // any locale-specific foo.
  return strcmp(*(char**)a, *(char**)b);
}

int
main(int argc, char **argv)
{
  GDir *dir;
  const char *name;
  GPtrArray *tests;
  unsigned i;

  g_test_init (&argc, &argv, NULL);

  g_type_init ();

  /* iterate through all 'test*.js' files in $JS_TEST_DIR */

  /* accumulate the results and sort them, so that our test runs
   * are consistent across machines (otherwise every developer will
   * run tests in a different order, depending on the whims of their
   * filesystem. */
  js_test_dir = g_getenv ("JS_TEST_DIR");
  dir = g_dir_open (js_test_dir, 0, NULL);

  g_assert (dir != NULL);

  tests = g_ptr_array_new();

  while ((name = g_dir_read_name(dir)) != NULL)
    {
      if (!(g_str_has_prefix (name, "test") &&
            g_str_has_suffix (name, ".js")))
          continue;

      g_ptr_array_add (tests, g_strdup(name));
    }

  g_dir_close (dir);

  g_ptr_array_sort (tests, mystrcmp);

  for (i = 0; i < tests->len; i++)
    {
      char *name = g_ptr_array_index (tests, i);
      char *test_name;
      char *file_name;

      /* pretty print, drop 'test' prefix and '.js' suffix from test name */
      test_name = g_strconcat ("/js/", name + 4, NULL);
      test_name[strlen (test_name)-3] = '\0';

      file_name = g_build_filename (js_test_dir, name, NULL);

      g_test_add (test_name, TbTestJSFixture, file_name, setup, test, teardown);

      g_free (test_name);
      g_free (name);

      /* not freeing file_name as it's needed while running the test */
    }

  g_ptr_array_free (tests, TRUE);

  return g_test_run ();
}
