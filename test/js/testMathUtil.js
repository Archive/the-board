const MathUtil = imports.util.mathUtil;

function testClamp() {
    assertEquals(5, MathUtil.clamp(7, 0, 5));
    assertEquals(0, MathUtil.clamp(-3, 0, 5));
    assertEquals(3, MathUtil.clamp(3, 0, 5));
    assertEquals(1, MathUtil.clamp(1, 1, 1));

    assertRaises('min greater than max', function() { MathUtil.clamp(0, 5, 0); });
}

gjstestRun();
