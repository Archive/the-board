#!/bin/sh
#
# Script that sets up jhbuild to build the-board. Run this to
# checkout jhbuild and the required configuration. This is heavily
# based on gnome-shell's gnome-shell-build-setup.sh
#
# Copyright (C) 2008, Red Hat, Inc.
# Copyright (C) 2010, Lucas Rocha
#
# Some ideas and code taken from gtk-osx-build
#
# Copyright (C) 2006, 2007, 2008 Imendio AB
#

# Pre-check on GNOME version

gnome_version=`gnome-session --version 2>/dev/null | (read name version && echo $version)`
have_gnome_26=false
case $gnome_version in
    2.2[6789]*|2.[3456789]*|3.*)
	have_gnome_26=true
    ;;
esac

if $have_gnome_26 ; then : ; else
   echo "GNOME 2.26 or newer is required to build The Board" 1>&2
   exit 1
fi

############################################################

release_file=

if which lsb_release > /dev/null 2>&1; then
  system=`lsb_release -is`
  version=`lsb_release -rs`
elif [ -f /etc/fedora-release ] ; then
  system=Fedora
  release_file=/etc/fedora-release
elif [ -f /etc/SuSE-release ] ; then
  system=SUSE
  release_file=/etc/SuSE-release
elif [ -f /etc/mandriva-release ]; then
  system=MandrivaLinux
  release_file=/etc/mandriva-release
fi

if [ x$release_file != x ] ; then
    version=`sed 's/[^0-9\.]*\([0-9\.]\+\).*/\1/' < $release_file`
fi

# Required software:
#
# For this script:
# binutils, curl, gcc, make, git
#
# General build stuff:
# automake, bison, flex, gettext, git, gnome-common, gtk-doc, intltool,
# libtool, pkgconfig
#
# Devel packages needed by the-board and its deps:
# dbus-glib, expat, GL, gstreamer, libffi,
# libjasper, libjpeg, libpng, libpulse, libtiff,
# libxml2, python, readline, spidermonkey ({mozilla,firefox,xulrunner}-js),
# libvorbis
#
# Non-devel packages needed by the-board and its deps:
# glxinfo, gstreamer-plugins-base, gstreamer-plugins-good,

if test "x$system" = xUbuntu -o "x$system" = xDebian -o "x$system" = xLinuxMint ; then
  reqd="
    build-essential curl automake bison flex gettext git-core gnome-common
    gnome-doc-utils gtk-doc-tools libdbus-glib-1-dev libexpat-dev libffi-dev
    libjasper-dev libjpeg-dev libpng-dev libtiff-dev libgl1-mesa-dev
    liborbit2-dev libpulse-dev libreadline5-dev libxml2-dev mesa-common-dev
    mesa-utils xulrunner-dev libvorbis-dev python-dev gperf
    "

  if apt-cache show autopoint > /dev/null 2> /dev/null; then
    reqd="$reqd autopoint"
  fi

  if [ ! -x /usr/bin/dpkg-checkbuilddeps ]; then
    echo "Please run 'sudo apt-get install dpkg-dev' and try again."
    echo
    exit 1
  fi

  for pkg in $reqd ; do
      if ! dpkg-checkbuilddeps -d $pkg /dev/null 2> /dev/null; then
        missing="$pkg $missing"
      fi
  done
  if test ! "x$missing" = x; then
    echo "Please run 'sudo apt-get install $missing' and try again."
    echo
    exit 1
  fi
fi

if test "x$system" = xFedora ; then
  reqd="
    binutils curl gcc gcc-c++ make automake bison flex gettext git gnome-common
    gnome-doc-utils intltool libtool pkgconfig dbus-glib-devel jasper-devel
    libffi-devel libjpeg-devel libpng-devel libtiff-devel mesa-libGL-devel
    pulseaudio-libs-devel readline-devel xulrunner-devel
    libxml2-devel glx-utils expat-devel libtool-ltdl-devel libvorbis-devel
    "

  if expr $version \>= 14 > /dev/null ; then
      reqd="$reqd gettext-autopoint"
  fi

  for pkg in $reqd ; do
      if ! rpm -q $pkg > /dev/null 2>&1; then
        missing="$pkg $missing"
      fi
  done
  if test ! "x$missing" = x; then
    gpk-install-package-name $missing
  fi
fi

if test "x$system" = xSUSE -o "x$system" = "xSUSE LINUX" ; then
  reqd=""
  for pkg in \
    curl bison flex gtk-doc gnome-common gnome-doc-utils-devel \
    libpulse-devel libtiff-devel libffi-devel \
    xorg-x11-proto-devel readline-devel \
    mozilla-xulrunner191-devel \
    xorg-x11-devel xorg-x11 xorg-x11-server-extra \
    ; do
      if ! rpm -q $pkg > /dev/null 2>&1; then
        reqd="$pkg $reqd"
      fi
  done
  if test ! "x$reqd" = x; then
    echo "Please run 'su --command=\"zypper install $reqd\"' and try again."
    echo
    exit 1
  fi
fi

if test "x$system" = xMandrivaLinux ; then
  reqd=""
  for pkg in \
    curl bison flex gnome-common gnome-doc-utils gtk-doc intltool \
    ffi5-devel GL-devel readline-devel libxulrunner-devel \
    libxdamage-devel mesa-demos \
    ; do
      if ! rpm -q --whatprovides $pkg > /dev/null 2>&1; then
        reqd="$pkg $reqd"
      fi
  done
  if test ! "x$reqd" = x; then
	gurpmi --auto $reqd
  fi
fi

SOURCE=$HOME/Code/the-board
BASEURL=http://git.gnome.org/browse/the-board/plain/build

if [ -d $SOURCE ] ; then : ; else
    mkdir -p $SOURCE
    echo "Created $SOURCE"
fi

if [ -d $SOURCE/jhbuild ] ; then
    if [ -d $SOURCE/jhbuild/.git ] ; then
        echo -n "Updating jhbuild ... "
        ( cd $SOURCE/jhbuild && git pull --rebase > /dev/null ) || exit 1
        echo "done"
    else
        echo "$SOURCE/jhbuild is not a git repository"
        echo "You should remove it and rerun this script"
	exit 1
    fi
else
    echo "Checking out jhbuild into $SOURCE/jhbuild ... "
    cd $SOURCE
    git clone git://git.gnome.org/jhbuild > /dev/null || exit 1
    echo "done"
fi

echo -n "Installing jhbuild..."
(cd $SOURCE/jhbuild && make -f Makefile.plain DISABLE_GETTEXT=1 bindir=$HOME/bin install >/dev/null)
echo "done"

echo -n "Writing $HOME/.jhbuildrc-the-board ... "
curl -L -s -o $HOME/.jhbuildrc-the-board $BASEURL/jhbuildrc-the-board
echo "done"

if test "x`echo $PATH | grep $HOME/bin`" = x; then
    echo "PATH does not contain $HOME/bin, it is recommended that you add that."
    echo
fi

echo "Done."
